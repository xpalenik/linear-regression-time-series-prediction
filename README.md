# Time Series Prediction Using Linear Regression

- task: predict when each product would go out of stock
- time: 5 days
- methods: linear regression, random forests
- input: 6 tables with sales data

I decided to capture the decline of stock quantity for each product, which made this a time-series analysis problem. Then I used linear regression, but noticed some limitations of this method. With no prior knowledge on time series, I used a heuristic to solve the problem.

For further research, I would probably use this paper:

Andaur, Juan & Ruz, Gonzalo & Goycoolea, Marcos. (2021). Predicting Out-of-Stock Using Machine Learning: An Application in a Retail Packaged Foods Manufacturing Company. Electronics. 10. 2787. 10.3390/electronics10222787.
https://www.researchgate.net/publication/356213664_Predicting_Out-of-Stock_Using_Machine_Learning_An_Application_in_a_Retail_Packaged_Foods_Manufacturing_Company/fulltext/619277f307be5f31b78461c3/Predicting-Out-of-Stock-Using-Machine-Learning-An-Application-in-a-Retail-Packaged-Foods-Manufacturing-Company.pdf

## Zobraz a načítaj dostupné tabuľky


```python
from keboola.component import CommonInterface
ci = CommonInterface()
input_tables = ci.get_input_tables_definitions()

for table in input_tables:
    logging.info(f'The table named: "{table.name}" is at path: {table.full_path}')
```

    The table named: "v-product-list.csv" is at path: /data/in/tables/v-product-list.csv
    The table named: "cm-stock-days-data.csv" is at path: /data/in/tables/cm-stock-days-data.csv
    


```python
import pandas as pd
productList = pd.read_csv('/data/in/tables/v-product-list.csv')
stockDaysData = pd.read_csv('/data/in/tables/cm-stock-days-data.csv')
```

    Note: NumExpr detected 16 cores but "NUMEXPR_MAX_THREADS" not set, so enforcing safe limit of 8.
    NumExpr defaulting to 8 threads.
    

## Vyber produkty, ktoré budeme predikovať

Predikovať vyčerpanie zásob môžeme len u produktov, ktoré sú aktuálne na sklade. Dátová sada však obsahuje prípady keď 1 produkt (s daným 'entity_id_hash') zaberá viac riadkov a v stĺpci 'is_in_stock' preto môže existovať viac hodnôt. Ako však dokazuje nasledovná kontrola, tieto hodnoty sú všetky rovnaké - buď 1 alebo 0 - a preto ide zrejme o aktuálny stav na sklade.


```python
productList[['entity_id_hash','is_in_stock']].groupby(['entity_id_hash']).mean()['is_in_stock'].value_counts()
```




    0.0    5910
    1.0    2883
    Name: is_in_stock, dtype: int64




```python
productList.shape
```




    (54809, 14)




```python
productList = productList[productList['long_term_unavaible'] == 0] # vyber dlhodobo dostupné produkty
productList = productList[productList['type_id'] == 'simple'] # vyber jednoduché (simple) produkty
productList = productList[productList['is_in_stock'] == 1] # vyčerpať zásoby môžeme len u produktov, ktoré su na sklade
productList = productList['entity_id_hash'].unique() # zaujímajú nás len unikátne produkty

productList.shape
```




    (1848,)



Z pôvodných 54809 sme teda vyfiltrovali 1848 produktov, ktoré budeme predikovať.

## Analýza údajov o predajnosti

Cieľom bude z tabuľky cm-stock-days-data získať dáta o predajnosti produktu. Z nich následne analýzov časovej rady pomocou zvoleného modelu strojového učenia predikovať, kedy množstvo produktu klesne na 0.


```python
stockDaysData.shape
```




    (7622765, 3)



Tabuľka má cez 7 miliónov záznamov, môžeme však odstrániť dlhodobo nedostupné, konfigurovateľné a tie čo nie sú na sklade.


```python
stockDaysData = stockDaysData[stockDaysData['product_id_hash'].isin(productList)]
```


```python
stockDaysData.shape
```




    (1351341, 3)




```python
stockDaysData['period'] = stockDaysData['period'].astype('datetime64[ns]')
```


```python
stockDaysData.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>period</th>
      <th>product_id_hash</th>
      <th>qty</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2017-09-03</td>
      <td>cb8a08a240f3ea7c99b220d24f54f477</td>
      <td>67.0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>2018-01-26</td>
      <td>800103a4d112ae28491b249670a071ec</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>17</th>
      <td>2017-09-14</td>
      <td>587b7b833034299fdd5f4b10e7dc9fca</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>25</th>
      <td>2017-09-24</td>
      <td>ff82db7535530637af7f8a96284b3459</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>26</th>
      <td>2017-10-28</td>
      <td>a546203962b88771bb06faf8d6ec065e</td>
      <td>2.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
stockDaysData['product_id_hash'].value_counts()
```




    2f3c6a4cd8af177f6456e7e51a916ff3    1212
    415185ea244ea2b2bedeb0449b926802    1212
    e1dc4bf1f94e87fdfeb2d91ae3dc10ef    1212
    ff1418e8cc993fe8abcfe3ce2003e5c5    1212
    40dba662fae60cd3bcceaa76a82d2873    1212
                                        ... 
    cb01a90256508ed990fe50e3562d0983       5
    c56aa2102f060ad7471fbefe5e296c92       5
    b39ac02ff5021fed10cb9988a23d5d02       5
    874121fee1d3bebea38815d169d47848       4
    9c8001602e98208ef4e8d1bbf79fee65       4
    Name: product_id_hash, Length: 1848, dtype: int64



Niektoré produkty obsahujú príliš málo dát pre spoľahlivú predikciu.

Teraz vizualizujem predajnosť pre zopár produktov.


```python
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(rc={'figure.figsize':(30,10)})
```

    generated new fontManager
    


```python
oneProduct = stockDaysData[stockDaysData['product_id_hash'] == '82836ca597a373e6c3cd5ae2d466161e']
sns.lineplot(x = oneProduct['period'], y = oneProduct['qty'])
plt.show()
```


    
![png](notebook_files/notebook_21_0.png)
    


Tento produkt ide celkom "na dračku". Zároveň nie sú väčšie problémy s doskladňovaním.


```python
oneProduct = stockDaysData[stockDaysData['product_id_hash'] == '25702d4234f4c7dc542adde64426a7ca']
sns.lineplot(x = oneProduct['period'], y = oneProduct['qty'])
plt.show()
```


    
![png](notebook_files/notebook_23_0.png)
    


Tento produkt sa predáva horšie. Stavy keď nie je na sklade sú stále minimálne.

Údaje o predajnosti majú lineárny charakter (množstvo produktu klesá s časom) a preto sa ponúka lineárna regresia. V grafe vyššie však dochádza k doplneniu zásob, čo sú tie vertikálne čiary. To je pre nás irelevantné, preto nasledovná funkcia tieto dopĺňania eliminuje. Výsledkom je nerastúca (klesajúca/konštnantná) funkcia ktorá zachytáva ako množstvo produktu klesá.

Význam parametra ignore_out_of_stock vysvetlím neskôr.


```python
import numpy as np

def get_sales(p, ignore_out_of_stock=False):
    p = p.sort_values(by=['period'])
    p.drop_duplicates()
    reversed_qty = p['qty'][::-1]
    reversed_result = []

    ground = 0
    prevq = 0

    def is_stock_refil(current_q, previous_q):
        return current_q < previous_q

    for q in reversed_qty:
        if is_stock_refil(q, prevq):
            ground += prevq

        if ignore_out_of_stock and q==0:
            pass
        else:
            reversed_result.append(ground + q)
            
        prevq = q

    return reversed_result[::-1]
```

Vezmime si napríklad nasledovný produkt.


```python
oneProduct = stockDaysData[stockDaysData['product_id_hash'] == '9cea886b9f44a3c2df1163730ab64994']

oneProduct = oneProduct.drop(columns='product_id_hash')
oneProduct = oneProduct.drop_duplicates()
oneProduct = oneProduct.sort_values(by=['period'])

sns.lineplot(x = oneProduct['period'], y = oneProduct['qty'])
plt.show()
```


    
![png](notebook_files/notebook_28_0.png)
    


Použijme get_sales() k získaniu jeho grafu predajnosti.


```python
y_sales = get_sales(oneProduct, ignore_out_of_stock=False)
sns.lineplot(x = oneProduct['period'], y = y_sales)
plt.show()
```


    
![png](notebook_files/notebook_30_0.png)
    


Ako vidno, tento produkt niekoľko mesiacov nebol na sklade. To môže výrazne ovplyvniť predikciu lineárnym (alebo akýmkoľvek iným) modelom. Z hľadiska predajnosti je to však viacmenej nepodstatné - ak produkt nie je na sklade neznamená to, že oň nie je záujem. Preto som k get_sales() pridal parameter 'ignore_out_of_stock' ktorý ignoruje kvantitu z časových úsekov keď produkt nebol na sklade.


```python
y_sales = get_sales(oneProduct, ignore_out_of_stock=True)
sns.lineplot(x = range(len(y_sales)), y = y_sales)
plt.show()
```


    
![png](notebook_files/notebook_32_0.png)
    


Rozdiel medzi odhadmi s a bez parametra ignore_out_of_stock si môžeme vizualizovať na nasledovných dvoch grafoch.


```python
from sklearn.linear_model import LinearRegression

y_sales = get_sales(oneProduct, ignore_out_of_stock=False)
y = np.asarray(y_sales)
X = np.asarray(range(len(y))).reshape(-1, 1)

clf = LinearRegression()
clf.fit(X, y)

# predict on the same period
preds = clf.predict(X)
 
# plot what has been learned
plt.plot(X, y)
plt.plot(X, preds)
```




    [<matplotlib.lines.Line2D at 0x7f9d23fe36a0>]




    
![png](notebook_files/notebook_34_1.png)
    



```python
from sklearn.linear_model import LinearRegression

y_sales = get_sales(oneProduct, ignore_out_of_stock=True)
y = np.asarray(y_sales)
X = np.asarray(range(len(y))).reshape(-1, 1)

clf = LinearRegression()
clf.fit(X, y)

# predict on the same period
preds = clf.predict(X)
 
# plot what has been learned
plt.plot(X, y)
plt.plot(X, preds)
```




    [<matplotlib.lines.Line2D at 0x7f9d3022e970>]




    
![png](notebook_files/notebook_35_1.png)
    


Použijeme teda druhý model k odhadu dňa vyčerpania zásob.


```python
from sklearn.linear_model import LinearRegression

def getEstimate(product):
    y_sales = get_sales(product, ignore_out_of_stock=True)
    y = np.asarray(y_sales).reshape(-1, 1)
    X = np.asarray(range(len(y))).reshape(-1, 1)

    my_lr = LinearRegression()
    my_lr.fit(X, y)

    a = my_lr.coef_[0][0]
    b = my_lr.intercept_[0]

    today = X[-1][0]
    stockout = (-b/a).astype(int) # podla rovnice y=ax+b
    
    return stockout - today

print('Stockout expected in', getEstimate(oneProduct), 'days')
```

    Stockout expected in 26 days
    

Potom som si uvedomil, že regresná priamka (oranžová) nedostatočne zohľadňuje najnovšie dáta. Ak sa produkt začal predávať viac, treba predpokladať skoršie vyčerpanie zásob a to model nezohľadňuje. Je to príliš hrubý odhad, ide to under-fitting. Vyskúšal som teda model RandomForestRegressor.


```python
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import PolynomialFeatures

y_sales = get_sales(oneProduct, ignore_out_of_stock=False)
y = np.asarray(y_sales)
X = np.asarray(oneProduct['period']).reshape(-1, 1)

clf = RandomForestRegressor(max_depth = 4)
clf.fit(X, y)

# predict on the same period
preds = clf.predict(X)
 
# plot what has been learned
plt.plot(X, y)
plt.plot(X, preds)
```




    [<matplotlib.lines.Line2D at 0x7f9d26081430>]




    
![png](notebook_files/notebook_39_1.png)
    


Ako vidno, ide o over-fitting. Pokúsil som sa optimalizovať hyperparametre cez RandomizedSearchCV no narazil som na problém krosvalidácie. Ide o časovú radu a tak nemožno Ľubovoľne rozdeliť dáta na train a test set. Je nutné použiť sklearn.model_selection.TimeSeriesSplit no už som ho nestihol naštudovať. Napadlo mi jednoduchšie riešenie odhadu a tak som tento smer opustil. 

## Finálne riešenie

Táto kapitola popisuje riešenie, ktoré som nakoniec použil na odhad.

Tu je graf predajnosti produktu, ktorý odhadujeme, spolu s jeho lineárnou aproximáciou.


```python
from sklearn.linear_model import LinearRegression
import datetime

p = stockDaysData[stockDaysData['product_id_hash'] == '25702d4234f4c7dc542adde64426a7ca']
p = p.drop_duplicates()
p = p.sort_values(by=['period'])
    
clf = LinearRegression()
    
y_sales = get_sales(p, ignore_out_of_stock=True)
y = np.asarray(y_sales)
X = np.asarray(range(len(y))).reshape(-1, 1)
    
clf.fit(X, y)
preds = clf.predict(X)

plt.plot(X, y)
plt.plot(X, preds)
```




    [<matplotlib.lines.Line2D at 0x7f07ec8d3d00>]




    
![png](notebook_files/notebook_44_1.png)
    


Namiesto regresnej priamky som k odhadu použil počet kusov produktu predaný za tú časť obdobia, po ktorú sa produkt reálne dal zakúpiť.


```python
def get_sales_rate(y):
    full_qty = y[0]
    curr_qty = y[-1]
    days = len(y)
    return (full_qty - curr_qty) / days

sales_rate = get_sales_rate(y)
sales_rate
```




    0.17557251908396945




```python
curr_qty = y[-1]
daysToNextStockout = round(curr_qty / sales_rate)

daysToNextStockout
```




    17




```python
nextStockout = p['period'].max() + pd.Timedelta(days=daysToNextStockout)
nextStockout
```




    Timestamp('2019-05-09 00:00:00')




```python
print('Today is day', p['period'].max())
print('Stockout expected in', daysToNextStockout, 'days')
print('Stockout is expected at day', nextStockout)
```

    Today is day 2019-04-22 00:00:00
    Stockout expected in 17 days
    Stockout is expected at day 2019-05-09 00:00:00
    

Produkt sa teda vyčerpá za 17 dní, v grafe som označil červeno bodkou.


```python
plt.plot(p['period'], p['qty'])
plt.plot(nextStockout,0, 'ro', markersize=10)
```




    [<matplotlib.lines.Line2D at 0x7f9d3032da90>]




    
![png](notebook_files/notebook_51_1.png)
    


Teraz sa pokúsim zistiť ako by dopadol odhad ak by som uvažoval len novšie údaje o predajnosti. Budem uvažovať len tú novšiu polovicu z predošlých údajov o predajnosti - približne obdobie od 2018-07 do súčasnosti. 


```python
sales_rate_emphasize_recent = get_sales_rate(y[len(y)//2:])
sales_rate_emphasize_recent
```




    0.22519083969465647




```python
curr_qty = y[-1]
daysToNextStockout_emphasize_recent = round(curr_qty / sales_rate_emphasize_recent)

daysToNextStockout_emphasize_recent
```




    13




```python
nextStockout_emphasize_recent = p['period'].max() + pd.Timedelta(days=daysToNextStockout_emphasize_recent)
nextStockout_emphasize_recent
```




    Timestamp('2019-05-05 00:00:00')



Novší odhad som označil zelenou bodkou. Ako vidno, predaj produktu v poslednej dobe zrýchlil a tak sa predpokladá skoršie vyčerpanie zásob.


```python
plt.plot(p['period'], p['qty'])
plt.plot(nextStockout,0, 'ro', markersize=10)
plt.plot(nextStockout_emphasize_recent,0, 'go', markersize=10)
```




    [<matplotlib.lines.Line2D at 0x7f9d342cdb80>]




    
![png](notebook_files/notebook_57_1.png)
    


Vytvoril som funkciu pre postup výpočtu odhadu.


```python
def get_next_stockout(p, emphasize_recent=False):
    p = p.drop_duplicates()
    p = p.sort_values(by=['period'])
    
    y = get_sales(p, ignore_out_of_stock=True)
    X = range(len(y))

    if emphasize_recent:
        y = y[len(y)//2:]
    
    sales_rate = get_sales_rate(y)
    curr_qty = y[-1]
    daysToNextStockout = round(curr_qty / sales_rate)
    
    return p['period'].max() + pd.Timedelta(days=daysToNextStockout)

get_next_stockout(p, emphasize_recent=True)
```




    Timestamp('2019-05-05 00:00:00')



Následuje ešte jeden príklad produktu, kde má zmysel uprednostniť novšie dáta. Ak mám málo údajov, ako v nasledovnom prípade, všetky historické dáta by mi predpovedali pravú (červenú) hodnotu. Rozumnejšie je však asi predpokladať skoršie vyčerpanie zásob (zelená), lebo v posledných dňoch predajnosť vzrástla. Rozdiel pritom činí viac než 1 mesiac.


```python
p = stockDaysData[stockDaysData['product_id_hash'] == 'cb01a90256508ed990fe50e3562d0983']
p = p.drop_duplicates()
p = p.sort_values(by=['period'])

plt.plot(p['period'], p['qty'])
plt.plot(get_next_stockout(p, emphasize_recent=False), 0, 'ro', markersize=10)
plt.plot(get_next_stockout(p, emphasize_recent=True), 0, 'go', markersize=10)
```




    [<matplotlib.lines.Line2D at 0x7f9d23f582b0>]




    
![png](notebook_files/notebook_61_1.png)
    


Následuje už len výpočet odhadu pre každý z odhadovaných produktov a export výsledkov do CSV.


```python
df = pd.DataFrame([], columns = ['product_id_hash', 'expected_stockout'])
remaining = len(productList)

for product_id_hash in productList:
    print('adding product', product_id_hash)
    
    try:
        p = stockDaysData[stockDaysData['product_id_hash'] == product_id_hash]

        p = p.drop(columns='product_id_hash')
        p = p.drop_duplicates()
        p = p.sort_values(by=['period'])

        df = df.append({'product_id_hash': product_id_hash, 'expected_stockout': get_next_stockout(p)}, ignore_index=True)
        
        print('success!')
        print('remaining products: ', remaining, '\n')
    except:
        print('unknown error, skipping product')
        
    remaining-=1

```

    adding product 072b030ba126b2f4b2374f342be9ed44
    success!
    remaining products:  1848 
    
    adding product 32bb90e8976aab5298d5da10fe66f21d
    success!
    remaining products:  1847 
    
    adding product e00da03b685a0dd18fb6a08af0923de0
    success!
    remaining products:  1846 
    
    adding product 1d7f7abc18fcb43975065399b0d1e48e
    success!
    remaining products:  1845 
    
    adding product 8c19f571e251e61cb8dd3612f26d5ecf
    success!
    remaining products:  1844 
    
    adding product 94f6d7e04a4d452035300f18b984988c
    success!
    remaining products:  1843 
    
    adding product f2fc990265c712c49d51a18a32b39f0c
    success!
    remaining products:  1842 
    
    adding product f9b902fc3289af4dd08de5d1de54f68f
    success!
    remaining products:  1841 
    
    adding product 01386bd6d8e091c2ab4c7c7de644d37b
    success!
    remaining products:  1840 
    
    adding product 46922a0880a8f11f8f69cbb52b1396be
    success!
    remaining products:  1839 
    
    adding product a532400ed62e772b9dc0b86f46e583ff
    success!
    remaining products:  1838 
    
    adding product 303ed4c69846ab36c2904d3ba8573050
    success!
    remaining products:  1837 
    
    adding product eaae339c4d89fc102edd9dbdb6a28915
    success!
    remaining products:  1836 
    
    adding product 26408ffa703a72e8ac0117e74ad46f33
    success!
    remaining products:  1835 
    
    adding product 5dd9db5e033da9c6fb5ba83c7a7ebea9
    success!
    remaining products:  1834 
    
    adding product ca9c267dad0305d1a6308d2a0cf1c39c
    success!
    remaining products:  1833 
    
    adding product 08d98638c6fcd194a4b1e6992063e944
    success!
    remaining products:  1832 
    
    adding product 24681928425f5a9133504de568f5f6df
    success!
    remaining products:  1831 
    
    adding product 556f391937dfd4398cbac35e050a2177
    success!
    remaining products:  1830 
    
    adding product aba3b6fd5d186d28e06ff97135cade7f
    success!
    remaining products:  1829 
    
    adding product c8ed21db4f678f3b13b9d5ee16489088
    success!
    remaining products:  1828 
    
    adding product 08419be897405321542838d77f855226
    success!
    remaining products:  1827 
    
    adding product 7f1171a78ce0780a2142a6eb7bc4f3c8
    success!
    remaining products:  1826 
    
    adding product 82f2b308c3b01637c607ce05f52a2fed
    success!
    remaining products:  1825 
    
    adding product 470e7a4f017a5476afb7eeb3f8b96f9b
    success!
    remaining products:  1824 
    
    adding product b6edc1cd1f36e45daf6d7824d7bb2283
    success!
    remaining products:  1823 
    
    adding product 670e8a43b246801ca1eaca97b3e19189
    success!
    remaining products:  1822 
    
    adding product 81e74d678581a3bb7a720b019f4f1a93
    success!
    remaining products:  1821 
    
    adding product 96b9bff013acedfb1d140579e2fbeb63
    success!
    remaining products:  1820 
    
    adding product 43fa7f58b7eac7ac872209342e62e8f1
    success!
    remaining products:  1819 
    
    adding product f0adc8838f4bdedde4ec2cfad0515589
    success!
    remaining products:  1818 
    
    adding product 3b5dca501ee1e6d8cd7b905f4e1bf723
    success!
    remaining products:  1817 
    
    adding product e2a2dcc36a08a345332c751b2f2e476c
    success!
    remaining products:  1816 
    
    adding product 632cee946db83e7a52ce5e8d6f0fed35
    success!
    remaining products:  1815 
    
    adding product 677e09724f0e2df9b6c000b75b5da10d
    success!
    remaining products:  1814 
    
    adding product 7250eb93b3c18cc9daa29cf58af7a004
    success!
    remaining products:  1813 
    
    adding product 013a006f03dbc5392effeb8f18fda755
    success!
    remaining products:  1812 
    
    adding product 301ad0e3bd5cb1627a2044908a42fdc2
    success!
    remaining products:  1811 
    
    adding product b0b183c207f46f0cca7dc63b2604f5cc
    success!
    remaining products:  1810 
    
    adding product fa83a11a198d5a7f0bf77a1987bcd006
    success!
    remaining products:  1809 
    
    adding product d045c59a90d7587d8d671b5f5aec4e7c
    success!
    remaining products:  1808 
    
    adding product eeb69a3cb92300456b6a5f4162093851
    success!
    remaining products:  1807 
    
    adding product 4e0cb6fb5fb446d1c92ede2ed8780188
    success!
    remaining products:  1806 
    
    adding product 6cfe0e6127fa25df2a0ef2ae1067d915
    success!
    remaining products:  1805 
    
    adding product 8f468c873a32bb0619eaeb2050ba45d1
    success!
    remaining products:  1804 
    
    adding product 24146db4eb48c718b84cae0a0799dcfc
    success!
    remaining products:  1803 
    
    adding product 6c14da109e294d1e8155be8aa4b1ce8e
    success!
    remaining products:  1802 
    
    adding product 81e5f81db77c596492e6f1a5a792ed53
    success!
    remaining products:  1801 
    
    adding product 838e8afb1ca34354ac209f53d90c3a43
    success!
    remaining products:  1800 
    
    adding product a284df1155ec3e67286080500df36a9a
    success!
    remaining products:  1799 
    
    adding product 17fafe5f6ce2f1904eb09d2e80a4cbf6
    success!
    remaining products:  1798 
    
    adding product 8f19793b2671094e63a15ab883d50137
    success!
    remaining products:  1797 
    
    adding product faafda66202d234463057972460c04f5
    success!
    remaining products:  1796 
    
    adding product 297fa7777981f402dbba17e9f29e292d
    success!
    remaining products:  1795 
    
    adding product 89f03f7d02720160f1b04cf5b27f5ccb
    success!
    remaining products:  1794 
    
    adding product 86d7c8a08b4aaa1bc7c599473f5dddda
    success!
    remaining products:  1793 
    
    adding product 673271cc47c1a4e77f57e239ed4d28a7
    success!
    remaining products:  1792 
    
    adding product b29eed44276144e4e8103a661f9a78b7
    success!
    remaining products:  1791 
    
    adding product 3e15cc11f979ed25912dff5b0669f2cd
    success!
    remaining products:  1790 
    
    adding product ff1418e8cc993fe8abcfe3ce2003e5c5
    success!
    remaining products:  1789 
    
    adding product e06f967fb0d355592be4e7674fa31d26
    success!
    remaining products:  1788 
    
    adding product 3f53d7190148675e3cd472fc826828c5
    success!
    remaining products:  1787 
    
    adding product ea204361fe7f024b130143eb3e189a18
    success!
    remaining products:  1786 
    
    adding product a6ea8471c120fe8cc35a2954c9b9c595
    success!
    remaining products:  1785 
    
    adding product 6b5754d737784b51ec5075c0dc437bf0
    success!
    remaining products:  1784 
    
    adding product 33bb83720ba9d2b6da87114380314af5
    success!
    remaining products:  1783 
    
    adding product f7ac67a9aa8d255282de7d11391e1b69
    success!
    remaining products:  1782 
    
    adding product 9657c1fffd38824e5ab0472e022e577e
    success!
    remaining products:  1781 
    
    adding product eb06b9db06012a7a4179b8f3cb5384d3
    success!
    remaining products:  1780 
    
    adding product 64c31821603ab476a318839606743bd6
    success!
    remaining products:  1779 
    
    adding product e93028bdc1aacdfb3687181f2031765d
    success!
    remaining products:  1778 
    
    adding product 908c9a564a86426585b29f5335b619bc
    success!
    remaining products:  1777 
    
    adding product 747d3443e319a22747fbb873e8b2f9f2
    success!
    remaining products:  1776 
    
    adding product b22b257ad0519d4500539da3c8bcf4dd
    success!
    remaining products:  1775 
    
    adding product d60678e8f2ba9c540798ebbde31177e8
    success!
    remaining products:  1774 
    
    adding product 5e6d27a7a8a8330df4b53240737ccc85
    success!
    remaining products:  1773 
    
    adding product ce47be4abd80ac324c645fd57a27fc73
    success!
    remaining products:  1772 
    
    adding product 59dfa2df42d9e3d41f5b02bfc32229dd
    success!
    remaining products:  1771 
    
    adding product d3630410c51e60941a9001a46871070e
    success!
    remaining products:  1770 
    
    adding product 91378b331327b40e564390c43cd6b2be
    success!
    remaining products:  1769 
    
    adding product 5812f92450ccaf17275500841c70924a
    success!
    remaining products:  1768 
    
    adding product f87522788a2be2d171666752f97ddebb
    success!
    remaining products:  1767 
    
    adding product 4ad13f04ef4373992c9d3046200aa350
    success!
    remaining products:  1766 
    
    adding product 2067e2650cd701ae71c68080f9dbbdc1
    success!
    remaining products:  1765 
    
    adding product 4ea83d951990d8bf07a68ec3e50f9156
    success!
    remaining products:  1764 
    
    adding product 2de7cf2043693db2ee898479a6e44529
    success!
    remaining products:  1763 
    
    adding product 7ba0691b7777b6581397456412a41390
    success!
    remaining products:  1762 
    
    adding product b445e314138101eecc58503e98aa2b2d
    success!
    remaining products:  1761 
    
    adding product 0084ae4bc24c0795d1e6a4f58444d39b
    success!
    remaining products:  1760 
    
    adding product 6e16656a6ee1de7232164767ccfa7920
    success!
    remaining products:  1759 
    
    adding product caa202034f268232c26fac9435f54e15
    success!
    remaining products:  1758 
    
    adding product 77c493ec14246d748db3ee8fce0092db
    success!
    remaining products:  1757 
    
    adding product 224e5e49814ca908e58c02e28a0462c1
    success!
    remaining products:  1756 
    
    adding product 52c409f1571f500e28f490a302a12540
    success!
    remaining products:  1755 
    
    adding product b89c30965ebc74912de879f22da62dbf
    success!
    remaining products:  1754 
    
    adding product 649a066d415bdda4ce2a7088292645e0
    success!
    remaining products:  1753 
    
    adding product 4da9d7b6d119db4d2d564a2197798380
    success!
    remaining products:  1752 
    
    adding product 820e694038fadbf9b60b834215b46fdb
    success!
    remaining products:  1751 
    
    adding product 4f11b55f57612f06fe9638b99f6c66e6
    success!
    remaining products:  1750 
    
    adding product 300d1539c3b6aa1793b5678b857732cf
    success!
    remaining products:  1749 
    
    adding product e1021d43911ca2c1845910d84f40aeae
    success!
    remaining products:  1748 
    
    adding product 2da6cc4a5d3a7ee43c1b3af99267ed17
    success!
    remaining products:  1747 
    
    adding product ddf9029977a61241841edeae15e9b53f
    success!
    remaining products:  1746 
    
    adding product 6915849303a3fe93657587cb9c469f00
    success!
    remaining products:  1745 
    
    adding product 9308b0d6e5898366a4a986bc33f3d3e7
    success!
    remaining products:  1744 
    
    adding product 4764f37856fc727f70b666b8d0c4ab7a
    success!
    remaining products:  1743 
    
    adding product d3802b1dc0d80d8a3c8ccc6ccc068e7c
    success!
    remaining products:  1742 
    
    adding product 20885c72ca35d75619d6a378edea9f76
    success!
    remaining products:  1741 
    
    adding product cd8d5260c8131ca7aeea5d41796d1a0a
    success!
    remaining products:  1740 
    
    adding product 38ef4b66cb25e92abe4d594acb841471
    success!
    remaining products:  1739 
    
    adding product 8068fee5f49946b3a8f85b1007cd40bb
    success!
    remaining products:  1738 
    
    adding product 40dba662fae60cd3bcceaa76a82d2873
    success!
    remaining products:  1737 
    
    adding product c563c2c394023a07d56ad6b3eb09537a
    success!
    remaining products:  1736 
    
    adding product 143758ee65fb29d30caa170c0db0ed36
    success!
    remaining products:  1735 
    
    adding product fa6c94460e902005a0b660266190c8ba
    success!
    remaining products:  1734 
    
    adding product 0e57098d0318a954d1443e2974a38fac
    success!
    remaining products:  1733 
    
    adding product 46d3f6029f6170ebccb28945964d09bf
    success!
    remaining products:  1732 
    
    adding product a9df2255ad642b923d95503b9a7958d8
    success!
    remaining products:  1731 
    
    adding product 70162fe655ec381ac6312ebf026aac54
    success!
    remaining products:  1730 
    
    adding product d35b05a832e2bb91f110d54e34e2da79
    success!
    remaining products:  1729 
    
    adding product db346ccb62d491029b590bbbf0f5c412
    success!
    remaining products:  1728 
    
    adding product 9854d7afce413aa13cd0a1d39d0bcec5
    success!
    remaining products:  1727 
    
    adding product cc58f7abf0b0cf2d5ac95ab60e4f14e9
    success!
    remaining products:  1726 
    
    adding product a1c5aff9679455a233086e26b72b9a06
    success!
    remaining products:  1725 
    
    adding product df42e2244c97a0d80d565ae8176d3351
    success!
    remaining products:  1724 
    
    adding product 31b91e3a8737fd8dd3d4b0c8c679049b
    success!
    remaining products:  1723 
    
    adding product 5463b514e21fbd3fec3772fba142a46e
    success!
    remaining products:  1722 
    
    adding product a19883fca95d0e5ec7ee6c94c6c32028
    success!
    remaining products:  1721 
    
    adding product 1e8ca836c962598551882e689265c1c5
    success!
    remaining products:  1720 
    
    adding product de3c1a733c9c51de130bc7ae775fd930
    success!
    remaining products:  1719 
    
    adding product c79ec57a8e72a87d8a69d2c6b8a2a8d4
    success!
    remaining products:  1718 
    
    adding product 6098ed616e715171f0dabad60a8e5197
    success!
    remaining products:  1717 
    
    adding product ca8a2d76a5bcc212226417361a5f0740
    success!
    remaining products:  1716 
    
    adding product 770c0e7e2af0db73409aa2431aa8f33e
    success!
    remaining products:  1715 
    
    adding product c929f2210333206f417e3862f431776d
    success!
    remaining products:  1714 
    
    adding product 0314c9b108b8c39f1cf878ed93fdd5ae
    success!
    remaining products:  1713 
    
    adding product a4d8e2a7e0d0c102339f97716d2fdfb6
    success!
    remaining products:  1712 
    
    adding product 88479e328a8633f54e9c667651832fbc
    success!
    remaining products:  1711 
    
    adding product 4f05d4821fe9967817dea5a20c4e7b35
    success!
    remaining products:  1710 
    
    adding product 5eb13cb69b6e20dd7a42030f5936a9dc
    success!
    remaining products:  1709 
    
    adding product 0777acff7c9ab34562699e4e1d05affb
    success!
    remaining products:  1708 
    
    adding product 109f91266ef89cc3690079b28abfe9a3
    success!
    remaining products:  1707 
    
    adding product d714d2c5a796d5814c565d78dd16188d
    success!
    remaining products:  1706 
    
    adding product 810dfbbebb17302018ae903e9cb7a483
    success!
    remaining products:  1705 
    
    adding product 8b5c8441a8ff8e151b191c53c1842a38
    success!
    remaining products:  1704 
    
    adding product 9f60ab2b55468f104055b16df8f69e81
    success!
    remaining products:  1703 
    
    adding product 37588c655ca22f7ca1664a2b211188ff
    success!
    remaining products:  1702 
    
    adding product bd22c2ef9e6f0fa97825c6be879f8fa4
    success!
    remaining products:  1701 
    
    adding product 18a9042b3fc5b02fe3d57fea87d6992f
    success!
    remaining products:  1700 
    
    adding product bba6bca05fecde04c682328e44b974b7
    success!
    remaining products:  1699 
    
    adding product ecf5631507a8aedcae34cef231aa7348
    success!
    remaining products:  1698 
    
    adding product f816dc0acface7498e10496222e9db10
    success!
    remaining products:  1697 
    
    adding product 174f8f613332b27e9e8a5138adb7e920
    success!
    remaining products:  1696 
    
    adding product 56c51a39a7c77d8084838cc920585bd0
    success!
    remaining products:  1695 
    
    adding product 60495b4e033e9f60b32a6607b587aadd
    success!
    remaining products:  1694 
    
    adding product 861578d797aeb0634f77aff3f488cca2
    success!
    remaining products:  1693 
    
    adding product a7f0d2b95c60161b3f3c82f764b1d1c9
    success!
    remaining products:  1692 
    
    adding product 65ae450c5536606c266f49f1c08321f2
    success!
    remaining products:  1691 
    
    adding product e038453073d221a4f32d0bab94ca7cee
    success!
    remaining products:  1690 
    
    adding product e46bc064f8e92ac2c404b9871b2a4ef2
    success!
    remaining products:  1689 
    
    adding product 2192890582189ff58ddbb2b79900f246
    success!
    remaining products:  1688 
    
    adding product adc8ca1b15e20915c3ea6008fc2f52ed
    success!
    remaining products:  1687 
    
    adding product 331cc28f8747a032890d0429b5a5f0e5
    success!
    remaining products:  1686 
    
    adding product 99296ad1eb8cd89661d163ddea3f16f1
    success!
    remaining products:  1685 
    
    adding product a5e308070bd6dd3cc56283f2313522de
    success!
    remaining products:  1684 
    
    adding product 590494d54ebe8eda5858c48f34e12b51
    success!
    remaining products:  1683 
    
    adding product 82edc5c9e21035674d481640448049f3
    success!
    remaining products:  1682 
    
    adding product 3b24156ad560a696116454056bc88ab4
    success!
    remaining products:  1681 
    
    adding product 47257279d0b4f033e373b16e65f8f089
    success!
    remaining products:  1680 
    
    adding product fd45ebc1e1d76bc1fe0ba933e60e9957
    success!
    remaining products:  1679 
    
    adding product 8ccfb1140664a5fa63177fb6e07352f0
    success!
    remaining products:  1678 
    
    adding product d40e0a2a2f466a90ee2630fc925e7af9
    success!
    remaining products:  1677 
    
    adding product 887a185b1a4080193d5cf63873ac6d70
    success!
    remaining products:  1676 
    
    adding product fd272fe04b7d4e68effd01bddcc6bb34
    success!
    remaining products:  1675 
    
    adding product ec99dd0bbd9458bc47d4b550b55aa1b2
    success!
    remaining products:  1674 
    
    adding product c5e1ab9c931df8f5e4c5a8aa53837d52
    success!
    remaining products:  1673 
    
    adding product 5d0cb12f8c9ad6845110317afc6e2183
    success!
    remaining products:  1672 
    
    adding product dc49dfebb0b00fd44aeff5c60cc1f825
    success!
    remaining products:  1671 
    
    adding product 0e139b17a92b2df7d6c3c840e51465fe
    success!
    remaining products:  1670 
    
    adding product fb8e51c5c713f2aaf71f62e03c5298db
    success!
    remaining products:  1669 
    
    adding product f0f6cc51dacebe556699ccb45e2d43a8
    success!
    remaining products:  1668 
    
    adding product b1b0ef5ba6b569680ece2fae998c4d0a
    success!
    remaining products:  1667 
    
    adding product 18a010d2a9813e91907ce88cd9143fdf
    success!
    remaining products:  1666 
    
    adding product 97e48472142cfdd1cd5d5b5ca6831cf4
    success!
    remaining products:  1665 
    
    adding product 55603a5f239e435c642244be3e891b85
    success!
    remaining products:  1664 
    
    adding product a732804c8566fc8f498947ea59a841f8
    success!
    remaining products:  1663 
    
    adding product f60ce002e5182e7b99a8a59b6d865a12
    success!
    remaining products:  1662 
    
    adding product fa612be4940bae15b019b36f9282c5ab
    success!
    remaining products:  1661 
    
    adding product 27584e8cefba0a67a8d1684d55a2a16a
    success!
    remaining products:  1660 
    
    adding product 148d411aeffed8a6f6ad4ecd77d1f904
    success!
    remaining products:  1659 
    
    adding product 11d867796d85db8cad5280ac44cec7c1
    success!
    remaining products:  1658 
    
    adding product beda24c1e1b46055dff2c39c98fd6fc1
    success!
    remaining products:  1657 
    
    adding product 40cccad2ac57c29035a432356f3c978d
    success!
    remaining products:  1656 
    
    adding product da6cb383f8f9e58f2c8af88a8c0eb65e
    success!
    remaining products:  1655 
    
    adding product 672d30ab508237ac28b92c3472c56688
    success!
    remaining products:  1654 
    
    adding product a546203962b88771bb06faf8d6ec065e
    success!
    remaining products:  1653 
    
    adding product feade1d2047977cd0cefdafc40175a99
    success!
    remaining products:  1652 
    
    adding product 1f5e7f2748adabf08629a6312ac3bfdd
    success!
    remaining products:  1651 
    
    adding product 2000f6325dfc4fc3201fc45ed01c7a5d
    success!
    remaining products:  1650 
    
    adding product 3c7417b8df0daf23f39f445e740c7a43
    success!
    remaining products:  1649 
    
    adding product e8aac01231200e7ef318b9db75c72695
    success!
    remaining products:  1648 
    
    adding product e02a35b1563d0db53486ec068ebab80f
    success!
    remaining products:  1647 
    
    adding product 64b3ec1fdfacead70c3a9bd77d824306
    success!
    remaining products:  1646 
    
    adding product 475d66314dc56a0df8fb8f7c5dbbaf78
    success!
    remaining products:  1645 
    
    adding product a6d5ab67798f3a675dc50c1d5b6c03d4
    success!
    remaining products:  1644 
    
    adding product 244edd7e85dc81602b7615cd705545f5
    success!
    remaining products:  1643 
    
    adding product d8a4e572d866aa45da78418d9d2ff9f9
    success!
    remaining products:  1642 
    
    adding product 8f04ac8eadb8a829a4c2117ade0f23da
    success!
    remaining products:  1641 
    
    adding product 50a074e6a8da4662ae0a29edde722179
    success!
    remaining products:  1640 
    
    adding product 1f187c8bc462403c4646ab271007edf4
    success!
    remaining products:  1639 
    
    adding product dc2208f9bbd11486d5dbbb9218e03017
    success!
    remaining products:  1638 
    
    adding product 65a99bb7a3115fdede20da98b08a370f
    success!
    remaining products:  1637 
    
    adding product fc8fdb29501a6289b7bc8b0bdd8155df
    success!
    remaining products:  1636 
    
    adding product 3c8a49145944fed2bbcaade178a426c4
    success!
    remaining products:  1635 
    
    adding product 88c040adb393832c87914347cc2afc3f
    success!
    remaining products:  1634 
    
    adding product a3ab4ff8fa4deed2e3bae3a5077675f0
    success!
    remaining products:  1633 
    
    adding product 103303dd56a731e377d01f6a37badae3
    success!
    remaining products:  1632 
    
    adding product 0e1feae55e360ff05fef58199b3fa521
    success!
    remaining products:  1631 
    
    adding product ab6439fa2daf0246f92eea433bca5ac4
    success!
    remaining products:  1630 
    
    adding product c89ca36e4d0430e75ca2390470a59a59
    success!
    remaining products:  1629 
    
    adding product 34f9a343f945196b66f807e0eb6249fd
    success!
    remaining products:  1628 
    
    adding product c4819d06b0ca810d38506453cfaae9d8
    success!
    remaining products:  1627 
    
    adding product ef0eff6088e2ed94f6caf720239f40d5
    success!
    remaining products:  1626 
    
    adding product a75a52f7209c01df2598a77ebc4de539
    success!
    remaining products:  1625 
    
    adding product 88f0bf2899c595146bff13b20342eb6a
    success!
    remaining products:  1624 
    
    adding product d84210a75448034bcc4947005695c306
    success!
    remaining products:  1623 
    
    adding product bdf3fd65c81469f9b74cedd497f2f9ce
    success!
    remaining products:  1622 
    
    adding product 7d6548bdc0082aacc950ed35e91fcccb
    success!
    remaining products:  1621 
    
    adding product 1a15d41947a732c97943fa5624f570d7
    success!
    remaining products:  1620 
    
    adding product 7d411dca7348327b71e894c52e76eeeb
    success!
    remaining products:  1619 
    
    adding product 2ff385c6e75c56b7a5a93d9fcd0c82ee
    success!
    remaining products:  1618 
    
    adding product f8ea2e8463760785106490befc78c339
    success!
    remaining products:  1617 
    
    adding product 989652eef28bc49eec908063ba36a854
    success!
    remaining products:  1616 
    
    adding product 475fbefa9ebfba9233364533aafd02a3
    success!
    remaining products:  1615 
    
    adding product 63c4b1baf3b4460fa9936b1a20919bec
    success!
    remaining products:  1614 
    
    adding product e2f374c3418c50bc30d67d5f7454a5b4
    success!
    remaining products:  1613 
    
    adding product 573f7f25b7b1eb79a4ec6ba896debefd
    success!
    remaining products:  1612 
    
    adding product 597c7b407a02cc0a92167e7a371eca25
    success!
    remaining products:  1611 
    
    adding product 752d2c9ecfe079e5e5f3539f4d750e5c
    success!
    remaining products:  1610 
    
    adding product f110a326be6999afdeb8e7002c0ce44d
    success!
    remaining products:  1609 
    
    adding product 8620005ac78d8257435d490058c643dd
    success!
    remaining products:  1608 
    
    adding product 1dfcb07c683107f038d8c886145d097e
    success!
    remaining products:  1607 
    
    adding product 01259a0cb2431834302abe2df60a1327
    success!
    remaining products:  1606 
    
    adding product 59112692262234e3fad47fa8eabf03a4
    success!
    remaining products:  1605 
    
    adding product 72cad9e1f9ae79872b8d6ac34fc2851c
    success!
    remaining products:  1604 
    
    adding product aee1bc7fa5da061b752d0efddbd16495
    success!
    remaining products:  1603 
    
    adding product cfbc6c5cfb8a3e10fab12aa3512153df
    success!
    remaining products:  1602 
    
    adding product 26310c700ffd1b5095454f336ae96648
    success!
    remaining products:  1601 
    
    adding product 79fde5402cbc75ae0615c9ae4c335b46
    success!
    remaining products:  1600 
    
    adding product edb684859b848362ec56904286947614
    success!
    remaining products:  1599 
    
    adding product 2f3c6a4cd8af177f6456e7e51a916ff3
    success!
    remaining products:  1598 
    
    adding product 7c33e57e3dbd8a52940fa1a963aa4a4a
    success!
    remaining products:  1597 
    
    adding product 1f6419b1cbe79c71410cb320fc094775
    success!
    remaining products:  1596 
    
    adding product 09def3ebbc44ff3426b28fcd88c83554
    success!
    remaining products:  1595 
    
    adding product 3910d2e3adfd0dc2e3a048f15c11eb74
    success!
    remaining products:  1594 
    
    adding product cb3ce9b06932da6faaa7fc70d5b5d2f4
    success!
    remaining products:  1593 
    
    adding product ddac1f6f13bb372a177804adcd3b8a31
    success!
    remaining products:  1592 
    
    adding product 5739fb4e82ed5366680b13441c6adeb4
    success!
    remaining products:  1591 
    
    adding product f08b7ac8aa30a2a9ab34394e200e1a71
    success!
    remaining products:  1590 
    
    adding product d8074a35855a7f4935e3e19222d9a9eb
    success!
    remaining products:  1589 
    
    adding product 8685549650016d9e1d14bf972262450b
    success!
    remaining products:  1588 
    
    adding product 85203ae86f2de2662ca5b6d614fbe495
    success!
    remaining products:  1587 
    
    adding product cfd66e741860718ddecf1f6eabd05fc6
    success!
    remaining products:  1586 
    
    adding product 1f5795e7b93f423c397e6f7aaff80133
    success!
    remaining products:  1585 
    
    adding product 1feb4cdda5aafe2a48cbe27544cd8e4b
    success!
    remaining products:  1584 
    
    adding product c80bcf42c220b8f5c41f85344242f1b0
    success!
    remaining products:  1583 
    
    adding product 4772c1b987f1f6d8c9d4ef0f3b764f7a
    success!
    remaining products:  1582 
    
    adding product a9ad5f2808f68eea468621a04c49efe1
    success!
    remaining products:  1581 
    
    adding product 84fdbc3ac902561c00871c9b0c226756
    success!
    remaining products:  1580 
    
    adding product ac4395adcb3da3b2af3d3972d7a10221
    success!
    remaining products:  1579 
    
    adding product ecb47fbb07a752413640f82a945530f8
    success!
    remaining products:  1578 
    
    adding product d33174c464c877fb03e77efdab4ae804
    success!
    remaining products:  1577 
    
    adding product b52340b4de4566b804c9880aa0b4af5f
    success!
    remaining products:  1576 
    
    adding product eafc8fe9c61d6760ae284c29840bbf0b
    success!
    remaining products:  1575 
    
    adding product c5c1bda1194f9423d744e0ef67df94ee
    success!
    remaining products:  1574 
    
    adding product 2912bbeedc16c67bd0529ab7d438c1ac
    success!
    remaining products:  1573 
    
    adding product 45c68484c6fc509cb25bdfca881e5cd8
    success!
    remaining products:  1572 
    
    adding product a821a161aa4214f5ff5b8ca372960ebb
    success!
    remaining products:  1571 
    
    adding product 1cc8a8ea51cd0adddf5dab504a285915
    success!
    remaining products:  1570 
    
    adding product f7dafc45da369f8581fdf3bd599075aa
    success!
    remaining products:  1569 
    
    adding product a5bad363fc47f424ddf5091c8471480a
    success!
    remaining products:  1568 
    
    adding product b112ca4087d668785e947a57493d1740
    success!
    remaining products:  1567 
    
    adding product 810bf83c7adfd8a04ac1f11508bab9e0
    success!
    remaining products:  1566 
    
    adding product 187acf7982f3c169b3075132380986e4
    success!
    remaining products:  1565 
    
    adding product 084afd913ab1e6ea58b8ca73f6cb41a6
    success!
    remaining products:  1564 
    
    adding product 7e6ff0205749bc6025b51155e26f6ced
    success!
    remaining products:  1563 
    
    adding product e88f243bf341ded9b4ced444795c3f17
    success!
    remaining products:  1562 
    
    adding product 30d454f09b771b9f65e3eaf6e00fa7bd
    success!
    remaining products:  1561 
    
    adding product 2edfeadfe636973b42d7b6ac315b896c
    success!
    remaining products:  1560 
    
    adding product 7e712eefe0de44f5509b2329b9196827
    success!
    remaining products:  1559 
    
    adding product f720ec3e5486f090fd382b68e230b435
    success!
    remaining products:  1558 
    
    adding product 0a934ecab584f7a4cd0220a7caeccbcc
    success!
    remaining products:  1557 
    
    adding product 40cb228987243c91b2dd0b7c9c4a0856
    success!
    remaining products:  1556 
    
    adding product 20546457187cf3d52ea86538403e47cc
    success!
    remaining products:  1555 
    
    adding product 274a10ffa06e434f2a94df765cac6bf4
    success!
    remaining products:  1554 
    
    adding product 723dadb8c699bf14f74503dbcb6e09c1
    success!
    remaining products:  1553 
    
    adding product 7a9a322cbe0d06a98667fdc5160dc6f8
    success!
    remaining products:  1552 
    
    adding product 001ab2fa029c064a45e41f8b2644a292
    success!
    remaining products:  1551 
    
    adding product 1770ae9e1b6bc9f5fd2841f141557ffb
    success!
    remaining products:  1550 
    
    adding product 3a4496776767aaa99f9804d0905fe584
    success!
    remaining products:  1549 
    
    adding product 769c3bce651ce5feaa01ce3b75986420
    success!
    remaining products:  1548 
    
    adding product d5036c64412973d610202be8dce2b82a
    success!
    remaining products:  1547 
    
    adding product 522e1ea43810e90242942ccc0995dae1
    success!
    remaining products:  1546 
    
    adding product 54c3d58c5efcf59ddeb7486b7061ea5a
    success!
    remaining products:  1545 
    
    adding product 0e4ceef65add6cf21c0f3f9da53b71c0
    success!
    remaining products:  1544 
    
    adding product 1b33d16fc562464579b7199ca3114982
    success!
    remaining products:  1543 
    
    adding product 68abef8ee1ac9b664a90b0bbaff4f770
    success!
    remaining products:  1542 
    
    adding product 4a4526b1ec301744aba9526d78fcb2a6
    success!
    remaining products:  1541 
    
    adding product 956685427c5cd9dcb04f784272727336
    success!
    remaining products:  1540 
    
    adding product 86a2f353e1e6692c05fe83d6fc79cf9d
    success!
    remaining products:  1539 
    
    adding product cf8d8c66b1212720e569b0bd67695451
    success!
    remaining products:  1538 
    
    adding product e7e8f8e5982b3298c8addedf6811d500
    success!
    remaining products:  1537 
    
    adding product b64a70760bb75e3ecfd1ad86d8f10c88
    success!
    remaining products:  1536 
    
    adding product 4fb8a7a22a82c80f2c26fe6c1e0dcbb3
    success!
    remaining products:  1535 
    
    adding product 427e3427c5f38a41bb9cb26525b22fba
    success!
    remaining products:  1534 
    
    adding product 502cbcfede9f1df5528af4204f33e0c8
    success!
    remaining products:  1533 
    
    adding product 50d2d2262762648589b1943078712aa6
    success!
    remaining products:  1532 
    
    adding product 8c53d30ad023ce50140181f713059ddf
    success!
    remaining products:  1531 
    
    adding product 2458ab18be2a140a1cfb932dd96f25d6
    success!
    remaining products:  1530 
    
    adding product 3e441eec3456b703a4fe741005f3981f
    success!
    remaining products:  1529 
    
    adding product 34186e9eb70e30487210b962e867b742
    success!
    remaining products:  1528 
    
    adding product f6a4f71e72dfe084f2d4b5bf96963e02
    success!
    remaining products:  1527 
    
    adding product 8cbe9ce23f42628c98f80fa0fac8b19a
    success!
    remaining products:  1526 
    
    adding product 33b9c7c18ec3acc3747c41e70e9bb3d6
    success!
    remaining products:  1525 
    
    adding product 34adeb8e3242824038aa65460a47c29e
    success!
    remaining products:  1524 
    
    adding product 4fe5149039b52765bde64beb9f674940
    success!
    remaining products:  1523 
    
    adding product 94bb077f18daa6620efa5cf6e6f178d2
    success!
    remaining products:  1522 
    
    adding product 10ff0b5e85e5b85cc3095d431d8c08b4
    success!
    remaining products:  1521 
    
    adding product 240ac9371ec2671ae99847c3ae2e6384
    success!
    remaining products:  1520 
    
    adding product e140dbab44e01e699491a59c9978b924
    success!
    remaining products:  1519 
    
    adding product b28d7c6b6aec04f5525b453411ff4336
    success!
    remaining products:  1518 
    
    adding product 2fd5d41ec6cfab47e32164d5624269b1
    success!
    remaining products:  1517 
    
    adding product f02208a057804ee16ac72ff4d3cec53b
    success!
    remaining products:  1516 
    
    adding product 853c68de7253cdd55dc37be410a45c60
    success!
    remaining products:  1515 
    
    adding product bf8dd8c68d02e161c28dc9ea139d4784
    success!
    remaining products:  1514 
    
    adding product faad95253aee7437871781018bdf3309
    success!
    remaining products:  1513 
    
    adding product a7971abb4134fc0cfcec7d589e1ebcf6
    success!
    remaining products:  1512 
    
    adding product 2e2079d63348233d91cad1fa9b1361e9
    success!
    remaining products:  1511 
    
    adding product aac933717a429f57c6ca58f32975c597
    success!
    remaining products:  1510 
    
    adding product c731077c04035ac9e92a3706288db18f
    success!
    remaining products:  1509 
    
    adding product 335cd1b90bfa4ee70b39d08a4ae0cf2d
    success!
    remaining products:  1508 
    
    adding product 4a64d913220fca4c33c140c6952688a8
    success!
    remaining products:  1507 
    
    adding product 2e855f9489df0712b4bd8ea9e2848c5a
    success!
    remaining products:  1506 
    
    adding product e1054bf2d703bca1e8fe101d3ac5efcd
    success!
    remaining products:  1505 
    
    adding product a01dfc715df9621113e91549c0ee7651
    success!
    remaining products:  1504 
    
    adding product fe663a72b27bdc613873fbbb512f6f67
    success!
    remaining products:  1503 
    
    adding product 237168031d88451c78fd8d6d6378c0b2
    success!
    remaining products:  1502 
    
    adding product 73d915c91b99b170993ea97d875a6330
    success!
    remaining products:  1501 
    
    adding product 197838c579c3b78927e0cd15ba4c9689
    success!
    remaining products:  1500 
    
    adding product 358f9e7be09177c17d0d17ff73584307
    success!
    remaining products:  1499 
    
    adding product 90fd4f88f588ae64038134f1eeaa023f
    success!
    remaining products:  1498 
    
    adding product 5a9542c773018268fc6271f7afeea969
    success!
    remaining products:  1497 
    
    adding product f5ac21cd0ef1b88e9848571aeb53551a
    success!
    remaining products:  1496 
    
    adding product 6e66329891cbb9adb1966e44379cfadf
    success!
    remaining products:  1495 
    
    adding product 91bc333f6967019ac47b49ca0f2fa757
    success!
    remaining products:  1494 
    
    adding product 48f7d3043bc03e6c48a6f0ebc0f258a8
    success!
    remaining products:  1493 
    
    adding product 2c26f9a59b0ba61233e6fc0af8e47f14
    success!
    remaining products:  1492 
    
    adding product 6a6610feab86a1f294dbbf5855c74af9
    success!
    remaining products:  1491 
    
    adding product 91a4d5c9c78d0de89b38ff408f49f39c
    success!
    remaining products:  1490 
    
    adding product 173f0f6bb0ee97cf5098f73ee94029d4
    success!
    remaining products:  1489 
    
    adding product fb642b781020b2aaeb1a7cee29acc915
    success!
    remaining products:  1488 
    
    adding product a6da680bcb675843419f2ba88a7ab7b8
    success!
    remaining products:  1487 
    
    adding product cb8a08a240f3ea7c99b220d24f54f477
    success!
    remaining products:  1486 
    
    adding product 8452a95c40e2b232acd9b8a8712935d7
    success!
    remaining products:  1485 
    
    adding product c6c61abda705fbc0728c076d60ed74b8
    success!
    remaining products:  1484 
    
    adding product eb30fa42eeb3bf429cb7e3a3d7061b3b
    success!
    remaining products:  1483 
    
    adding product 9c22c0b51b3202246463e986c7e205df
    success!
    remaining products:  1482 
    
    adding product d814f4e3dcf9e6f473e213232b9ba115
    success!
    remaining products:  1481 
    
    adding product aab11087c24981eb0e03846792ff5718
    success!
    remaining products:  1480 
    
    adding product 246a3c5544feb054f3ea718f61adfa16
    success!
    remaining products:  1479 
    
    adding product 7417744a2bac776fabe5a09b21c707a2
    success!
    remaining products:  1478 
    
    adding product a71378c0f8d76dbf90feeecd095d0ed9
    success!
    remaining products:  1477 
    
    adding product 808e53023ea4a8a9d6ecbc1290580f72
    success!
    remaining products:  1476 
    
    adding product 5a7b238ba0f6502e5d6be14424b20ded
    success!
    remaining products:  1475 
    
    adding product 12780ea688a71dabc284b064add459a4
    success!
    remaining products:  1474 
    
    adding product 9e7ba617ad9e69b39bd0c29335b79629
    success!
    remaining products:  1473 
    
    adding product a24281a03c28fa405eb29b54ebfe5d9b
    success!
    remaining products:  1472 
    
    adding product 26b58a41da329e0cbde0cbf956640a58
    success!
    remaining products:  1471 
    
    adding product 5d78d182fd5f5510588695863d22ac27
    success!
    remaining products:  1470 
    
    adding product 6fe43269967adbb64ec6149852b5cc3e
    success!
    remaining products:  1469 
    
    adding product 5553cfaf751a4b14960b7581a20bc142
    success!
    remaining products:  1468 
    
    adding product c4bbac870026694953a91cbd99149a13
    success!
    remaining products:  1467 
    
    adding product 0b9e57c46de934cee33b0e8d1839bfc2
    success!
    remaining products:  1466 
    
    adding product bf5a1d9043100645b2067fa70d7a1ea6
    success!
    remaining products:  1465 
    
    adding product 70821a40b06f8751781d5a895357da67
    success!
    remaining products:  1464 
    
    adding product 56a3107cad6611c8337ee36d178ca129
    success!
    remaining products:  1463 
    
    adding product b7046757c3682a28c5bf2024e57678a0
    success!
    remaining products:  1462 
    
    adding product c0a5a65e55124eae3388586316a25f57
    success!
    remaining products:  1461 
    
    adding product beb04c41b45927cf7e9f8fd4bb519e86
    success!
    remaining products:  1460 
    
    adding product ac27b77292582bc293a51055bfc994ee
    success!
    remaining products:  1459 
    
    adding product 59de0175dc17bcf0f4fb5ae6ae3f444f
    success!
    remaining products:  1458 
    
    adding product 7070f9088e456682f0f84f815ebda761
    success!
    remaining products:  1457 
    
    adding product 9ceee7501ba30946fab8728aec06a9f5
    success!
    remaining products:  1456 
    
    adding product 2ba61cc3a8f44143e1f2f13b2b729ab3
    success!
    remaining products:  1455 
    
    adding product 4f714c73db5191f3a71a380cba8843ed
    success!
    remaining products:  1454 
    
    adding product 4cfe94fcc9db2f0a16ba44fa5b71d8ec
    success!
    remaining products:  1453 
    
    adding product 82debd8a12b498e765a11a8e51159440
    success!
    remaining products:  1452 
    
    adding product 662a2e96162905620397b19c9d249781
    success!
    remaining products:  1451 
    
    adding product a64a034c3cb8eac64eb46ea474902797
    success!
    remaining products:  1450 
    
    adding product 5dbc8390f17e019d300d5a162c3ce3bc
    success!
    remaining products:  1449 
    
    adding product 484a231d05ee0b8331980daf4c1749fb
    success!
    remaining products:  1448 
    
    adding product 03227b950778ab86436ff79fe975b596
    success!
    remaining products:  1447 
    
    adding product cf9819df265db90772d487d5b2cd3cf4
    success!
    remaining products:  1446 
    
    adding product 6740526b78c0b230e41ae61d8ca07cf5
    success!
    remaining products:  1445 
    
    adding product 3bc71faebe42e1639eb6fded38d714cd
    success!
    remaining products:  1444 
    
    adding product ddb1b62e0c8c0b8b020fb2a35cee6494
    success!
    remaining products:  1443 
    
    adding product 44ba1d022ff64c3e9281781b13d0eef9
    success!
    remaining products:  1442 
    
    adding product 609c5e5089a9aa967232aba2a4d03114
    success!
    remaining products:  1441 
    
    adding product 2afc4dfb14e55c6face649a1d0c1025b
    success!
    remaining products:  1440 
    
    adding product d96eed18098da6ab5c15dd856998e4bd
    success!
    remaining products:  1439 
    
    adding product 415185ea244ea2b2bedeb0449b926802
    success!
    remaining products:  1438 
    
    adding product 587b7b833034299fdd5f4b10e7dc9fca
    success!
    remaining products:  1437 
    
    adding product bdc363788b2b48c031bf406cf15aa252
    success!
    remaining products:  1436 
    
    adding product a89b71bb5227c75d463dd82a03115738
    success!
    remaining products:  1435 
    
    adding product 9d05c2d955b24bd5d20b1638156ea0ef
    success!
    remaining products:  1434 
    
    adding product 4275f89744278864da88c2fda68ec4e9
    success!
    remaining products:  1433 
    
    adding product 3b5e2c9be5002e87e0477099db5ff21b
    success!
    remaining products:  1432 
    
    adding product baed9f51d412c2514ee46a0942138ad6
    success!
    remaining products:  1431 
    
    adding product 82836ca597a373e6c3cd5ae2d466161e
    success!
    remaining products:  1430 
    
    adding product 41a6fd31aa2e75c3c6d427db3d17ea80
    success!
    remaining products:  1429 
    
    adding product d72a7ed33514158ae5e68ed6d80177b9
    success!
    remaining products:  1428 
    
    adding product d7b431b1a0cc5f032399870ff4710743
    success!
    remaining products:  1427 
    
    adding product bdcc41211aa62a8f10f26d1a2d1727bf
    success!
    remaining products:  1426 
    
    adding product 170f6aa36530c364b77ddf83a84e7351
    success!
    remaining products:  1425 
    
    adding product 517f24c02e620d5a4dac1db388664a63
    success!
    remaining products:  1424 
    
    adding product 44e6b86aeefa3eca5832a98043a7b6fa
    success!
    remaining products:  1423 
    
    adding product 7b6982e584636e6a1cda934f1410299c
    success!
    remaining products:  1422 
    
    adding product 3557a86db669836730d946052d988e46
    success!
    remaining products:  1421 
    
    adding product 8965f76632d7672e7d3cf29c87ecaa0c
    success!
    remaining products:  1420 
    
    adding product de6b1cf3fb0a3aa1244d30f7b8c29c41
    success!
    remaining products:  1419 
    
    adding product 7ec69dd44416c46745f6edd947b470cd
    success!
    remaining products:  1418 
    
    adding product 1134ac57b5b1d38b7d70c1b6feaa28cf
    success!
    remaining products:  1417 
    
    adding product ae3f58a127f6c1f12c4942432d1f246a
    success!
    remaining products:  1416 
    
    adding product 9e886e4b377efe990f936dde0d9c98ca
    success!
    remaining products:  1415 
    
    adding product 86edc93505434db5aa0e091e50e678b7
    success!
    remaining products:  1414 
    
    adding product 1c63926ebcabda26b5cdb31b5cc91efb
    success!
    remaining products:  1413 
    
    adding product bd4d08cd70f4be1982372107b3b448ef
    success!
    remaining products:  1412 
    
    adding product a10a26631d45928cb8be4ebabbee8b8d
    success!
    remaining products:  1411 
    
    adding product 97785e0500ad16c18574c64189ccf4b4
    success!
    remaining products:  1410 
    
    adding product e4270771aa8df98b3317d174170ca046
    success!
    remaining products:  1409 
    
    adding product 80f2f15983422987ea30d77bb531be86
    success!
    remaining products:  1408 
    
    adding product f3f1fa1e4348bfbebdeee8c80a04c3b9
    success!
    remaining products:  1407 
    
    adding product 5de8a36008b04a6167761fa19b61aa6c
    success!
    remaining products:  1406 
    
    adding product 6fe6a8a6e6cb710584efc4af0c34ce50
    success!
    remaining products:  1405 
    
    adding product daea32adcae6abcb548134fa98f139f9
    success!
    remaining products:  1404 
    
    adding product d0cbf1a1aa1726784df15a81ead214f7
    success!
    remaining products:  1403 
    
    adding product ac53fab47b547a0d47b77e424cf119ba
    success!
    remaining products:  1402 
    
    adding product 5291822d0636dc429e80e953c58b6a76
    success!
    remaining products:  1401 
    
    adding product b0490b85e92b64dbb5db76bf8fca6a82
    success!
    remaining products:  1400 
    
    adding product e3b6fb0fd4df098162eede3313c54a8d
    success!
    remaining products:  1399 
    
    adding product ab81265d898ef7f38a3e95fc98c2a669
    success!
    remaining products:  1398 
    
    adding product 8a4488c177d9dc8c3da7c745c89ca214
    success!
    remaining products:  1397 
    
    adding product 2b0aa0d9e30ea3a55fc271ced8364536
    success!
    remaining products:  1396 
    
    adding product 2f4ccb0f7a84f335affb418aee08a6df
    success!
    remaining products:  1395 
    
    adding product 828c3938b662961ed8f775ed638b97f2
    success!
    remaining products:  1394 
    
    adding product ba2030d9a88b7db99edb3da67200167c
    success!
    remaining products:  1393 
    
    adding product ac71e0079799a57cc6616312cbbbaf84
    success!
    remaining products:  1392 
    
    adding product c34a7191f6e9948068b83e7179ea3da8
    success!
    remaining products:  1391 
    
    adding product 63a8f9e307f0bf4473c24dd4db17cebd
    success!
    remaining products:  1390 
    
    adding product b476828992f393a09339cf6270d30aa8
    success!
    remaining products:  1389 
    
    adding product 1959eb9d5a0f7ebc58ebde81d5df400d
    success!
    remaining products:  1388 
    
    adding product d0f4dae80c3d0277922f8371d5827292
    success!
    remaining products:  1387 
    
    adding product 167ccbe15cc1664c9a63c20ac4c6a55a
    success!
    remaining products:  1386 
    
    adding product 9be681ea06f52111e4c1ef99d3763770
    success!
    remaining products:  1385 
    
    adding product 1db3fa8e5bbd04882892f478a301a311
    success!
    remaining products:  1384 
    
    adding product c7b3f097f4810cbb3c4b18c09ab893bc
    success!
    remaining products:  1383 
    
    adding product 8763d72bba4a7ade23f9ae1f09f4efc7
    success!
    remaining products:  1382 
    
    adding product 5d55e7c13b0f4d7cf9d5d55d3af329c8
    success!
    remaining products:  1381 
    
    adding product 5f11b27f131494a1c014fcced2f13165
    success!
    remaining products:  1380 
    
    adding product 1dacb10f0623c67cb7dbb37587d8b38a
    success!
    remaining products:  1379 
    
    adding product 1ce4fe042832e6bd7d06697a43055373
    success!
    remaining products:  1378 
    
    adding product 077b83af57538aa183971a2fe0971ec1
    success!
    remaining products:  1377 
    
    adding product f610a13de080fb8df6cf972fc01ad93f
    success!
    remaining products:  1376 
    
    adding product 60106888f8977b71e1f15db7bc9a88d1
    success!
    remaining products:  1375 
    
    adding product ccdf3864e2fa9089f9eca4fc7a48ea0a
    success!
    remaining products:  1374 
    
    adding product 07bba581a2dd8d098a3be0f683560643
    success!
    remaining products:  1373 
    
    adding product 7ee6f2b3b68a212d3b7a4f6557eb8cc7
    success!
    remaining products:  1372 
    
    adding product 85d6e9c8255c0364fb67b5ac8a25eea3
    success!
    remaining products:  1371 
    
    adding product 767d01b4bac1a1e8824c9b9f7cc79a04
    success!
    remaining products:  1370 
    
    adding product fdc0eb412a84fa549afe68373d9087e9
    success!
    remaining products:  1369 
    
    adding product c344336196d5ec19bd54fd14befdde87
    success!
    remaining products:  1368 
    
    adding product e0ae4561193dbf6e4cf7e8f4006948e3
    success!
    remaining products:  1367 
    
    adding product 927e838a450e2fe6225edfc3d12e2463
    success!
    remaining products:  1366 
    
    adding product 055e31fa43e652cb4ab6c0ee845c8d36
    success!
    remaining products:  1365 
    
    adding product fc95fa5740ba01a870cfa52f671fe1e4
    success!
    remaining products:  1364 
    
    adding product 8973ba741e7bd6450d8023552f43728e
    success!
    remaining products:  1363 
    
    adding product f9fd5ec4c141a95257aa99ef1b590672
    success!
    remaining products:  1362 
    
    adding product 367147f1755502d9bc6189f8e2c3005d
    success!
    remaining products:  1361 
    
    adding product 05ae14d7ae387b93370d142d82220f1b
    success!
    remaining products:  1360 
    
    adding product 148148d62be67e0916a833931bd32b26
    success!
    remaining products:  1359 
    
    adding product 6d96718a701f5bfba283bbdc71dfa5c4
    success!
    remaining products:  1358 
    
    adding product 84a955d5ff75f508ec01007bc2b9b301
    success!
    remaining products:  1357 
    
    adding product eddeb82df22554fa67c641e3f8a25566
    success!
    remaining products:  1356 
    
    adding product 3b199f42a9909061516b6ce6d334af6d
    success!
    remaining products:  1355 
    
    adding product 2f0928c25ff3f884e8d2fa38835bd328
    success!
    remaining products:  1354 
    
    adding product 233f1dd0f3f537bcb7a338ea74d63483
    success!
    remaining products:  1353 
    
    adding product 094366eaa7a4b5d7f9ed227f212b3649
    success!
    remaining products:  1352 
    
    adding product baeabb8ff01160eec0b5db7da2805f57
    success!
    remaining products:  1351 
    
    adding product 680390c55bbd9ce416d1d69a9ab4760d
    success!
    remaining products:  1350 
    
    adding product 631e9c01c190fc1515b9fe3865abbb15
    success!
    remaining products:  1349 
    
    adding product fccc64972a9468a11f125cadb090e89e
    success!
    remaining products:  1348 
    
    adding product fd45c64e026040dbcb83395829d2aea5
    success!
    remaining products:  1347 
    
    adding product 569ff987c643b4bedf504efda8f786c2
    success!
    remaining products:  1346 
    
    adding product 37d7902cb2d3de686e497e31624d82e0
    success!
    remaining products:  1345 
    
    adding product e3b80d30a727c738f3cff0941f6bc55a
    success!
    remaining products:  1344 
    
    adding product b56ea7b6aa77f6f9008bc9362fab3597
    success!
    remaining products:  1343 
    
    adding product 4c9d1fbce4890fc2731b6a61262313b1
    success!
    remaining products:  1342 
    
    adding product 58ee2794cc87707943624dc8db2ff5a0
    success!
    remaining products:  1341 
    
    adding product 838aac83e00e8c5ca0f839c96d6cb3be
    success!
    remaining products:  1340 
    
    adding product 2e9777b99786a3ef6e5d786e2bc2e16f
    success!
    remaining products:  1339 
    
    adding product 5f8a7deb15235a128fcd99ad6bfde11e
    success!
    remaining products:  1338 
    
    adding product b1b20d09041289e6c3fbb81850c5da54
    success!
    remaining products:  1337 
    
    adding product 0d770c496aa3da6d2c3f2bd19e7b9d6b
    success!
    remaining products:  1336 
    
    adding product 3413ce14d52b87557e87e2c1518c2cbe
    success!
    remaining products:  1335 
    
    adding product 95e1533eb1b20a97777749fb94fdb944
    success!
    remaining products:  1334 
    
    adding product 34e420f6e47d96669897a45586997a57
    success!
    remaining products:  1333 
    
    adding product 2cfa47a65809ea0496bbf9aa363dc5da
    success!
    remaining products:  1332 
    
    adding product 7bec7e63a493e2d61891b1e4051ef75a
    success!
    remaining products:  1331 
    
    adding product 56880339cfb8fe04c2d17c6160d0512f
    success!
    remaining products:  1330 
    
    adding product 46384036044a604b6b3316fc167fc15f
    success!
    remaining products:  1329 
    
    adding product c1d53b7a97707b5cd1815c8d228d8ef1
    success!
    remaining products:  1328 
    
    adding product 29586cb449c90e249f1f09a0a4ee245a
    success!
    remaining products:  1327 
    
    adding product 27b09e189a405b6cca6ddd7ec869c143
    success!
    remaining products:  1326 
    
    adding product 392526094bcba21af9fd4102ce5ed092
    success!
    remaining products:  1325 
    
    adding product f5a14d4963acf488e3a24780a84ac96c
    success!
    remaining products:  1324 
    
    adding product 654516d1b4df6917094de807156adc14
    success!
    remaining products:  1323 
    
    adding product 3d36c07721a0a5a96436d6c536a132ec
    success!
    remaining products:  1322 
    
    adding product 03c874ab55baa3c1f835d108415fac44
    success!
    remaining products:  1321 
    
    adding product f169b1a771215329737c91f70b5bf05c
    success!
    remaining products:  1320 
    
    adding product c2f599841f21aaefeeabd2a60ef7bfe8
    success!
    remaining products:  1319 
    
    adding product e0cd3f16f9e883ca91c2a4c24f47b3d9
    success!
    remaining products:  1318 
    
    adding product 90cc440b1b8caa520c562ac4e4bbcb51
    success!
    remaining products:  1317 
    
    adding product 1a260649dac0ddb2290f609a13f4b814
    success!
    remaining products:  1316 
    
    adding product 0e1418311a013ebb344e7fcf8d199cc3
    success!
    remaining products:  1315 
    
    adding product 9a0684d9dad4967ddd09594511de2c52
    success!
    remaining products:  1314 
    
    adding product 196894366d827c56344bfe5186dbcf64
    success!
    remaining products:  1313 
    
    adding product 2e3ae207832305b6a0bff2dbc8a18b90
    success!
    remaining products:  1312 
    
    adding product bcb7c13ff9746a60fa8c3e748acd054d
    success!
    remaining products:  1311 
    
    adding product b691334ccf10d4ab144d672f7783c8a3
    success!
    remaining products:  1310 
    
    adding product a87c11b9100c608b7f8e98cfa316ff7b
    success!
    remaining products:  1309 
    
    adding product d4cd91e80f36f8f3103617ded9128560
    success!
    remaining products:  1308 
    
    adding product a1b63b36ba67b15d2f47da55cdb8018d
    success!
    remaining products:  1307 
    
    adding product 146389f11f0e76cbc28ca267a34353a7
    success!
    remaining products:  1306 
    
    adding product 619953730129049907919279f29bd9d7
    success!
    remaining products:  1305 
    
    adding product 89d3d7800304002cd469f0c402bd3ea0
    success!
    remaining products:  1304 
    
    adding product 9381fc93ad66f9ec4b2eef71147a6665
    success!
    remaining products:  1303 
    
    adding product 2118d8a1b7004ed5baf5347a4f99f502
    success!
    remaining products:  1302 
    
    adding product 0e9b734aa25ca8096cb7b56dc0dd8929
    success!
    remaining products:  1301 
    
    adding product 14eac0d254a6ccaf9b67584c7830a5c0
    success!
    remaining products:  1300 
    
    adding product 5e15fb59326e7a9c3d6558ca74621683
    success!
    remaining products:  1299 
    
    adding product 6ffcc0d3641930e3d8980ec43343ccc5
    success!
    remaining products:  1298 
    
    adding product 5ef78f63ba22e7dfb2fa44613311b932
    success!
    remaining products:  1297 
    
    adding product b58f7d184743106a8a66028b7a28937c
    success!
    remaining products:  1296 
    
    adding product 90365351ccc7437a1309dc64e4db32a3
    success!
    remaining products:  1295 
    
    adding product 0b7a9d54deeb611edc4540d286e9a042
    success!
    remaining products:  1294 
    
    adding product 8091588a3968da46e3e43a76bf3b3a98
    success!
    remaining products:  1293 
    
    adding product cdf66a6a7a04d87d865335701790c3e3
    success!
    remaining products:  1292 
    
    adding product 8e77b3768b440a281c5101ca7941d5e0
    success!
    remaining products:  1291 
    
    adding product 8830c97ab60254cd05628c6e61e8c54c
    success!
    remaining products:  1290 
    
    adding product 288cd2567953f06e460a33951f55daaf
    success!
    remaining products:  1289 
    
    adding product 49562478de4c54fafd4ec46fdb297de5
    success!
    remaining products:  1288 
    
    adding product 9fb4651c05b2ed70fba5afe0b039a550
    success!
    remaining products:  1287 
    
    adding product 33b3214d792caf311e1f00fd22b392c5
    success!
    remaining products:  1286 
    
    adding product 342285bb2a8cadef22f667eeb6a63732
    success!
    remaining products:  1285 
    
    adding product e1dc4bf1f94e87fdfeb2d91ae3dc10ef
    success!
    remaining products:  1284 
    
    adding product 0b07c4aabfdc28bb8236e195c401e1b2
    success!
    remaining products:  1283 
    
    adding product 88ccf9a98e8519b2011df33952832f2f
    success!
    remaining products:  1282 
    
    adding product cc5f52c98723a2349c97bfe4c00fd799
    success!
    remaining products:  1281 
    
    adding product 198dd5fb9c43b2d29a548f8c77e85cf9
    success!
    remaining products:  1280 
    
    adding product 894a9b94bcc5969b60bd18e8ea9c0ddc
    success!
    remaining products:  1279 
    
    adding product ab49ef78e2877bfd2c2bfa738e459bf0
    success!
    remaining products:  1278 
    
    adding product 3d387d2612f9027154ed3b99a7427da1
    success!
    remaining products:  1277 
    
    adding product def130d0b67eb38b7a8f4e7121ed432c
    success!
    remaining products:  1276 
    
    adding product 865bf46435bd84fa5d89f64cf3ba7347
    success!
    remaining products:  1275 
    
    adding product 5523d651bfb642be33057a3b78d02c9e
    success!
    remaining products:  1274 
    
    adding product 6a711a119a8a7a9f877b5f379bfe9ea2
    success!
    remaining products:  1273 
    
    adding product 01922cbeae89ad4d79ab769e84e7c5da
    success!
    remaining products:  1272 
    
    adding product 5227fa9a19dce7ba113f50a405dcaf09
    success!
    remaining products:  1271 
    
    adding product ad0f7a25211abc3889cb0f420c85e671
    success!
    remaining products:  1270 
    
    adding product 44a6a9fee78f2bb58e758a209df95f1a
    success!
    remaining products:  1269 
    
    adding product 400e5e6a7ce0c754f281525fae75a873
    success!
    remaining products:  1268 
    
    adding product 7cc538b1337957dae283c30ad46def38
    success!
    remaining products:  1267 
    
    adding product ec24a54d62ce57ba93a531b460fa8d18
    success!
    remaining products:  1266 
    
    adding product a85edfa24307bad582dbfb9713d7eb6b
    success!
    remaining products:  1265 
    
    adding product a667f4e7b0c8a3babe331569d3eac6bd
    success!
    remaining products:  1264 
    
    adding product 95f2b84de5660ddf45c8a34933a2e66f
    success!
    remaining products:  1263 
    
    adding product a7b7e4b27722574c611fe91476a50238
    success!
    remaining products:  1262 
    
    adding product 1c336b8080f82bcc2cd2499b4c57261d
    success!
    remaining products:  1261 
    
    adding product 488e4104520c6aab692863cc1dba45af
    success!
    remaining products:  1260 
    
    adding product 524f141e189d2a00968c3d48cadd4159
    success!
    remaining products:  1259 
    
    adding product e7ac288b0f2d41445904d071ba37aaff
    success!
    remaining products:  1258 
    
    adding product a1e865a9b1065392ed6035d8ccd072d9
    success!
    remaining products:  1257 
    
    adding product 4d630f9347177b17ec7a362f19489239
    success!
    remaining products:  1256 
    
    adding product c5c3d4fe6b2cc463c7d7ecba17cc9de7
    success!
    remaining products:  1255 
    
    adding product 1b90614883e606d5621b45c14f4f2963
    success!
    remaining products:  1254 
    
    adding product e42ad93f7491f2a38ba2ec1416ef6f55
    success!
    remaining products:  1253 
    
    adding product f78688fb6a5507413ade54a230355acd
    success!
    remaining products:  1252 
    
    adding product d7e77c835af3d2a803c1cf28d60575bc
    success!
    remaining products:  1251 
    
    adding product ad5ab36761669d6eadbaee691c4a1d22
    success!
    remaining products:  1250 
    
    adding product 7c05147f3029c97ce26c0cb0b2469fca
    success!
    remaining products:  1249 
    
    adding product 8e19a39c36b8e5e3afd2a3b2692aea96
    success!
    remaining products:  1248 
    
    adding product 11e2ad6bf99300cd3808bb105b55d4b8
    success!
    remaining products:  1247 
    
    adding product 0e915db6326b6fb6a3c56546980a8c93
    success!
    remaining products:  1246 
    
    adding product ddd993b2fef3fdff101872bb03cfded8
    success!
    remaining products:  1245 
    
    adding product ad1f8bb9b51f023cdc80cf94bb615aa9
    success!
    remaining products:  1244 
    
    adding product 6459257ddab7b85bf4b57845e875e4d4
    success!
    remaining products:  1243 
    
    adding product ebbac19a6a88726ff7927a79610bf6be
    success!
    remaining products:  1242 
    
    adding product 5e98d23afe19a774d1b2dcbefd5103eb
    success!
    remaining products:  1241 
    
    adding product e6be4c22a5963ab00dfe8f3b695b5332
    success!
    remaining products:  1240 
    
    adding product 06358599b7afb2506e063c1ea0a09fbd
    success!
    remaining products:  1239 
    
    adding product 176bf6219855a6eb1f3a30903e34b6fb
    success!
    remaining products:  1238 
    
    adding product 9559fc73b13fa721a816958488a5b449
    success!
    remaining products:  1237 
    
    adding product 94c4dd41f9dddce696557d3717d98d82
    success!
    remaining products:  1236 
    
    adding product f1b8b7b3ceb65c188dcdc0851634cadf
    success!
    remaining products:  1235 
    
    adding product 9cd78264cf2cd821ba651485c111a29a
    success!
    remaining products:  1234 
    
    adding product 7ef2f13f0e9d3478d7c36f6483d38a86
    success!
    remaining products:  1233 
    
    adding product c70341de2c112a6b3496aec1f631dddd
    success!
    remaining products:  1232 
    
    adding product caaa29eab72b231b0af62fbdff89bfce
    success!
    remaining products:  1231 
    
    adding product ef2ee09ea9551de88bc11fd7eeea93b0
    success!
    remaining products:  1230 
    
    adding product 10ffbba2ec9025b945acc154f3403aec
    success!
    remaining products:  1229 
    
    adding product 4a300d3a0ae99b58b0dfcd3fde526bf5
    success!
    remaining products:  1228 
    
    adding product 33cf42b38bbcf1dd6ba6b0f0cd005328
    success!
    remaining products:  1227 
    
    adding product f75b757d3459c3e93e98ddab7b903938
    success!
    remaining products:  1226 
    
    adding product 6a783b626a6d892a132dc195e5504272
    success!
    remaining products:  1225 
    
    adding product 312f1ba2a72318edaaa995a67835fad5
    success!
    remaining products:  1224 
    
    adding product cc06a6150b92e17dd3076a0f0f9d2af4
    success!
    remaining products:  1223 
    
    adding product 4edb2dc80889b1aec708ec4730f22387
    success!
    remaining products:  1222 
    
    adding product 0ea711391df8d060f4f81141e192814e
    success!
    remaining products:  1221 
    
    adding product d6539d3b57159babf6a72e106beb45bd
    success!
    remaining products:  1220 
    
    adding product 219e052492f4008818b8adb6366c7ed6
    success!
    remaining products:  1219 
    
    adding product 315b4df935f4775ef5033a4833a9e0e1
    success!
    remaining products:  1218 
    
    adding product b19d3c9e40467f65287c078ea8970b83
    success!
    remaining products:  1217 
    
    adding product cc8090c4d2791cdd9cd2cb3c24296190
    success!
    remaining products:  1216 
    
    adding product 7c93ebe873ef213123c8af4b188e7558
    success!
    remaining products:  1215 
    
    adding product c4bfbf68f5d8d0f8b9a0752ca08ea01d
    success!
    remaining products:  1214 
    
    adding product 76d7c0780ceb8fbf964c102ebc16d75f
    success!
    remaining products:  1213 
    
    adding product 57e5cb96e22546001f1d6520ff11d9ba
    success!
    remaining products:  1212 
    
    adding product abc99d6b9938aa86d1f30f8ee0fd169f
    success!
    remaining products:  1211 
    
    adding product 83187550749e6b8024a097630f9d4722
    success!
    remaining products:  1210 
    
    adding product 059d9e01176ab2f0892fe2215835bf19
    success!
    remaining products:  1209 
    
    adding product c7217b04fe11f374f9a6737901025606
    success!
    remaining products:  1208 
    
    adding product bdad073d2c77b0525e32a0e9784089ea
    success!
    remaining products:  1207 
    
    adding product 7012ef0335aa2adbab58bd6d0702ba41
    success!
    remaining products:  1206 
    
    adding product dac32839a9f0baae954b41abee610cc0
    success!
    remaining products:  1205 
    
    adding product 488b084119a1c7a4950f00706ec7ea16
    success!
    remaining products:  1204 
    
    adding product 3bf29f38421bc1764e6f1d1545479f93
    success!
    remaining products:  1203 
    
    adding product f8e6ba1db0f3c4054afec1684ba8fb26
    success!
    remaining products:  1202 
    
    adding product 9ae0a504e3af13e2e6b3ff478a8f637b
    success!
    remaining products:  1201 
    
    adding product 7e185cc0ad0a719c730af5354d7142c1
    success!
    remaining products:  1200 
    
    adding product e3a54649aeec04cf1c13907bc6c5c8aa
    success!
    remaining products:  1199 
    
    adding product 9afbe998374ca7326d35d84180786096
    success!
    remaining products:  1198 
    
    adding product 50e207ab6946b5d78b377ae0144b9e07
    success!
    remaining products:  1197 
    
    adding product 32b127307a606effdcc8e51f60a45922
    success!
    remaining products:  1196 
    
    adding product 99c83c904d0d64fbef50d919a5c66a80
    success!
    remaining products:  1195 
    
    adding product 607bc9ebe4abfcd65181bfbef6252830
    success!
    remaining products:  1194 
    
    adding product 0e674a918ebca3f78bfe02e2f387689d
    success!
    remaining products:  1193 
    
    adding product f862d13454fd267baa5fedfffb200567
    success!
    remaining products:  1192 
    
    adding product 2d45cbe914655ca562553cb81fdfc464
    success!
    remaining products:  1191 
    
    adding product df308fd90635b28d82558cf580c73ed9
    success!
    remaining products:  1190 
    
    adding product 032a01d83345f23883c98c540ff32fe7
    success!
    remaining products:  1189 
    
    adding product d8ea5f53c1b1eb087ac2e356253395d8
    success!
    remaining products:  1188 
    
    adding product 012a91467f210472fab4e11359bbfef6
    success!
    remaining products:  1187 
    
    adding product f7b027d45fd7484f6d0833823b98907e
    success!
    remaining products:  1186 
    
    adding product 78ccad7da4c2fc2646d1848e965794c5
    success!
    remaining products:  1185 
    
    adding product 8e50baf642bd6685e593bf238aa27051
    success!
    remaining products:  1184 
    
    adding product c96e651946818e0787d6296f69549fe1
    success!
    remaining products:  1183 
    
    adding product c4c42505a03f2e969b4c0a97ee9b34e7
    success!
    remaining products:  1182 
    
    adding product 3000311ca56a1cb93397bc676c0b7fff
    success!
    remaining products:  1181 
    
    adding product f953ad57910572bd6803da3faaa6e92b
    success!
    remaining products:  1180 
    
    adding product 0ea6f098a59fcf2462afc50d130ff034
    success!
    remaining products:  1179 
    
    adding product e44e875c12109e4fa3716c05008048b2
    success!
    remaining products:  1178 
    
    adding product d82f9436247aa0049767b776dceab4ed
    success!
    remaining products:  1177 
    
    adding product 95c8ba4434e9db2bf3e20c639b04c56f
    success!
    remaining products:  1176 
    
    adding product e6be5b6def555465fea6d6458bd7eba5
    success!
    remaining products:  1175 
    
    adding product 94aada62f90dd50a84ca74304563d5db
    success!
    remaining products:  1174 
    
    adding product ad2972cf612acdeec0f99338a768aa05
    success!
    remaining products:  1173 
    
    adding product 7f83c19d8adc72f08f8fde30a57eef79
    success!
    remaining products:  1172 
    
    adding product b6cf334c22c8f4ce8eb920bb7b512ed0
    success!
    remaining products:  1171 
    
    adding product 442b548e816f05640dec68f497ca38ac
    success!
    remaining products:  1170 
    
    adding product ce1aad92b939420fc17005e5461e6f48
    success!
    remaining products:  1169 
    
    adding product 82273dfbbc9cc64149d6e6d52d3104fa
    success!
    remaining products:  1168 
    
    adding product 445e24b5f22cacb9d51a837c10e91a3f
    success!
    remaining products:  1167 
    
    adding product 99a401435dcb65c4008d3ad22c8cdad0
    success!
    remaining products:  1166 
    
    adding product 8be6adae5ae0e157014d7d250870f212
    success!
    remaining products:  1165 
    
    adding product e0f48a1058f0f0204b22d4a2fd6f18ae
    success!
    remaining products:  1164 
    
    adding product fe2b952bd6b9030970b3866b328bd9c7
    success!
    remaining products:  1163 
    
    adding product 178b0113689dce8a7e48360c3886dc99
    success!
    remaining products:  1162 
    
    adding product 9e6adb1432c4a75a33d48693328e4159
    success!
    remaining products:  1161 
    
    adding product 987b75e2727ae55289abd70d3f5864e6
    success!
    remaining products:  1160 
    
    adding product d11509055cea2caaa57bc2abe499b3e5
    success!
    remaining products:  1159 
    
    adding product 2e7638c6f7667569fe469fec28c7405b
    success!
    remaining products:  1158 
    
    adding product 57db7d68d5335b52d5153a4e01adaa6b
    success!
    remaining products:  1157 
    
    adding product fdda6e957f1e5ee2f3b311fe4f145ae1
    success!
    remaining products:  1156 
    
    adding product c7a2af589d255231c76944fe4a45a500
    success!
    remaining products:  1155 
    
    adding product 2983e3047c0c730d3b7c022584717f3f
    success!
    remaining products:  1154 
    
    adding product e5e6851e7f7ffd3530e7389e183aa468
    success!
    remaining products:  1153 
    
    adding product 30d0da2f0929084d504baaec38fe28cd
    success!
    remaining products:  1152 
    
    adding product 62ac9cd1eac1b6b1d204d458ee016173
    success!
    remaining products:  1151 
    
    adding product c2f8e6f7f5a740e5b753357c9bb2c664
    success!
    remaining products:  1150 
    
    adding product fcdf698a5d673435e0a5a6f9ffea05ca
    success!
    remaining products:  1149 
    
    adding product af88d16112663ef32519c582073f44c4
    success!
    remaining products:  1148 
    
    adding product 6a130f1dc6f0c829f874e92e5458dced
    success!
    remaining products:  1147 
    
    adding product 088660d31e3314b1c5817fa45e9f25f1
    success!
    remaining products:  1146 
    
    adding product 236f119f58f5fd102c5a2ca609fdcbd8
    success!
    remaining products:  1145 
    
    adding product 437d46a857214c997956eaf0e3b21a55
    success!
    remaining products:  1144 
    
    adding product 25702d4234f4c7dc542adde64426a7ca
    success!
    remaining products:  1143 
    
    adding product b56522cb95aa89c207e129509362cce3
    success!
    remaining products:  1142 
    
    adding product 4be2c8f27b8a420492f2d44463933eb6
    success!
    remaining products:  1141 
    
    adding product 8a7cf65139a9fbb34f03b046d8dc597c
    success!
    remaining products:  1140 
    
    adding product 4ab50afd6dcc95fcba76d0fe04295632
    success!
    remaining products:  1139 
    
    adding product 82674fc29bc0d9895cee346548c2cb5c
    success!
    remaining products:  1138 
    
    adding product e77910ebb93b511588557806310f78f1
    success!
    remaining products:  1137 
    
    adding product 229aeb9e2ae66f2fac1149e5240b2fdd
    success!
    remaining products:  1136 
    
    adding product 2ec0274c1774841e6820ad9339b81dbf
    success!
    remaining products:  1135 
    
    adding product 19d47109e3c9e2c1423eac228aff27d1
    success!
    remaining products:  1134 
    
    adding product 09853c7fb1d3f8ee67a61b6bf4a7f8e6
    success!
    remaining products:  1133 
    
    adding product cfe8504bda37b575c70ee1a8276f3486
    success!
    remaining products:  1132 
    
    adding product 77b917da760ab9aeca583fd0bb0e1c67
    success!
    remaining products:  1131 
    
    adding product 73f124c77df247a60e1963b8ab5940da
    success!
    remaining products:  1130 
    
    adding product 1592104031ceaa405b8a103c399e2633
    success!
    remaining products:  1129 
    
    adding product 15bb63b28926cd083b15e3b97567bbea
    success!
    remaining products:  1128 
    
    adding product 0f9cfb7a9acced8a4167ea8006fdd080
    success!
    remaining products:  1127 
    
    adding product b55c86af1c55672a8792354910cd548d
    success!
    remaining products:  1126 
    
    adding product 58ec72df0caca51df569d0b497c33805
    success!
    remaining products:  1125 
    
    adding product aaf2979785deb27864047e0ea40ef1b7
    success!
    remaining products:  1124 
    
    adding product d042be1b4b72c110d21287b3dad13867
    success!
    remaining products:  1123 
    
    adding product 918f5cd5a5c0d48671d4d4fc54bab2e9
    success!
    remaining products:  1122 
    
    adding product 4b7f871c66be5ac7630c27bb5e21fe7f
    success!
    remaining products:  1121 
    
    adding product 327204b057100a1b7c574c2691c9a378
    success!
    remaining products:  1120 
    
    adding product d87aa42cd08ba8612664a73dbdb64221
    success!
    remaining products:  1119 
    
    adding product 3c0cd9bcd0686e8bc0a9047eae120cc5
    success!
    remaining products:  1118 
    
    adding product 881cb5534ac04cd691cdfa681afffb45
    success!
    remaining products:  1117 
    
    adding product 97d0e0329055e6ddaaaf2335a2509231
    success!
    remaining products:  1116 
    
    adding product e46709aa58ba51019b4e6c1b23d9ae03
    success!
    remaining products:  1115 
    
    adding product f0282b5ff85e7c9c66200d780bd7e72e
    success!
    remaining products:  1114 
    
    adding product 46ba59a6994802347d659680875fb173
    success!
    remaining products:  1113 
    
    adding product 7c9e9afa5a9dc68ccaf27d9effeb9383
    success!
    remaining products:  1112 
    
    adding product 3dde11a7673e90ad96fafd0b3b27a477
    success!
    remaining products:  1111 
    
    adding product 84cb17743002b4cfb81b0153cee648fc
    success!
    remaining products:  1110 
    
    adding product 49e863b146f3b5470ee222ee84669b1c
    success!
    remaining products:  1109 
    
    adding product 11833d4b4ec685c371ae6d1a65cc341e
    success!
    remaining products:  1108 
    
    adding product 5c7a3b81a677c639c76989610183c0e0
    success!
    remaining products:  1107 
    
    adding product 5527eaab87a00dbe1614481ef174f285
    success!
    remaining products:  1106 
    
    adding product 17a3120e4e5fbdc3cb5b5f946809b06a
    success!
    remaining products:  1105 
    
    adding product e0854e3c03ec877be65d351b90680d46
    success!
    remaining products:  1104 
    
    adding product 2cd2915e69546904e4e5d4a2ac9e1652
    success!
    remaining products:  1103 
    
    adding product ce4449660c6523b377b22a1dc2da5556
    success!
    remaining products:  1102 
    
    adding product 0da54aa0b1ee702d0c45af548b1a54c7
    success!
    remaining products:  1101 
    
    adding product 3dcaf04c357c577a857f3ffadc555f9b
    success!
    remaining products:  1100 
    
    adding product 07d2c6fd5472b9796184e152bd92a535
    success!
    remaining products:  1099 
    
    adding product 439d8c975f26e5005dcdbf41b0d84161
    success!
    remaining products:  1098 
    
    adding product ca3a856a28df7d77d948949206ff9fdf
    success!
    remaining products:  1097 
    
    adding product b4892f808f9efbd561cecbfbec3ad20d
    success!
    remaining products:  1096 
    
    adding product 838f14a84363d9a7ac1b06ad63fc6fb5
    success!
    remaining products:  1095 
    
    adding product 30de9ece7cf3790c8c39ccff1a044209
    success!
    remaining products:  1094 
    
    adding product 46c7cb50b373877fb2f8d5c4517bb969
    success!
    remaining products:  1093 
    
    adding product 0f541eccc4dc49cc19da7ca4594fad27
    success!
    remaining products:  1092 
    
    adding product 08808cfb5939be387af3c159b83c6b98
    success!
    remaining products:  1091 
    
    adding product e37d015e5d80348a275284efacdb6db5
    success!
    remaining products:  1090 
    
    adding product ac8a9143597891fc2fc2ded41a9a9ec7
    success!
    remaining products:  1089 
    
    adding product a1d4c20b182ad7137ab3606f0e3fc8a4
    success!
    remaining products:  1088 
    
    adding product a440a3d316c5614c7a9310e902f4a43e
    success!
    remaining products:  1087 
    
    adding product 2f2cd5c753d3cee48e47dbb5bbaed331
    success!
    remaining products:  1086 
    
    adding product cc225865b743ecc91c4743259813f604
    success!
    remaining products:  1085 
    
    adding product 25f09e44e51b17fb527fba402bfba5ab
    success!
    remaining products:  1084 
    
    adding product 6c250b592dc94d4de38a79db4d2b18f2
    success!
    remaining products:  1083 
    
    adding product 2d44e06a7038f2dd98f0f54c4be35e22
    success!
    remaining products:  1082 
    
    adding product 2d5c4ea4c4aaf3aea8ac8dee1df8fbe8
    success!
    remaining products:  1081 
    
    adding product 74f23f9e28cbc5ddaae8582f48642a59
    success!
    remaining products:  1080 
    
    adding product 27ef345422b300b5bc84817e0f83ca8b
    success!
    remaining products:  1079 
    
    adding product 67ff32d40fb51f1a2fd2c4f1b1019785
    success!
    remaining products:  1078 
    
    adding product bc3c4a6331a8a9950945a1aa8c95ab8a
    success!
    remaining products:  1077 
    
    adding product d9fbed9da256e344c1fa46bb46c34c5f
    success!
    remaining products:  1076 
    
    adding product 859555c74e9afd45ab771c615c1e49a6
    success!
    remaining products:  1075 
    
    adding product e6872f5bbe75073f8c7cfb93de7f6f3a
    success!
    remaining products:  1074 
    
    adding product e6d80593a7d6bb499229c85e7fa4e7ae
    success!
    remaining products:  1073 
    
    adding product f8e918489f1e0a81ff11312f4d0630c1
    success!
    remaining products:  1072 
    
    adding product becc353586042b6dbcc42c1b794c37b6
    success!
    remaining products:  1071 
    
    adding product b4d22bb574aed5fdd900a274930252f6
    success!
    remaining products:  1070 
    
    adding product 319be2a70dd4b557266c6ca2c20f6da7
    success!
    remaining products:  1069 
    
    adding product cc4af25fa9d2d5c953496579b75f6f6c
    success!
    remaining products:  1068 
    
    adding product a2b8a85a29b2d64ad6f47275bf1360c6
    success!
    remaining products:  1067 
    
    adding product 750263dbb2fb8547bdd810ee11a08c7a
    success!
    remaining products:  1066 
    
    adding product 3a9de64a6c62c8cd6ff8320bafb8452f
    success!
    remaining products:  1065 
    
    adding product 0919b5c38396c3f0c41f1112d538e42c
    success!
    remaining products:  1064 
    
    adding product 544defa9fddff50c53b71c43e0da72be
    success!
    remaining products:  1063 
    
    adding product 1a07bcc79f21590b3ed2622d5807bdd0
    success!
    remaining products:  1062 
    
    adding product 6ebb69ffbebe9fd95d160ffc29e0fe5d
    success!
    remaining products:  1061 
    
    adding product 68a64c1e5639454c3185e7ea2db9fc48
    success!
    remaining products:  1060 
    
    adding product 48dfb0e62ef53dc160c26788433c2d1a
    success!
    remaining products:  1059 
    
    adding product a9b94e2e91ee1dae4106f72c3e48880e
    success!
    remaining products:  1058 
    
    adding product 93c83a131fa0fd208e161910a17519c4
    success!
    remaining products:  1057 
    
    adding product 7d5430cf85f78c4b7aa09813b14bce0d
    success!
    remaining products:  1056 
    
    adding product 5ee5605917626676f6a285fa4c10f7b0
    success!
    remaining products:  1055 
    
    adding product e382f91e2c82c3853aeb0d3948275232
    success!
    remaining products:  1054 
    
    adding product 8f4d94fa779cb6b74225a9e26c700a39
    success!
    remaining products:  1053 
    
    adding product ee492a6c8f567fb3100b7dc9d3600cbe
    success!
    remaining products:  1052 
    
    adding product fa40b3850046b362217c121a274720fd
    success!
    remaining products:  1051 
    
    adding product 8b519f198dd26772e3e82874826b04aa
    success!
    remaining products:  1050 
    
    adding product 7cc5a75432e9a547200e3668c3761ae7
    success!
    remaining products:  1049 
    
    adding product d5c8e1ab6fc0bfeb5f29aafa999cdb29
    success!
    remaining products:  1048 
    
    adding product 85dfba75bcadb576723264b5986f2ac2
    success!
    remaining products:  1047 
    
    adding product 135593dd9bc3d98e8d8e71d788c9dda6
    success!
    remaining products:  1046 
    
    adding product 3aaa3db6a8983226601cac5dde15a26b
    success!
    remaining products:  1045 
    
    adding product d4a973e303ec37692cc8923e3148eef7
    success!
    remaining products:  1044 
    
    adding product 4afe044911ed2c247005912512ace23b
    success!
    remaining products:  1043 
    
    adding product 63771e3e7738ed3048c3dc440023db38
    success!
    remaining products:  1042 
    
    adding product 9cea886b9f44a3c2df1163730ab64994
    success!
    remaining products:  1041 
    
    adding product 44151de6be734db545ec958e77b0f9df
    success!
    remaining products:  1040 
    
    adding product 9078f2a8254704bd760460f027072e52
    success!
    remaining products:  1039 
    
    adding product 247d87b085efdb305fa6583ccf1a9f54
    success!
    remaining products:  1038 
    
    adding product 1b742ae215adf18b75449c6e272fd92d
    success!
    remaining products:  1037 
    
    adding product d30d0f522a86b3665d8e3a9a91472e28
    success!
    remaining products:  1036 
    
    adding product 71b9e42fd1490c2ee83c1bc4c4e37da3
    success!
    remaining products:  1035 
    
    adding product 30410be149e6771f60881182342452d5
    success!
    remaining products:  1034 
    
    adding product b20fa060328b0cdf51b464ee37efe182
    success!
    remaining products:  1033 
    
    adding product 8f6242793017047d373f29f270388ba9
    success!
    remaining products:  1032 
    
    adding product 1ddfa4ccdcea53130b500eaabb190e4b
    success!
    remaining products:  1031 
    
    adding product 4fc66104f8ada6257fa55f29a2a567c7
    success!
    remaining products:  1030 
    
    adding product c3d96fbd5b1b45096ff04c04038fff5d
    success!
    remaining products:  1029 
    
    adding product c404a5adbf90e09631678b13b05d9d7a
    success!
    remaining products:  1028 
    
    adding product 2f3d9b534d726b2d921451852adedb0c
    success!
    remaining products:  1027 
    
    adding product e2db7186375992e729165726762cb4c1
    success!
    remaining products:  1026 
    
    adding product f1de5100906f31712aaa5166689bfdf4
    success!
    remaining products:  1025 
    
    adding product 0c95054981de037de06e544a52eb3613
    success!
    remaining products:  1024 
    
    adding product edf0320adc8658b25ca26be5351b6c4a
    success!
    remaining products:  1023 
    
    adding product 7ae26cbe9586dea7d1f0fa372aa86811
    success!
    remaining products:  1022 
    
    adding product 00a2aa5c43a94f625ebf713cb5bfb091
    success!
    remaining products:  1021 
    
    adding product fc5a29b5d423c94cdfacb0f706eecdb7
    success!
    remaining products:  1020 
    
    adding product f2fb9d75af8f3f2eb322ff968e62a324
    success!
    remaining products:  1019 
    
    adding product a894b83c9b7a00dba6c52cecf7a31fbb
    success!
    remaining products:  1018 
    
    adding product 16bb35ba24bac33d95ee9f1f65a41b53
    success!
    remaining products:  1017 
    
    adding product 800103a4d112ae28491b249670a071ec
    success!
    remaining products:  1016 
    
    adding product 90b8e8eca90756905bf80c293ae6a50a
    success!
    remaining products:  1015 
    
    adding product 28f248e9279ac845995c4e9f8af35c2b
    success!
    remaining products:  1014 
    
    adding product 00430c0c1fae276c9713ab5f21167882
    success!
    remaining products:  1013 
    
    adding product cf2f3fe19ffba462831d7f037a07fc83
    success!
    remaining products:  1012 
    
    adding product 342c472b95d00421be10e9512b532866
    success!
    remaining products:  1011 
    
    adding product 77ae1a5da3b68dc65a9d1648242a29a7
    success!
    remaining products:  1010 
    
    adding product d4ca950da1d6fd954520c45ab19fef1c
    success!
    remaining products:  1009 
    
    adding product cef1b938860dd6718de5eaae697b60e5
    success!
    remaining products:  1008 
    
    adding product abcc5329cfe5846db63ff4dee74eb906
    success!
    remaining products:  1007 
    
    adding product bd4a6d0563e0604510989eb8f9ff71f5
    success!
    remaining products:  1006 
    
    adding product 2004e0f2b74655ee92d3a6af6bdb6626
    success!
    remaining products:  1005 
    
    adding product b6af2c9703f203a2794be03d443af2e3
    success!
    remaining products:  1004 
    
    adding product b060700f0a542a147685180b143ad61e
    success!
    remaining products:  1003 
    
    adding product 19f6a8886908b80b8b6e9f212dbeea09
    success!
    remaining products:  1002 
    
    adding product c3f4db3a634aa769c0f1161219272d03
    success!
    remaining products:  1001 
    
    adding product d47bf0af618a3523a226ed7cada85ce3
    success!
    remaining products:  1000 
    
    adding product c9e5c2b59d98488fe1070e744041ea0e
    success!
    remaining products:  999 
    
    adding product a73d2d60ed472454a0360027aa039bdb
    success!
    remaining products:  998 
    
    adding product 3d773b5ce67533d1b5b52d9b57936860
    success!
    remaining products:  997 
    
    adding product 1543ceff58b1606182e9b7cf357712b3
    success!
    remaining products:  996 
    
    adding product 10cc088a48f313ab3b1f4e6e76353dd4
    success!
    remaining products:  995 
    
    adding product 4a46fbfca3f1465a27b210f4bdfe6ab3
    success!
    remaining products:  994 
    
    adding product 0a3b5a7a477d359746061d41c3a04fd6
    success!
    remaining products:  993 
    
    adding product 9375084629cd055e6b819053bc9714de
    success!
    remaining products:  992 
    
    adding product 841b60e20ff680b0d59aa9d6902fe289
    success!
    remaining products:  991 
    
    adding product d85b63ef0ccb114d0a3bb7b7d808028f
    success!
    remaining products:  990 
    
    adding product aceacd5df18526f1d96ee1b9714e95eb
    success!
    remaining products:  989 
    
    adding product 1aa7a8773e6a7fdacbcedf9999009a38
    success!
    remaining products:  988 
    
    adding product 85ef8e895264ae2dcab7bcd0f04d9bea
    success!
    remaining products:  987 
    
    adding product 1f5f6ad95cc908a20bb7e30ee28a5958
    success!
    remaining products:  986 
    
    adding product e046ede63264b10130007afca077877f
    success!
    remaining products:  985 
    
    adding product 3941c4358616274ac2436eacf67fae05
    success!
    remaining products:  984 
    
    adding product 690f44c8c2b7ded579d01abe8fdb6110
    success!
    remaining products:  983 
    
    adding product 4be49c79f233b4f4070794825c323733
    success!
    remaining products:  982 
    
    adding product bc6d753857fe3dd4275dff707dedf329
    success!
    remaining products:  981 
    
    adding product 3eb65004054f5d21fca4087f5658c727
    success!
    remaining products:  980 
    
    adding product 42a85a0102b0a64d8737ffd2e00a57f4
    success!
    remaining products:  979 
    
    adding product 37c9216b00a111ac0e1f81de25ddff77
    success!
    remaining products:  978 
    
    adding product e7db14e12fb49c1d78a573e6e5f542c2
    success!
    remaining products:  977 
    
    adding product a9cc6694dc40736d7a2ec018ea566113
    success!
    remaining products:  976 
    
    adding product e85cc63b4f0f312f11e073fc68ccffd5
    success!
    remaining products:  975 
    
    adding product 609a199881ca4ba9c95688235cd6ac5c
    success!
    remaining products:  974 
    
    adding product 5e083bd37263c80781fff960e8f5e655
    success!
    remaining products:  973 
    
    adding product 3d9f8ee1db299aa712a029a0e3a2d6f4
    success!
    remaining products:  972 
    
    adding product 73f9ddba165b5c59c61dd64960ba8b2d
    success!
    remaining products:  971 
    
    adding product 87019fb492fe6f03c3bdb29cf2ffb6eb
    success!
    remaining products:  970 
    
    adding product ea20aed6df7caa746052d227d194a395
    success!
    remaining products:  969 
    
    adding product db5bdc8ad46ab6087d9cdfd8a8662ddf
    success!
    remaining products:  968 
    
    adding product 7dab099bfda35ad14715763b75487b47
    success!
    remaining products:  967 
    
    adding product 516341c3e8f4543c8d465b0c514a6f92
    success!
    remaining products:  966 
    
    adding product 3dcf44c3136a27a28b3bb27586fd5fc5
    success!
    remaining products:  965 
    
    adding product 77b830096c1888016b4d7a730bbe9731
    success!
    remaining products:  964 
    
    adding product 4f0bf7b7b1aca9ad15317a0b4efdca14
    success!
    remaining products:  963 
    
    adding product 9739efc4f01292e764c86caa59af353e
    success!
    remaining products:  962 
    
    adding product 915e3742d51cb477268ba29275c3ce09
    success!
    remaining products:  961 
    
    adding product eec96a7f788e88184c0e713456026f3f
    success!
    remaining products:  960 
    
    adding product 0d82627e10660af39ea7eb69c3568955
    success!
    remaining products:  959 
    
    adding product 251d52afeb09449719aa7ba0b842c755
    success!
    remaining products:  958 
    
    adding product 108670e12c6e0fcf4d959fc8d2eabef6
    success!
    remaining products:  957 
    
    adding product cb7c403aa312160380010ee3dd4bfc53
    success!
    remaining products:  956 
    
    adding product 6804c9bca0a615bdb9374d00a9fcba59
    success!
    remaining products:  955 
    
    adding product 35c1f9c50543aeedf1a3167bcb9d2756
    success!
    remaining products:  954 
    
    adding product 17d187eaf6157b4e219552d6a187290a
    success!
    remaining products:  953 
    
    adding product 7d92c08873b4979b544e7fb64fdb1c6c
    success!
    remaining products:  952 
    
    adding product 37cfff3c04f95b22bcf166df586cd7a9
    success!
    remaining products:  951 
    
    adding product 91a575b38c7c4526decc579655a2a49c
    success!
    remaining products:  950 
    
    adding product 621eb0b827c09dd1804e87bd74f79383
    success!
    remaining products:  949 
    
    adding product fbb17c69f51a5950e05e08cc14599b57
    success!
    remaining products:  948 
    
    adding product 6e17a5fd135fcaf4b49f2860c2474c7c
    success!
    remaining products:  947 
    
    adding product e345fac6bc5c868f0222430c733fa26e
    success!
    remaining products:  946 
    
    adding product a1bcb47486d5abaeabf8fc1d64abe62b
    success!
    remaining products:  945 
    
    adding product 5c8cb735a1ce65dac514233cbd5576d6
    success!
    remaining products:  944 
    
    adding product b0928f2d4ba7ea33b05024f21d937f48
    success!
    remaining products:  943 
    
    adding product 6f780a0221033e49ffec2199ba1d74b2
    success!
    remaining products:  942 
    
    adding product e3978ba7ecdecc63be5f5bf0281a0ed6
    success!
    remaining products:  941 
    
    adding product 191f8f858acda435ae0daf994e2a72c2
    success!
    remaining products:  940 
    
    adding product 919d2356219c1fa0c0bd560246532c72
    success!
    remaining products:  939 
    
    adding product 1f9702dbc66344013ffb884419665816
    success!
    remaining products:  938 
    
    adding product 6d6081760ded88d3807d3562178ecabb
    success!
    remaining products:  937 
    
    adding product 49c166931e8a70ff2a57a5780dcbb892
    success!
    remaining products:  936 
    
    adding product eea5d933e9dce59c7dd0f6532f9ea81b
    success!
    remaining products:  935 
    
    adding product 2d4027d6df9c0256b8d4474ce88f8c88
    success!
    remaining products:  934 
    
    adding product 9f6f2381bc56ef668e94f6d1fb4f6309
    success!
    remaining products:  933 
    
    adding product 76330c26dea62332de2ca7b4a9ef51ec
    success!
    remaining products:  932 
    
    adding product 29056bc4790af32aa8458e3fbc737485
    success!
    remaining products:  931 
    
    adding product 988f9153ac4fd966ea302dd9ab9bae15
    success!
    remaining products:  930 
    
    adding product c359889a833e7612e0cff1dc69d272bc
    success!
    remaining products:  929 
    
    adding product fa733611ef13bd333ebfbab7eed14b63
    success!
    remaining products:  928 
    
    adding product bf4d73f316737b26f1e860da0ea63ec8
    success!
    remaining products:  927 
    
    adding product f7fa6aca028e7ff4ef62d75ed025fe76
    success!
    remaining products:  926 
    
    adding product c9bc734c0663a142b7bec265098f8dbf
    success!
    remaining products:  925 
    
    adding product dc91d6e23d859879bbaf0a9d7f27fb77
    success!
    remaining products:  924 
    
    adding product 3f00f874e9837b0ec850a34c85432d66
    success!
    remaining products:  923 
    
    adding product c8d3a760ebab631565f8509d84b3b3f1
    success!
    remaining products:  922 
    
    adding product df1a336b7e0b0cb186de6e66800c43a9
    success!
    remaining products:  921 
    
    adding product ba3c95c2962d3aab2f6e667932daa3c5
    success!
    remaining products:  920 
    
    adding product 54f3bc04830d762a3b56a789b6ff62df
    success!
    remaining products:  919 
    
    adding product ff82db7535530637af7f8a96284b3459
    success!
    remaining products:  918 
    
    adding product 24368c745de15b3d2d6279667debcba3
    success!
    remaining products:  917 
    
    adding product 6c2fdcf862a752ca2c9e49866a05e1df
    success!
    remaining products:  916 
    
    adding product 8e5e15c4e6d09c8333a17843461041a9
    success!
    remaining products:  915 
    
    adding product 2b16a44bb65751bb0ebe5d8b42644bc4
    success!
    remaining products:  914 
    
    adding product 55063089b08df5797d3eebca7c087ed4
    success!
    remaining products:  913 
    
    adding product 959776b99b006e5785c3a3364949ce47
    success!
    remaining products:  912 
    
    adding product 73bf6c41e241e28b89d0fb9e0c82f9ce
    success!
    remaining products:  911 
    
    adding product 89562dccfeb1d0394b9ae7e09544dc70
    success!
    remaining products:  910 
    
    adding product fc0de4e0396fff257ea362983c2dda5a
    success!
    remaining products:  909 
    
    adding product 6e839dd93911f945cd02c9b15da23db0
    success!
    remaining products:  908 
    
    adding product e044fb795495fd22d8146e50b961e852
    success!
    remaining products:  907 
    
    adding product e9b82e4d55c91c6abbf9dedf898172a0
    success!
    remaining products:  906 
    
    adding product 61bfdc160e4c099203c72258d8825340
    success!
    remaining products:  905 
    
    adding product 657b96f0592803e25a4f07166fff289a
    success!
    remaining products:  904 
    
    adding product 7b647a7d88f4d6319bf0d600d168dbeb
    success!
    remaining products:  903 
    
    adding product c43aa697165c1f99d53d37d5d279f9a3
    success!
    remaining products:  902 
    
    adding product 2aac0e27587428fe2aafe882c5974a85
    success!
    remaining products:  901 
    
    adding product 4aee31b0ec9f7bb7885473d95961e9a6
    success!
    remaining products:  900 
    
    adding product 96ba4a06bae961abbfb783d9d715150c
    success!
    remaining products:  899 
    
    adding product 6872937617af85db5a39a5243e858d1f
    success!
    remaining products:  898 
    
    adding product 64314c17210c549a854f1f1c7adce8b6
    success!
    remaining products:  897 
    
    adding product ddf354219aac374f1d40b7e760ee5bb7
    success!
    remaining products:  896 
    
    adding product 17ab7b5bb7ca18f6d5f33dfbcbaee1a2
    success!
    remaining products:  895 
    
    adding product a24bdc3e59a4c624eee8318a51bb55b9
    success!
    remaining products:  894 
    
    adding product 33686c2d8930be81c843ffb7d4312605
    success!
    remaining products:  893 
    
    adding product 84562f4374b74baa0907563bccdf1492
    success!
    remaining products:  892 
    
    adding product 319a67432f51ed53938542b809320dd2
    success!
    remaining products:  891 
    
    adding product e00944d55e6432ccf20f9fda2492b6fd
    success!
    remaining products:  890 
    
    adding product ce653013fadbb2ff27530d3de3790f1b
    success!
    remaining products:  889 
    
    adding product 6c7de1f27f7de61a6daddfffbe05c058
    success!
    remaining products:  888 
    
    adding product d82d678e9583c1f5f283ec56fbf1abb7
    success!
    remaining products:  887 
    
    adding product 2c2fb9efd4b8a1f837bf47004a49ce45
    success!
    remaining products:  886 
    
    adding product 01c6fc8bc32f1237be039ceb6b4b4b2d
    success!
    remaining products:  885 
    
    adding product f1f485b532be392dd7964f94dbd0562a
    success!
    remaining products:  884 
    
    adding product a621f7ab8fd0eae3805566885dda4a25
    success!
    remaining products:  883 
    
    adding product 163e836b057fa98808f41048cba1195f
    success!
    remaining products:  882 
    
    adding product 1271475706211e282089b789f5f73b24
    success!
    remaining products:  881 
    
    adding product be7c20a83fb93c62352414aa58e525c2
    success!
    remaining products:  880 
    
    adding product f501d2693c06f905f4c210b495748a79
    success!
    remaining products:  879 
    
    adding product 5497d34aeeed8c74dc8146b00c1ed489
    success!
    remaining products:  878 
    
    adding product 2c3f3db53ca4d872f79d87ec33c8c5fd
    success!
    remaining products:  877 
    
    adding product 068bcdad6accbee0b2adc53cb82eb533
    success!
    remaining products:  876 
    
    adding product 5e5bd82a90466d9434c270b85ddf187c
    success!
    remaining products:  875 
    
    adding product e607b9b80358410a2bcdcbc7e9978ce1
    success!
    remaining products:  874 
    
    adding product 3090f54f8d8eecd6469c3a9eb3ddb48a
    success!
    remaining products:  873 
    
    adding product 82166899aa3db47b87226f5e2abfbca7
    success!
    remaining products:  872 
    
    adding product 0a688ad7351b2e3cfec7ee3112206e6b
    success!
    remaining products:  871 
    
    adding product aa6753f1f7962a29a43ffa397473774f
    success!
    remaining products:  870 
    
    adding product f6a81a05f0dc6797d195dfb9aad909bb
    success!
    remaining products:  869 
    
    adding product 4231962b766e3f90f64fa07e4fc2d5cf
    success!
    remaining products:  868 
    
    adding product 854b68b3c6c3e69b52672108c5a99010
    success!
    remaining products:  867 
    
    adding product 94f2997c96b61f9f3cd816418d376ff9
    success!
    remaining products:  866 
    
    adding product 25ca7e9577b39f7730394bc4db6b58ab
    success!
    remaining products:  865 
    
    adding product 945628f89a79f5c21148e6c85b241947
    unknown error, skipping product
    adding product f60749fa4cee4f9dfe786aba9d7be0da
    unknown error, skipping product
    adding product 4e8e25b6b415f4026f6fd44b5ddd7c76
    unknown error, skipping product
    adding product 3e8242853ca7831eb03da9c1cef03f61
    unknown error, skipping product
    adding product 69812e3f04486f27c1b6f6d2820e5c90
    unknown error, skipping product
    adding product ce737c887b558e8467dede8ca59029eb
    success!
    remaining products:  859 
    
    adding product 408567fb466fdcc5171cd962e3c83862
    success!
    remaining products:  858 
    
    adding product cd0d3644bad027859b1ec6128a36a974
    success!
    remaining products:  857 
    
    adding product 0e1b703e310abf1b3932abf6806f9c39
    success!
    remaining products:  856 
    
    adding product ecbc7b96b9ec7951187d0da02230773d
    success!
    remaining products:  855 
    
    adding product 196b8872e7be15c1fad14af539f43314
    success!
    remaining products:  854 
    
    adding product e7bb900e1736ca0d271b420ae968c9da
    unknown error, skipping product
    adding product e601ac8ec15075e6c6d0831dbd5a9c81
    unknown error, skipping product
    adding product ef7726ba08a0f64821ecd00e781680d1
    unknown error, skipping product
    adding product 738c045d6205c050d3d6c57e5e6f2d76
    unknown error, skipping product
    adding product 476c386f6e35d7cbcf9085354dd035dc
    success!
    remaining products:  849 
    
    adding product 6e69d4cad6bc8086f5aef6f2ea34b722
    success!
    remaining products:  848 
    
    adding product c82d64a97a01ac0869fcb90cd22b96c0
    success!
    remaining products:  847 
    
    adding product 842f8aa34c56102c83aaed81f3b175d8
    unknown error, skipping product
    adding product 61deee96aa0901e6edeb0a4a077ad082
    unknown error, skipping product
    adding product c0a4b0fa8862500a10f65f4d6b1d4490
    success!
    remaining products:  844 
    
    adding product 30c54fe92fc8181d33b91c6dbc8de9d7
    unknown error, skipping product
    adding product 436c869cc2e14e9a3ad092e9ea0d6bb0
    unknown error, skipping product
    adding product b94148cb773e1bebf30b1f5488a96cb7
    unknown error, skipping product
    adding product 4682b5b0e2a747661f14bb99b7c74544
    success!
    remaining products:  840 
    
    adding product e90f24cff5f0c12e4041b8e3eaa3a623
    unknown error, skipping product
    adding product f5d5acb297496b4f9bbf96c3618d828c
    unknown error, skipping product
    adding product 2a845b3e91df58cd29f565345c31d49a
    success!
    remaining products:  837 
    
    adding product ecc92a19f0de821519b715d10cbf7c62
    success!
    remaining products:  836 
    
    adding product b0cbd1803daba2f4be6919b9f773f921
    unknown error, skipping product
    adding product 29f05888c012d13d76804c9a504c7aca
    success!
    remaining products:  834 
    
    adding product 84403a769c84d34088c8b19584a235ad
    success!
    remaining products:  833 
    
    adding product 4ad039368867afcdd799870f374cea4c
    success!
    remaining products:  832 
    
    adding product b12564853249eafedf53525ef9b6b0f6
    success!
    remaining products:  831 
    
    adding product 414c073ee1379bd7bc7b332159cab1e6
    success!
    remaining products:  830 
    
    adding product b1946b34ce976b3f223d5afc2052e89d
    success!
    remaining products:  829 
    
    adding product dd2cc2088b77d344f7b5ec13e6ff4922
    success!
    remaining products:  828 
    
    adding product 4fd04fbeeb9aabf2f029c8133c4bf881
    success!
    remaining products:  827 
    
    adding product abf57562806a737f3f8456b63b50f5ff
    success!
    remaining products:  826 
    
    adding product 61204932bccb948357e1a0281de24080
    success!
    remaining products:  825 
    
    adding product 36677a1d815d4528bebf89833d168f56
    success!
    remaining products:  824 
    
    adding product 28b430b807e6a7bd1fb80cc77c9a7361
    success!
    remaining products:  823 
    
    adding product 054feb2acfd28eeca93f8f22e35cc6d1
    unknown error, skipping product
    adding product 4ca1097f8f6b3fab8f0ab9111af027da
    unknown error, skipping product
    adding product 7c3a966d88a80726a95c2e16e56c3997
    unknown error, skipping product
    adding product 78b9508436357acbab1aabb76e12739f
    success!
    remaining products:  819 
    
    adding product 4cc6cb1360a93496260ac5b9b575f37d
    unknown error, skipping product
    adding product a90ab441fabcbbac21aaf1360076bd8d
    success!
    remaining products:  817 
    
    adding product a7319a1732f803bece8314911dd6f52b
    success!
    remaining products:  816 
    
    adding product dd473ece077230d91b9340e3b4e57c11
    unknown error, skipping product
    adding product 7a5400eeb415675960d6822b6bdffb7a
    unknown error, skipping product
    adding product 4f3e1a55bdd71769da5e20b374f20f43
    unknown error, skipping product
    adding product 20a79e1ab3a172d48f5f78498616ec4d
    unknown error, skipping product
    adding product c2db5ca3f8789c124affa4023764635d
    unknown error, skipping product
    adding product 4eedf3f92629bc3b1a208cfaacc4b2d2
    unknown error, skipping product
    adding product 8e14f4de91da22108a5247cc18253a0a
    unknown error, skipping product
    adding product 92b11f86a8c3f4e663ef070a822e1d2b
    unknown error, skipping product
    adding product 6d8470222cb0e9ca1519ab84406ef2b2
    unknown error, skipping product
    adding product 5af7c96251ec5ea316631afea1456741
    success!
    remaining products:  806 
    
    adding product 3ec7b0f8bc5bf7eb2a73ea78a115d94a
    success!
    remaining products:  805 
    
    adding product 67da624214ee8e89992ce70459e669a3
    success!
    remaining products:  804 
    
    adding product 1046b70c80e922db36d9b2d6f733ce52
    success!
    remaining products:  803 
    
    adding product 6b1086f5fb6b725a975602564298d4b9
    success!
    remaining products:  802 
    
    adding product 976deaee3c65cda83c9671a3645f905a
    unknown error, skipping product
    adding product e0b43d7adf04f77c81d846f9d8eee988
    success!
    remaining products:  800 
    
    adding product 0704b08c67807572593dba22b536d449
    success!
    remaining products:  799 
    
    adding product ceee632a967af1be6e1c0a29e21bb1c5
    unknown error, skipping product
    adding product e73902cc3bc6c3bb0534870f6b8272e4
    unknown error, skipping product
    adding product 8a8aee6f4d29fa77828d4f011a237ea5
    success!
    remaining products:  796 
    
    adding product 7ba77854a96f6a3182fc526ae3018403
    unknown error, skipping product
    adding product 09bd2862fe3035ad3223816b439045e3
    success!
    remaining products:  794 
    
    adding product 987abbb509e98a9b8cc08f57363e8733
    success!
    remaining products:  793 
    
    adding product 123f7670babac5a92883d9a7afb4262e
    unknown error, skipping product
    adding product de6fa2e6abc1e244d7dc3534d3c81e2a
    unknown error, skipping product
    adding product 59349d7c8145846439782da8c2d78754
    unknown error, skipping product
    adding product 9f3fedf17034316a32b96e87686c44d9
    success!
    remaining products:  789 
    
    adding product 164ecd36bc9fc1781df239d00b004d3b
    success!
    remaining products:  788 
    
    adding product ac83d3f400e95a5d31e7c59d2743bf73
    success!
    remaining products:  787 
    
    adding product 544d86a583c877780b83a3b31e226465
    success!
    remaining products:  786 
    
    adding product a352147f3aad581bed027339ac1d5dd9
    success!
    remaining products:  785 
    
    adding product b67d084d74c3f7c0145f96a0ac4c82a8
    success!
    remaining products:  784 
    
    adding product 35c1124dd508ead6eb8c4aee9a7f5d71
    success!
    remaining products:  783 
    
    adding product 61dee0400b6103176dbc54320450c12f
    success!
    remaining products:  782 
    
    adding product 251fbd782fec91a50eb1b6050f8d7f2b
    success!
    remaining products:  781 
    
    adding product 1e5afae270de728fd14f20133233d33a
    success!
    remaining products:  780 
    
    adding product cfcb7077d0bda605e84974ded63d24c1
    success!
    remaining products:  779 
    
    adding product 15bffa22eafc325bb53266ecefb25d5c
    success!
    remaining products:  778 
    
    adding product ae490cce121bcb4989be859bcbda433a
    success!
    remaining products:  777 
    
    adding product 1084a91264d0a5d47eeb3659f9c36935
    unknown error, skipping product
    adding product 7a456d7f29b4e7e92bb9a14c24dce430
    unknown error, skipping product
    adding product caec17d84884addeec35c3610645ab63
    unknown error, skipping product
    adding product 9c26f743a1c2d7d8a27fb9e8d366d365
    unknown error, skipping product
    adding product f4401dd087608e344ac946c2f5a982e4
    unknown error, skipping product
    adding product 5d130e4cef1c50de7e168837012989b0
    unknown error, skipping product
    adding product d7ce4c420a30736d81b6a9fde18fc13c
    unknown error, skipping product
    adding product 7007052b2206fece53fd3750fb3016d0
    unknown error, skipping product
    adding product 7da3270fafd99ed8fcc9e395f4e7c181
    unknown error, skipping product
    adding product 54aba384bebf7d76045bc3dad7251d5b
    unknown error, skipping product
    adding product 7c2174131255d8e906a502237185a436
    success!
    remaining products:  766 
    
    adding product 46a62c34c7b8b0c0d02f0833df49ec20
    unknown error, skipping product
    adding product 0d7c463832b871c20405a6c9296b5517
    success!
    remaining products:  764 
    
    adding product 851e8eaf4988ed55c3d335ea8d5ed61b
    success!
    remaining products:  763 
    
    adding product a5ae76409740d5b7536719ff1d14cb1f
    success!
    remaining products:  762 
    
    adding product 45a042358c47c0059ee86d8508dfcbec
    unknown error, skipping product
    adding product 89bdedf8c38bda669ba5aba697d7703b
    unknown error, skipping product
    adding product 82bdd6d74c304d5130239833c88d2f18
    success!
    remaining products:  759 
    
    adding product a401bed218424c069af5121745e2c46f
    success!
    remaining products:  758 
    
    adding product f9dd94e7acd400658ac4fd2817ea4fef
    success!
    remaining products:  757 
    
    adding product 92f57807d7a38f80a3b0f7fd4b639da9
    success!
    remaining products:  756 
    
    adding product 7b061988b655fa9f9d4ffc41d1d68160
    success!
    remaining products:  755 
    
    adding product 57b9c682ed39822cdebb3c80d823794b
    success!
    remaining products:  754 
    
    adding product 9f3a4ff872f93c8de65efc2f2360ce77
    success!
    remaining products:  753 
    
    adding product 4b5e104aff8d766f766da12284d53651
    success!
    remaining products:  752 
    
    adding product e9b3f2e3a02a1a13b91ce5929258df80
    success!
    remaining products:  751 
    
    adding product 801ebb19f5827eacf5682802ce3ea179
    success!
    remaining products:  750 
    
    adding product 167d739013d283895e59f360a8ef992c
    success!
    remaining products:  749 
    
    adding product c40de21f3a2cbf872822c2621b715908
    success!
    remaining products:  748 
    
    adding product 77b6d3de326a27f5240f743e228e6d60
    success!
    remaining products:  747 
    
    adding product 9c31737d2e075dc48cffaee6253b790c
    success!
    remaining products:  746 
    
    adding product 091b26e964b0e771fbcc107aad43186e
    success!
    remaining products:  745 
    
    adding product a1dd9267e737ca837e80cb0f1bb7118d
    success!
    remaining products:  744 
    
    adding product 476650d630a12751578bf6f9a84f359e
    success!
    remaining products:  743 
    
    adding product 33a7dc86f60ef6b8228c9df8a7e68d30
    success!
    remaining products:  742 
    
    adding product 80bd1978a3dff0dbaceb95fb9191e7ee
    success!
    remaining products:  741 
    
    adding product 0107acb41ef20db2289d261d4e34fd38
    success!
    remaining products:  740 
    
    adding product e02721e864b2649003bcf15ba4da931a
    success!
    remaining products:  739 
    
    adding product f93f4793c2783325b8b6e96c21f3ce5e
    success!
    remaining products:  738 
    
    adding product f5bdd987e82cfcad049b164a59d1fe2f
    success!
    remaining products:  737 
    
    adding product 3623f5f708cd883f4f31d8af4125cebe
    success!
    remaining products:  736 
    
    adding product f902f09c48aac5fdbdc389271d704daa
    success!
    remaining products:  735 
    
    adding product a89b9b817d1c710e6ef5000032e1c514
    success!
    remaining products:  734 
    
    adding product efa41f347fb5bfa798ab738ead1d2045
    success!
    remaining products:  733 
    
    adding product bb2dba24644723c5ee4d687215836391
    success!
    remaining products:  732 
    
    adding product bd0181690d928c05350f75ce49aecb2a
    success!
    remaining products:  731 
    
    adding product 73619c7b11b447e069e92d80914bc329
    success!
    remaining products:  730 
    
    adding product 2e99f39ee5ebc8f125c2c4c2a13d96ef
    success!
    remaining products:  729 
    
    adding product 8c2151e2ba9d892deb590aa6699f7990
    success!
    remaining products:  728 
    
    adding product fd2488ebfc2cc505962035deb80bc233
    success!
    remaining products:  727 
    
    adding product 11bf14c1513b62f30e5e8be425774d30
    success!
    remaining products:  726 
    
    adding product b9630d51806c5bf8ab0cef3fd97d414f
    success!
    remaining products:  725 
    
    adding product 1a21d8c9bbb99bca627434dbf4b98d01
    success!
    remaining products:  724 
    
    adding product ccfed80e87ba3e3a64b55176df02a9d5
    success!
    remaining products:  723 
    
    adding product 53c884f6c22fed309c0c3614f5669eaf
    success!
    remaining products:  722 
    
    adding product 27b5429c1dcf4774c3ee26cd87e3f0df
    success!
    remaining products:  721 
    
    adding product 48c1c1cfe38d8291bb6b2f09319c58df
    success!
    remaining products:  720 
    
    adding product f6276420f0e05d929d71f1dffe6705ea
    success!
    remaining products:  719 
    
    adding product f0ab8e556d0cbb5c0e4201791cfaeae0
    success!
    remaining products:  718 
    
    adding product 6cd4d4f7768fc86ca5642be0f600b518
    success!
    remaining products:  717 
    
    adding product 356b14ff545d88fe64c756f330c11598
    success!
    remaining products:  716 
    
    adding product 2607a30a2be67d96768aa3dbe1cc7731
    success!
    remaining products:  715 
    
    adding product 3ffa944140b77ef7b5e7500eb4ca2fe5
    success!
    remaining products:  714 
    
    adding product 95b428e98d2b66a8ab324313cfc45300
    success!
    remaining products:  713 
    
    adding product bdddf7327677bbfc879f505df7122e2e
    success!
    remaining products:  712 
    
    adding product f29fa72991dafa0ab83a053e89b9866a
    success!
    remaining products:  711 
    
    adding product 6750c0194a5f9ae7194a0ae154b64959
    success!
    remaining products:  710 
    
    adding product a007685ecc0ccf820b8ac1d6e77f69fd
    success!
    remaining products:  709 
    
    adding product 0ebd0b8b51eb0d0062065a7657486c8e
    success!
    remaining products:  708 
    
    adding product e2e57ded5b59a2058dd5855564c6b5ea
    success!
    remaining products:  707 
    
    adding product 3603599ee31fce12be0faf0958263356
    success!
    remaining products:  706 
    
    adding product bcb35e73c974a4ba0b5685dbd875c2fa
    success!
    remaining products:  705 
    
    adding product d2a83dc418ee9d6209d8356fca703f13
    success!
    remaining products:  704 
    
    adding product f5abe18064d57c2e5a768504a2041036
    success!
    remaining products:  703 
    
    adding product 6f50a5cb27dcdda93b96f3bfaee56c1c
    success!
    remaining products:  702 
    
    adding product c8c91941c44ee273af84ef092c01a0b1
    success!
    remaining products:  701 
    
    adding product aabcec3c1485362f5f588135883d37b6
    success!
    remaining products:  700 
    
    adding product ed34401478b1f44e3bf7f79d4f1f5593
    success!
    remaining products:  699 
    
    adding product 8cc980162f04a7e6993a24eba6316032
    success!
    remaining products:  698 
    
    adding product fc095b4f8d0d50d30b1afedbf74fcff1
    success!
    remaining products:  697 
    
    adding product ad0e9e545bd1d949dcf019ce06a2ae95
    success!
    remaining products:  696 
    
    adding product fbcefc201bbac612e5ff6b96c64e2465
    success!
    remaining products:  695 
    
    adding product 5cea81eae128b3027b8e8b7ed836f6ff
    success!
    remaining products:  694 
    
    adding product 437bce6c54114f437f169de31d370f1b
    success!
    remaining products:  693 
    
    adding product bb01328b7c80115379ecf1765e027c36
    success!
    remaining products:  692 
    
    adding product dd0e5d3313b032ce56c959d25e1beee1
    success!
    remaining products:  691 
    
    adding product 21c3b4a7e53f44b1e09f2702f50e775b
    success!
    remaining products:  690 
    
    adding product bb5b5546e7cae6a27e0697a11918958e
    success!
    remaining products:  689 
    
    adding product 25b6a534a8c88b8e73fe7b865fd36bde
    success!
    remaining products:  688 
    
    adding product 7b905b5bd19b35cf3f9d762aa6acaa45
    success!
    remaining products:  687 
    
    adding product ef9ffa986fa98b2485fe6b57ac0c6b1b
    success!
    remaining products:  686 
    
    adding product 713a6858eec9a67eeff4b55c6184656a
    success!
    remaining products:  685 
    
    adding product 110eed2c630aab0f3fa87d6473926732
    success!
    remaining products:  684 
    
    adding product f0d90e8af61baa1bcf391504fb5d95da
    success!
    remaining products:  683 
    
    adding product 7b889da86fa368b083e6b41f1c879fa9
    success!
    remaining products:  682 
    
    adding product d89f151c51c3d0903f0b39042fb55753
    success!
    remaining products:  681 
    
    adding product 115acf0e62e6e62aab5e6dcd475d1a32
    success!
    remaining products:  680 
    
    adding product f95490b29665f1400527d32a286f63ad
    success!
    remaining products:  679 
    
    adding product d6a1499555c182d0fa8919c666fa4710
    success!
    remaining products:  678 
    
    adding product 705cbc6741682eaaf63f44ce341c08a7
    success!
    remaining products:  677 
    
    adding product 091de388b6057d21b628726885c2b0db
    success!
    remaining products:  676 
    
    adding product 5103ae07e6b7cd584364696695b075de
    success!
    remaining products:  675 
    
    adding product 18d84eb30b59b5f3cc748bfe9f68b472
    success!
    remaining products:  674 
    
    adding product 778b443f4e38e2e5a7b573c151cfe64e
    success!
    remaining products:  673 
    
    adding product 0421c02f0fc9cebfa39a613c3c59c3e2
    success!
    remaining products:  672 
    
    adding product 88787df7498d4e60d6d6395fead9a9e4
    success!
    remaining products:  671 
    
    adding product 1b3c1623c5c98ad8549b8f62670d1f52
    success!
    remaining products:  670 
    
    adding product 1aa4d17f2dcdae2f4ced909341741792
    success!
    remaining products:  669 
    
    adding product 80577d9cb5c479e8e8b85252f1bfe005
    success!
    remaining products:  668 
    
    adding product e40cce862b0fb75635e102bc1dd07f6f
    success!
    remaining products:  667 
    
    adding product cd65710fc56d8163dfaed043e4129690
    success!
    remaining products:  666 
    
    adding product 4bd5096853abc791756085adf90dfe7f
    success!
    remaining products:  665 
    
    adding product e3fd383f6ed435f35f70175031cb4697
    success!
    remaining products:  664 
    
    adding product bddf76687dc8ab77f05b2e75fd3176c3
    success!
    remaining products:  663 
    
    adding product 22c1acb3539e1aeba278f7885ddb8d35
    success!
    remaining products:  662 
    
    adding product 8cc3d0d6ce79806cac8e6ac80e3b4cdb
    success!
    remaining products:  661 
    
    adding product 8d0e8d50eb0bad1727b38382d4fa42ef
    success!
    remaining products:  660 
    
    adding product 85c8395916ffc2198dd670da1b20d108
    success!
    remaining products:  659 
    
    adding product 4a850ecfb32efa4f6e894ed5b631d445
    success!
    remaining products:  658 
    
    adding product 35f4421c476ab29bd7492717ccb0642c
    success!
    remaining products:  657 
    
    adding product 1fee5d8b6b5230e47fc933334d03ff5b
    success!
    remaining products:  656 
    
    adding product 614b785e10cceb93fb854958a5f93d1f
    success!
    remaining products:  655 
    
    adding product 3aa5bcc51a5e92cb51f514cc51391d4e
    success!
    remaining products:  654 
    
    adding product 16db0f01adf864f5cd063e1f02371910
    success!
    remaining products:  653 
    
    adding product c30eae095af40e4bdefe6e0f1636eea2
    success!
    remaining products:  652 
    
    adding product 51407c3d5b2240a30a353fa9ae7769ad
    success!
    remaining products:  651 
    
    adding product 9301cb784fa8d1f29d1125c71184ab94
    success!
    remaining products:  650 
    
    adding product 6aaf618580961e7d7d50f1a6aa246fa1
    success!
    remaining products:  649 
    
    adding product 9b2946207cfe91ad1c5c5f4888f39cbb
    success!
    remaining products:  648 
    
    adding product bfe671b7d65b8143e5a5e13d2415ec2c
    success!
    remaining products:  647 
    
    adding product d6cb41a908909feead800375f0e96b04
    success!
    remaining products:  646 
    
    adding product 6c9084f3201fb66874073cffff5618d0
    success!
    remaining products:  645 
    
    adding product 16a766553d27e24a2ecb59dda7126dd8
    success!
    remaining products:  644 
    
    adding product 95430bfbaaa74b4b18b28b0ba10b4b39
    success!
    remaining products:  643 
    
    adding product f676fd7c5cadc839db8b7e65bfbdd8c6
    success!
    remaining products:  642 
    
    adding product 5d5fb81695efe05e819b08eb4deecf01
    success!
    remaining products:  641 
    
    adding product 1452a9dac23cad0697d2d9b76470e234
    success!
    remaining products:  640 
    
    adding product 9c37f52f0de5fb6ee4ee1d8cb7a0b085
    success!
    remaining products:  639 
    
    adding product ec4ecf2f2dc2d3314c1d4cbd433b632b
    success!
    remaining products:  638 
    
    adding product ba9353718aa3b1793b8a23d51e19ef15
    success!
    remaining products:  637 
    
    adding product c8443b6213aa517f2d701ebf845fdae4
    success!
    remaining products:  636 
    
    adding product e3030406173074724559c34666f5830f
    success!
    remaining products:  635 
    
    adding product 744c7571cf7ab98667accb835b549b85
    success!
    remaining products:  634 
    
    adding product 0853de1f652d3a206e18264d18eb3bb0
    success!
    remaining products:  633 
    
    adding product 3d07ea96868c61f951e614e15fd5994b
    success!
    remaining products:  632 
    
    adding product be37636fec7514e291c13c1024c56a9b
    success!
    remaining products:  631 
    
    adding product d17c5e09fc9a4e0dcbea13cb36dd1c36
    success!
    remaining products:  630 
    
    adding product 80f5c854f86f7ecdd80a84b2973a9b08
    success!
    remaining products:  629 
    
    adding product 967e9762cd256961572df916205f2eaf
    success!
    remaining products:  628 
    
    adding product b8a28def6f279bc999c1cc9e12e4e218
    success!
    remaining products:  627 
    
    adding product afb385227f5ff1dcd5c746974baf1060
    success!
    remaining products:  626 
    
    adding product 85ad02a7b97b3ebeaacc7cb3bf321405
    success!
    remaining products:  625 
    
    adding product 3540a005ce1b9c61b8c40338a7fa3b56
    success!
    remaining products:  624 
    
    adding product d1f6b0f22fb84f1e2ee58c9481de0e5e
    success!
    remaining products:  623 
    
    adding product fbe6ce6a3f92aa796ef05b887d97a891
    success!
    remaining products:  622 
    
    adding product a4db53c680ab731e9aa1eba01398bcac
    success!
    remaining products:  621 
    
    adding product b56e3960d3b3075f46e862488ed4979a
    success!
    remaining products:  620 
    
    adding product a92df7cbfdddcc938abc806992026a19
    success!
    remaining products:  619 
    
    adding product dc74d495021d5eb62f2a0dc42a47442b
    success!
    remaining products:  618 
    
    adding product 394201ad68984c677be221f6d9f263fc
    success!
    remaining products:  617 
    
    adding product 1b4c423abf96a91af57fcb3f5dc9491e
    success!
    remaining products:  616 
    
    adding product 285440c1920db8887db5645e46429ac4
    success!
    remaining products:  615 
    
    adding product 00649cd10c9934e5bb72fe5beaf0f283
    success!
    remaining products:  614 
    
    adding product ddabfbbcf6c501f08c49b55f78b4ac32
    success!
    remaining products:  613 
    
    adding product cf9dfba593148297d8d8b0abd1c42612
    success!
    remaining products:  612 
    
    adding product 800ca5c1483432d248c8104d60a0db71
    success!
    remaining products:  611 
    
    adding product 9f6c425b71df66aa20ff08042ad059d9
    success!
    remaining products:  610 
    
    adding product 16a0720adefe16db0df9b9023e733639
    success!
    remaining products:  609 
    
    adding product 3661a947520a1a9694fc9efcdb1133db
    success!
    remaining products:  608 
    
    adding product 988e17b060ece118107df1a54e5cb125
    success!
    remaining products:  607 
    
    adding product ee955e252af3c85e66e15864e31174fe
    success!
    remaining products:  606 
    
    adding product ada67ce42f7e51433fdc45e523f90ff7
    success!
    remaining products:  605 
    
    adding product 33649064cd253288159fcd9af5277e3d
    success!
    remaining products:  604 
    
    adding product 6a21c55a1bc5e866aac9b2bf95ed42d9
    success!
    remaining products:  603 
    
    adding product 5810733635b8629df4a4badaaef78f6c
    success!
    remaining products:  602 
    
    adding product df584d514eb49beffdd8b37311204fc9
    success!
    remaining products:  601 
    
    adding product 6774b4304eb6167c263fa777644370aa
    success!
    remaining products:  600 
    
    adding product d023867f3674a2d8514221428c6760ef
    success!
    remaining products:  599 
    
    adding product 4ce43eeff8805bee6936a5bcb383edc6
    success!
    remaining products:  598 
    
    adding product d57c3910d36def0f811078b484fd8530
    success!
    remaining products:  597 
    
    adding product 24cc4e6e1e5504cef39f1aadd7b09d79
    success!
    remaining products:  596 
    
    adding product adfacb714c53751aee5b8fefeb8426ce
    success!
    remaining products:  595 
    
    adding product a01874b1881feb4f498d3f877bd41e62
    success!
    remaining products:  594 
    
    adding product 992293aa502a94d9d76d1d0313c00873
    success!
    remaining products:  593 
    
    adding product 7362b26d78069dd38f4b45743fddc7ee
    success!
    remaining products:  592 
    
    adding product 070ffad3c0f12da66ca3b5d0c2d23069
    success!
    remaining products:  591 
    
    adding product f6c744ece7e1a36892eba3a5d2938110
    success!
    remaining products:  590 
    
    adding product 5b5c733364156c898f73e9dfc6fa3794
    success!
    remaining products:  589 
    
    adding product deda2f6fe94d5a980d004d35ee664e97
    unknown error, skipping product
    adding product 8424248303304cda787d00ef2732f8f0
    success!
    remaining products:  587 
    
    adding product af1870edf5b2de354c2b90d442a299d3
    success!
    remaining products:  586 
    
    adding product 193996d8a345080ba5cebe43bea3bd15
    success!
    remaining products:  585 
    
    adding product 5ca429b0056550eab08bcfe770eaf98e
    success!
    remaining products:  584 
    
    adding product b3217d23efdb295c5a2e786a50c2e37e
    success!
    remaining products:  583 
    
    adding product eaf441351bf076375ab3a90f8b89b696
    success!
    remaining products:  582 
    
    adding product 1bc967477f11983ba1d41e2167ae575b
    success!
    remaining products:  581 
    
    adding product ce1088f5f083f849cfded441f0a38332
    unknown error, skipping product
    adding product 26e12e8ce3cf76d35b5ab714143378cd
    success!
    remaining products:  579 
    
    adding product b1ab8aea23da706493a95512b29c7dd7
    success!
    remaining products:  578 
    
    adding product 07929c4f27367e5490900478a8fb77f9
    success!
    remaining products:  577 
    
    adding product e5f1d4219be6d4dafe14bc929d5c2808
    success!
    remaining products:  576 
    
    adding product 229b80a4b334bbda34ec0d063f11c699
    success!
    remaining products:  575 
    
    adding product 6fb2011334a2bf8dca4e120157fab408
    success!
    remaining products:  574 
    
    adding product 95cb29c65c9f39aee2714e7734c344cc
    success!
    remaining products:  573 
    
    adding product 4cd32d6e6c24dad2afe99e445b936b66
    success!
    remaining products:  572 
    
    adding product 2aae855e4fce821396110897f2513c60
    success!
    remaining products:  571 
    
    adding product 7500bbe7e791e2d3a19f61cf43b87b99
    success!
    remaining products:  570 
    
    adding product 4a487a7a8a7320a4e05a9b3ee8b1d9ec
    success!
    remaining products:  569 
    
    adding product fee801ecfba08d39cd8ebed9fdcbe7e9
    success!
    remaining products:  568 
    
    adding product 58ea66374b3faa6082d480f1214c2ad2
    success!
    remaining products:  567 
    
    adding product f40ed67587440c64e5b3881297d5c827
    success!
    remaining products:  566 
    
    adding product d750dbc5c4510c96e92e256136bdeb73
    success!
    remaining products:  565 
    
    adding product 1ffcb5b752250faafdbeed38ec2cbcc4
    success!
    remaining products:  564 
    
    adding product 49129551dac6241eb7d1f601f058679b
    success!
    remaining products:  563 
    
    adding product 7427cc88b0300c330f360b2a5a52992a
    success!
    remaining products:  562 
    
    adding product 5896580fef0fba59f84ab5a2f3d3664d
    success!
    remaining products:  561 
    
    adding product 324bacc7aab550b824bbd20d352cbff4
    success!
    remaining products:  560 
    
    adding product 0197fd56abb69bf33e16d0abdc2a439f
    success!
    remaining products:  559 
    
    adding product 767f4c28a2895898b0edab21c1b18ebd
    success!
    remaining products:  558 
    
    adding product 2bc3900bff210b080e6bd19c10331244
    success!
    remaining products:  557 
    
    adding product 9ddaf29fd4e776a9d94bd4f0add6dd5d
    success!
    remaining products:  556 
    
    adding product b0199c060aaef7f56d1995e3e4bf8f8e
    success!
    remaining products:  555 
    
    adding product 32f8bee7c830d03270034463142809cd
    success!
    remaining products:  554 
    
    adding product d69762da81e1d0a33b4d3839e7108eb8
    success!
    remaining products:  553 
    
    adding product 0ce7de2152411167ee38ce75b664517c
    success!
    remaining products:  552 
    
    adding product 38b651967fc03601ac051178920acd30
    success!
    remaining products:  551 
    
    adding product 45c8951d7a63d3827f1d45d2ac103dfd
    success!
    remaining products:  550 
    
    adding product a9410746047725298db12ef6eece6487
    success!
    remaining products:  549 
    
    adding product 6ba38a7106bf9b639fdac2b293912b4e
    success!
    remaining products:  548 
    
    adding product 85e5e0842d745041f29f57f0f3c7a92c
    success!
    remaining products:  547 
    
    adding product 0cba576cd16d0f399c78c557b4899a58
    success!
    remaining products:  546 
    
    adding product 3b2570d084283629e1d02cc8a37bceb7
    success!
    remaining products:  545 
    
    adding product f17a5d67253e3c9c77404e31c6f25c55
    success!
    remaining products:  544 
    
    adding product ad146b968de11f1f6cd49a76f3acc6fa
    success!
    remaining products:  543 
    
    adding product 5981b0f78d15e748a9b0c43202461be2
    success!
    remaining products:  542 
    
    adding product 6de138653a359bbbc1d392d52fa333e6
    success!
    remaining products:  541 
    
    adding product befa5d34b2404ae1750f4010be7b87ec
    success!
    remaining products:  540 
    
    adding product 9625fdcfdf80c49a86c36dcc39ff7e92
    success!
    remaining products:  539 
    
    adding product e0ecee586e85f41544067ac9492a39ff
    success!
    remaining products:  538 
    
    adding product d6fc0654a1b93d121f1a0e4729ff6a08
    success!
    remaining products:  537 
    
    adding product fe6caaa257ac7ef1bfccfc897c098066
    success!
    remaining products:  536 
    
    adding product 338231d03b74dff1aed6a83b0a7c680c
    success!
    remaining products:  535 
    
    adding product cf787e7b5f9346fe81658c708ca68c61
    success!
    remaining products:  534 
    
    adding product 5558e56d737512c66be1e01ecf091b54
    success!
    remaining products:  533 
    
    adding product 9f2f91391d575e5368cfe9ad4048eeea
    success!
    remaining products:  532 
    
    adding product 882c920a157abf020705a45788ba568f
    success!
    remaining products:  531 
    
    adding product 31882dbf700c4b88a999f391ec520665
    success!
    remaining products:  530 
    
    adding product d76803aaf883a0a289d3b4075901d298
    success!
    remaining products:  529 
    
    adding product 801f7c385478d949ce0cdc2640450f89
    success!
    remaining products:  528 
    
    adding product 34d7ece1f805d89e526991b282388e5a
    success!
    remaining products:  527 
    
    adding product eae24a2ae37a194a8564bc9bd921cc07
    success!
    remaining products:  526 
    
    adding product c9c41b10ece8463fc7398b7a283f00f9
    success!
    remaining products:  525 
    
    adding product 393a0f103dfdc3798cf1cfc2e7bdbc1f
    success!
    remaining products:  524 
    
    adding product 5989fd7934c8b2000d3f83cfb75cdb47
    success!
    remaining products:  523 
    
    adding product 9b67ff5862eb652ed35b0554cb7f62f2
    success!
    remaining products:  522 
    
    adding product 5f65c233d57a4b31b1e4edbaa79bf6ca
    success!
    remaining products:  521 
    
    adding product 0b214389fac101bf39d422494db14302
    success!
    remaining products:  520 
    
    adding product eb5c505e43f84a12f3e8340a2b679567
    success!
    remaining products:  519 
    
    adding product 559a625353723964777aeaeb47124a7f
    success!
    remaining products:  518 
    
    adding product 78cb7fa4cd7b891a867134cf4885e1f5
    success!
    remaining products:  517 
    
    adding product be0bbb0f90bc9a8c1124eb992bae98f9
    success!
    remaining products:  516 
    
    adding product a80788ea7a51aea6758580944fd40b5d
    success!
    remaining products:  515 
    
    adding product 480262a8c8a59b699e77de6ad8bb0001
    success!
    remaining products:  514 
    
    adding product 19b060fd3e8d334fa8ce36cc20f5480d
    success!
    remaining products:  513 
    
    adding product 35c86d11621ca9f4cc9276c307397c6d
    success!
    remaining products:  512 
    
    adding product 53afd7ab97449cdbb682b6b7e335c524
    success!
    remaining products:  511 
    
    adding product ac358cbed52f811a87ccf33be3e5b5bb
    success!
    remaining products:  510 
    
    adding product f06110ef2e1e1ae119cbacf71dd17732
    success!
    remaining products:  509 
    
    adding product c15e1bfe1ac5ed47bec025ca88301b3f
    success!
    remaining products:  508 
    
    adding product b084182f10cd8031300234c7551ef710
    success!
    remaining products:  507 
    
    adding product 08afe31e80612e5ea99ac4daea3b666b
    success!
    remaining products:  506 
    
    adding product 12bf6e4ef656f8d8a5b2ee769c747c77
    success!
    remaining products:  505 
    
    adding product a291eb7ce8c6c27ed798151c4a0741bc
    success!
    remaining products:  504 
    
    adding product 8301f99aa26536037481546bf5543536
    success!
    remaining products:  503 
    
    adding product 3bc9f4ac200719ade62cf70b5dba1e9b
    success!
    remaining products:  502 
    
    adding product ea30a83f57dabc455488ad906feaf213
    success!
    remaining products:  501 
    
    adding product 3f78614f061c508c892c4dfe0abdf27e
    success!
    remaining products:  500 
    
    adding product d62b664e0111db219783a71ed8225336
    success!
    remaining products:  499 
    
    adding product aea5525e5aa202e1efd5895c330110bd
    success!
    remaining products:  498 
    
    adding product 0fe38ee1a1a9c6bd953a5c27a5ca9258
    success!
    remaining products:  497 
    
    adding product c519d2a456ff3844afd6966f7707100b
    success!
    remaining products:  496 
    
    adding product 47e3711856579ea4c53625d15fcc306d
    unknown error, skipping product
    adding product 539c294351032d5473dbcfcc619ff021
    success!
    remaining products:  494 
    
    adding product a7a1b53e91a3344e9eb9db2a6fa4eae2
    success!
    remaining products:  493 
    
    adding product 5cd338743288fdb62b74ee279d51bf93
    unknown error, skipping product
    adding product e195c8d9639740a0e9118526e45d4389
    success!
    remaining products:  491 
    
    adding product 53fa2bb31e7b9c5afebcd5bc6212c09d
    success!
    remaining products:  490 
    
    adding product 3e5966412bd897dfd9b77ffae75eef1a
    success!
    remaining products:  489 
    
    adding product c1927a57de5c455d3bb1e6cdae59fab5
    success!
    remaining products:  488 
    
    adding product 32e22c26cece29777049a84adf05f465
    success!
    remaining products:  487 
    
    adding product 18dc2aa306574480d292c7b0210f7545
    success!
    remaining products:  486 
    
    adding product 5723aa963dd4fc65b5ebce3950d57fb4
    success!
    remaining products:  485 
    
    adding product 3eb4c11722507eae902e13a8f202e947
    success!
    remaining products:  484 
    
    adding product 7502098036174eed238449e4bf4a5df4
    success!
    remaining products:  483 
    
    adding product 56620d45f773d0080554b694a21e5ba7
    success!
    remaining products:  482 
    
    adding product 256c6f5b61473aaaf1fe73f571b4b28b
    success!
    remaining products:  481 
    
    adding product 246fd89796299b9af42e8093e81cc124
    success!
    remaining products:  480 
    
    adding product badc93fe6e7962936c7f7d5ad32f69e4
    success!
    remaining products:  479 
    
    adding product b851cb30a3604f1a03c679a79b0a443d
    success!
    remaining products:  478 
    
    adding product dd14fcb4232caeda4d922db41d6174df
    success!
    remaining products:  477 
    
    adding product 9e1a510b91b3f7306a4eb02c65adb290
    success!
    remaining products:  476 
    
    adding product 2ee2b82af4bb59ba0c5773d149146e05
    success!
    remaining products:  475 
    
    adding product af20aa8cc361a5498fb417683dfd8488
    success!
    remaining products:  474 
    
    adding product baf00f206054a125267743f710215406
    success!
    remaining products:  473 
    
    adding product 4d60a385a8f1a095e0999b065bc14bc8
    success!
    remaining products:  472 
    
    adding product 38a44c258ac6985bb71726a37252d313
    success!
    remaining products:  471 
    
    adding product 08a7a2e472df1185eec3b0c1c0b1ba14
    unknown error, skipping product
    adding product e4971735e71b7c924d2f5aef6f5a7334
    success!
    remaining products:  469 
    
    adding product 9fd4b00af024dc168d3d955414ceb8e9
    success!
    remaining products:  468 
    
    adding product b13c4b2504158972122bb5cbeda135b2
    success!
    remaining products:  467 
    
    adding product 23a741fa2f45791cd3462521042150b2
    success!
    remaining products:  466 
    
    adding product bdbb7b82339a6700741e6dd665b8a993
    success!
    remaining products:  465 
    
    adding product 3e7e22b67764ffd727da564826610ecb
    success!
    remaining products:  464 
    
    adding product 6027b8cfb50835b0349ee47aaac993d5
    success!
    remaining products:  463 
    
    adding product 109e3ceb0fb7112c1e9e4a3b45e68212
    success!
    remaining products:  462 
    
    adding product 84543ef19503939d36bf0cda6d512ffd
    success!
    remaining products:  461 
    
    adding product da688d46dbe8aed91344330b0d863db5
    success!
    remaining products:  460 
    
    adding product 9c6183bdf4637455ff537a54bc6903b5
    success!
    remaining products:  459 
    
    adding product 694a429b7c2cfdc889201e3c6f2ec942
    success!
    remaining products:  458 
    
    adding product af27be11d68710883283079c149c9a0b
    success!
    remaining products:  457 
    
    adding product 8d4af5c8b9b40206046d4f2c889eceed
    success!
    remaining products:  456 
    
    adding product b09d978ea462060c446ed6833f58735c
    success!
    remaining products:  455 
    
    adding product 28395fe80c4551cf02fb5eb7ab36fc27
    success!
    remaining products:  454 
    
    adding product b18f646fb284fc597d309242de321023
    success!
    remaining products:  453 
    
    adding product bda9f65e28426fc1f93c4f5f223cd1bc
    success!
    remaining products:  452 
    
    adding product 8ef3abcafea125f2518236e28761c9da
    success!
    remaining products:  451 
    
    adding product d454766e392ac21320cccd0b55ecba00
    success!
    remaining products:  450 
    
    adding product 8357ebc8e1db498622c8933ea5319243
    success!
    remaining products:  449 
    
    adding product c40d81efdade5f24d4d1181392064079
    success!
    remaining products:  448 
    
    adding product b03d078b859d32d6b12b3e491acaf978
    success!
    remaining products:  447 
    
    adding product ac6eed7114066ff5d9b10473b7ef6341
    success!
    remaining products:  446 
    
    adding product 4e18967c55baab1033250c9f8b0016b1
    success!
    remaining products:  445 
    
    adding product 829972db7d322fb01409a04c43702f6f
    success!
    remaining products:  444 
    
    adding product e07c2c81c883f095372f10815aaa25ba
    success!
    remaining products:  443 
    
    adding product d8e0a5cf600594ec60296c205af805e7
    success!
    remaining products:  442 
    
    adding product 356cdab4ed0406224c0880771445819a
    unknown error, skipping product
    adding product f9c1a456675346ff7d0f2b8b1e7b1899
    unknown error, skipping product
    adding product 3dfd792516fe8f7234c087e34318241d
    unknown error, skipping product
    adding product 150c1a63c456776f622da0602c807f2a
    unknown error, skipping product
    adding product 89cb339a11bdbd43d54b1307129474f8
    success!
    remaining products:  437 
    
    adding product 4db3b4270aca22aa23c78c4acf712915
    success!
    remaining products:  436 
    
    adding product 650627c390add68b2ee9cf404da91ce1
    success!
    remaining products:  435 
    
    adding product c5f03386978cf62ddb7f2f46e9bd5790
    success!
    remaining products:  434 
    
    adding product 481a8ae21ac57745db20e071b7c04c35
    success!
    remaining products:  433 
    
    adding product 5fde161290a4ebf1163b976f2fa03cdd
    success!
    remaining products:  432 
    
    adding product 0c945a8e12dd7ff713c275c1ad6de9e1
    success!
    remaining products:  431 
    
    adding product a94c022440f10ccdd9dc1debbf32c6e4
    success!
    remaining products:  430 
    
    adding product 0a1882920dc11082416afef7402e006d
    success!
    remaining products:  429 
    
    adding product 5e27fa9f1e017d329261a023f2400ce1
    success!
    remaining products:  428 
    
    adding product 40ddaee25009b9e616272713206535a1
    success!
    remaining products:  427 
    
    adding product ca1e7c16062816dd5c888af7ea5afa0b
    success!
    remaining products:  426 
    
    adding product a7329df9cdd8a212eb351e34fed578c9
    success!
    remaining products:  425 
    
    adding product 967d1576e70c8f8649702b974e035744
    success!
    remaining products:  424 
    
    adding product f7e1e746ce228f98a63d227c23da4f4e
    success!
    remaining products:  423 
    
    adding product 97d84fba14b84fb843751040837c95b4
    success!
    remaining products:  422 
    
    adding product 766c626b2cb1532f235d3bd44279f2f9
    success!
    remaining products:  421 
    
    adding product e50bea89361ff267664d517a596ac387
    success!
    remaining products:  420 
    
    adding product 9d949090ad3edca795fa487cdbbfc436
    success!
    remaining products:  419 
    
    adding product c5e04ccb6be7fab8ccb9df005a075cc2
    success!
    remaining products:  418 
    
    adding product 20a8571b66205bd36a898172ae082c53
    success!
    remaining products:  417 
    
    adding product b3b34ad981273adad88546e295a0ff21
    success!
    remaining products:  416 
    
    adding product f581475b3b03d3686c8868d35352e20b
    success!
    remaining products:  415 
    
    adding product 2065d2abc480bb9c5155747ecab64395
    success!
    remaining products:  414 
    
    adding product 7874884678098f0edf576029131759d8
    success!
    remaining products:  413 
    
    adding product f3d40945077a8cb357098311250d4213
    success!
    remaining products:  412 
    
    adding product 2963dbc550404970787514aa177006e8
    success!
    remaining products:  411 
    
    adding product 021b8947656eb84e4c641506215777c8
    success!
    remaining products:  410 
    
    adding product b65e747fd66bfc23a11308b8f52dad94
    success!
    remaining products:  409 
    
    adding product 6b34ebfac7c2054f2626b6fd5d719224
    success!
    remaining products:  408 
    
    adding product f7fb43719fb4947a5d0faa61de9fb232
    success!
    remaining products:  407 
    
    adding product f766062778cfe284c267e9bb1bd5499d
    success!
    remaining products:  406 
    
    adding product 2f88d23e18b9c290f92240a327d865e8
    success!
    remaining products:  405 
    
    adding product 5e54918976bb01299a2eca9319c229d4
    success!
    remaining products:  404 
    
    adding product f076abd96d6f1a9ae1b32bde776e9e82
    success!
    remaining products:  403 
    
    adding product 1d10c65b774172bd62b8b1dae15b8fd8
    success!
    remaining products:  402 
    
    adding product 45c2d3411b9139f5c37b5041aad1ff9b
    success!
    remaining products:  401 
    
    adding product 7da9e0bb90d7f5b27e9af974fe437abf
    success!
    remaining products:  400 
    
    adding product f73ff6b435051f83259a5f7b2f542e4b
    success!
    remaining products:  399 
    
    adding product 536c5213cb6e3f546b8a5612aa104aac
    success!
    remaining products:  398 
    
    adding product 85bbbcff1e1ac5dae544e0a76eedde4b
    success!
    remaining products:  397 
    
    adding product 0ab50f2d01ea2c3fa94e6faeb2142de3
    success!
    remaining products:  396 
    
    adding product 70386b1a05d9528ee02b3d56e5ed479f
    success!
    remaining products:  395 
    
    adding product bc0c06456b3c6d29acf100c89ee9b1d3
    success!
    remaining products:  394 
    
    adding product 5c4f80aa52df224e6df555eab12c4fc7
    success!
    remaining products:  393 
    
    adding product 5b8c48ed0e21e38e1dcf27a0db6f23dc
    success!
    remaining products:  392 
    
    adding product faabf1c1f629e5ab72fcc8f11b462df5
    success!
    remaining products:  391 
    
    adding product ec6019ea251a4e03b08d4135153be64e
    success!
    remaining products:  390 
    
    adding product cf3df6fa1165f1ceaa6c246e9d7d0492
    success!
    remaining products:  389 
    
    adding product 82553a1ebce1e1df751e69b697bd097b
    success!
    remaining products:  388 
    
    adding product b5932e1bc9bc30711f71a60a3d5c965c
    success!
    remaining products:  387 
    
    adding product 28869ca36d89935b7de5d54a513e63e3
    success!
    remaining products:  386 
    
    adding product 70f0e6d5970254940a7de06dc63e4ed3
    success!
    remaining products:  385 
    
    adding product 738602136ff278808f085f26dc8913ad
    success!
    remaining products:  384 
    
    adding product 2bff3ec0941ed48576a06eed788fad54
    success!
    remaining products:  383 
    
    adding product 8bb2c536d0fb580ce30b60ca38f773dd
    success!
    remaining products:  382 
    
    adding product 08c48adc90c8525f8ca1f8d727b5780c
    success!
    remaining products:  381 
    
    adding product 12b2fce48d921b502cb67aaf23df662f
    success!
    remaining products:  380 
    
    adding product 2661d3ecfd1458a72d642c635f4972ce
    success!
    remaining products:  379 
    
    adding product c6fc35734a1a498915984159907854e9
    success!
    remaining products:  378 
    
    adding product 87fc1fd2316e69d5c46396b47ed63b0a
    success!
    remaining products:  377 
    
    adding product 1677ff8f43415db89c2157e0f9e042aa
    success!
    remaining products:  376 
    
    adding product cb99590f7cf124e88bdd3a40b3b1c8bb
    success!
    remaining products:  375 
    
    adding product dc0e16a46c7bb604bc7fd87037f32787
    success!
    remaining products:  374 
    
    adding product 457c753860099e09373e202e39292de9
    success!
    remaining products:  373 
    
    adding product aa713fa341f6786c39b587498449a999
    success!
    remaining products:  372 
    
    adding product 8ceab02fd10ccfca0ee92f8d14087c7d
    success!
    remaining products:  371 
    
    adding product 599708672e8b3790d67dbf4379f75355
    success!
    remaining products:  370 
    
    adding product a69017f8746c392b173dc70700fda957
    success!
    remaining products:  369 
    
    adding product 7851934f3332c6b7ff9a3e4ed82e532d
    success!
    remaining products:  368 
    
    adding product b4525c940c2ee20606f7a6a59f32ab8b
    success!
    remaining products:  367 
    
    adding product 98a733901e53052474f2320d0a3a9473
    success!
    remaining products:  366 
    
    adding product 8c4b0479f20772cb9b68cf5f161d1e6f
    success!
    remaining products:  365 
    
    adding product 874b2add857bd9bcc60635a51eb2b697
    success!
    remaining products:  364 
    
    adding product ef246753a70fce661e16668898810624
    success!
    remaining products:  363 
    
    adding product b3d299cba3d27f7e8bb7818c4b421f9d
    success!
    remaining products:  362 
    
    adding product 73fb305c6b3819a3e01c5d351e699abc
    success!
    remaining products:  361 
    
    adding product 1fd1df658a0a3d7f385185db7c9c5029
    success!
    remaining products:  360 
    
    adding product 32364276cb2f62e1e492f15ca557159c
    success!
    remaining products:  359 
    
    adding product 15bfa7afabd1570846f5aa93a08b6503
    success!
    remaining products:  358 
    
    adding product e2ae9d605614017e3ae77dcbc1aaee23
    success!
    remaining products:  357 
    
    adding product f5fd4686872b63a5840cc1113450801e
    success!
    remaining products:  356 
    
    adding product cc18000b67cb813af111404b90b21019
    success!
    remaining products:  355 
    
    adding product ec450d5fa45d2aefb861309e1f063b78
    success!
    remaining products:  354 
    
    adding product 4744015f38ee03ef7f62f60a5017907c
    success!
    remaining products:  353 
    
    adding product e5b10e2dfc76b52f3f707e43cb97bbf4
    success!
    remaining products:  352 
    
    adding product 0ed3916f73ce4fa90ec1fc8d83a619cf
    success!
    remaining products:  351 
    
    adding product c1092c40dfa01c731017bd0dd7cf63ef
    success!
    remaining products:  350 
    
    adding product 68d9b6f0ca40f114c048f4a91d701085
    success!
    remaining products:  349 
    
    adding product 0de705dc7d8026cc9b2128b775e4c35e
    success!
    remaining products:  348 
    
    adding product 34c9857f8eaa71063a6db763f5c9ff73
    success!
    remaining products:  347 
    
    adding product 18b30c4ac2b116fdb322b3a7f749979e
    success!
    remaining products:  346 
    
    adding product f3d283dc9dbbcf9377d91798ac47cf2f
    success!
    remaining products:  345 
    
    adding product 65742cafb273e12fc7bb968b5fca065e
    success!
    remaining products:  344 
    
    adding product 6ab2ef9e3a9f4ed96fb1b77191e612d0
    success!
    remaining products:  343 
    
    adding product 4fdabf3e20f3f118022711bf916c6703
    success!
    remaining products:  342 
    
    adding product 97a111b32fdafbaa0de29a40b2df1ffd
    success!
    remaining products:  341 
    
    adding product 5d213468da8857324393c707fb3f6f67
    success!
    remaining products:  340 
    
    adding product 6abfefaf79dff3c6a5bcfde47a85749f
    success!
    remaining products:  339 
    
    adding product e1dbbe5fa0cc885cabb5d674c14ca7a9
    success!
    remaining products:  338 
    
    adding product f72e0b9fc085734d1bb6932d3f5b48fd
    success!
    remaining products:  337 
    
    adding product e2ce1b07e23d3e34e4d8ab4d93a0b43c
    success!
    remaining products:  336 
    
    adding product abdb9f5517daf77fe4714ad0669c9e19
    success!
    remaining products:  335 
    
    adding product ba84d9ed0e2c391714dd41620490d8db
    success!
    remaining products:  334 
    
    adding product 6ca8bd680dac1bb9924d6ad39c81de5c
    success!
    remaining products:  333 
    
    adding product cd6274314204f0b7342f35ae9dfa0165
    success!
    remaining products:  332 
    
    adding product 446ac7480f6cd015d176f8b3d28a03b5
    success!
    remaining products:  331 
    
    adding product ba683b00e5811b2a1e28212d787739e9
    success!
    remaining products:  330 
    
    adding product 55ae21ea938d436617a8bddffede5e3d
    success!
    remaining products:  329 
    
    adding product b98e5a6e50fe75d799e9e9cd27f756b6
    success!
    remaining products:  328 
    
    adding product db884574bea7de391188651592585c7e
    success!
    remaining products:  327 
    
    adding product 4927e1395ab1d386386a762cad17d7a7
    success!
    remaining products:  326 
    
    adding product 197410d59aa228e0c9af78e1950d2381
    success!
    remaining products:  325 
    
    adding product 04dce75ffbe9517b642899d80d63d41f
    success!
    remaining products:  324 
    
    adding product 4718d3908442b0f7b4533b1c366bdbe6
    success!
    remaining products:  323 
    
    adding product 0ac18d27cc2284445bc249e8a83462fe
    success!
    remaining products:  322 
    
    adding product b589889a75048f80f0d1ce793c6382cc
    success!
    remaining products:  321 
    
    adding product 68da88f6136bd6e456811e4a1f941ac0
    success!
    remaining products:  320 
    
    adding product 2e627298db86cde0482baae618c4e517
    success!
    remaining products:  319 
    
    adding product 9b531edcbb1a4a81e667cd8acce6b8ab
    success!
    remaining products:  318 
    
    adding product 96a0d70498272acfee21d3dbae846113
    success!
    remaining products:  317 
    
    adding product 36d3be4cf501c5ad9e07d3e2507b181a
    success!
    remaining products:  316 
    
    adding product 308794a90ec43d779df31a2e865a6f36
    success!
    remaining products:  315 
    
    adding product f226f6cf9fdfe5a00262793195a3d228
    success!
    remaining products:  314 
    
    adding product 1bf3fa859c48493f5f2606ccaaa0f20e
    success!
    remaining products:  313 
    
    adding product 077bc03b8e133cac8e16533bc79c673c
    success!
    remaining products:  312 
    
    adding product d59ec4d93d959f651ed743c75192723e
    success!
    remaining products:  311 
    
    adding product a6e541f4abdd89b30649ca4e7f47ec24
    success!
    remaining products:  310 
    
    adding product 38b6ab474442cca618cc9ef22dce6e02
    success!
    remaining products:  309 
    
    adding product cef73ce6eae212e5db48e62f609243e9
    success!
    remaining products:  308 
    
    adding product 449ddfa2100f691195ad1b10e5bcd846
    success!
    remaining products:  307 
    
    adding product adb1341c69f3803a176b96c5584520f4
    unknown error, skipping product
    adding product 7998b659c5fdea8653a0ed11b4a89dd2
    unknown error, skipping product
    adding product 45402d4ff8981a182dcfc4813600961f
    success!
    remaining products:  304 
    
    adding product 9ce60c64ac4510df68537de96631261f
    unknown error, skipping product
    adding product 87e942236933558e0ea7cd7dee76e9db
    unknown error, skipping product
    adding product a1280bb57e980da66d54eb0f20cbb95e
    success!
    remaining products:  301 
    
    adding product c72741e550f08085fefee77a99d9ccb3
    unknown error, skipping product
    adding product c323092e3dc96ec44049c28c7dd27089
    unknown error, skipping product
    adding product bf811576819a427614cbc193920b16df
    success!
    remaining products:  298 
    
    adding product 5bfcc918b4ac11cfd8c4a49e73bfa882
    success!
    remaining products:  297 
    
    adding product abb4b1c0c6cda71595f6aedfb3f81bbe
    success!
    remaining products:  296 
    
    adding product 61b4258564db32b0b663450f8cfa54cc
    success!
    remaining products:  295 
    
    adding product 55b9d07f95df2d8a391673726bf4ef3d
    success!
    remaining products:  294 
    
    adding product 2b21de16a5c3b0913227003411b15196
    success!
    remaining products:  293 
    
    adding product cb57678fdbadec9b46d95b112035d4d3
    success!
    remaining products:  292 
    
    adding product c928d38c5e85028940fbf707e6d20c7d
    success!
    remaining products:  291 
    
    adding product 8b7496b0d4f85eb60f8c70fc494c9983
    success!
    remaining products:  290 
    
    adding product a1fdbc71f5161bbdc010209c018587d3
    success!
    remaining products:  289 
    
    adding product 7b0e861aacb92e74f2ea443d7c626b53
    success!
    remaining products:  288 
    
    adding product 3542b3cfea2422668353ada28e15463e
    success!
    remaining products:  287 
    
    adding product 592b06020a3ed41d1320aba4928daf8d
    success!
    remaining products:  286 
    
    adding product e483cc701d962f6b22bfea4b09635652
    success!
    remaining products:  285 
    
    adding product 31716bdf834f7838689285ce155e7a64
    success!
    remaining products:  284 
    
    adding product b6ad337fad5d5dcaac313e78183ebf61
    success!
    remaining products:  283 
    
    adding product 07f7b1153c6e600da9abeddc1b03f2c8
    success!
    remaining products:  282 
    
    adding product 46c4e6a9d4e1392f1f86747329e15ab0
    success!
    remaining products:  281 
    
    adding product 9ac2fc991d780c349fe1e5863e731108
    success!
    remaining products:  280 
    
    adding product 08aa869cbca066c1278b2d2e4ed7b19d
    success!
    remaining products:  279 
    
    adding product a908989e899dc4f8273b76094426e9fe
    success!
    remaining products:  278 
    
    adding product cb2bed7f0d9f1d9af7833c27a0fbaa1c
    success!
    remaining products:  277 
    
    adding product a1a9310fb733cdd49c0808731b68c048
    success!
    remaining products:  276 
    
    adding product 45b531e01616fe0a6b2d8d51583b36a7
    success!
    remaining products:  275 
    
    adding product a1e4a9d047858b87c17707c0c4e91657
    success!
    remaining products:  274 
    
    adding product d61e0dab436d2a75c7ec983d78c4a2cd
    success!
    remaining products:  273 
    
    adding product b05a122ddef15ca76477c4edbc885d2c
    success!
    remaining products:  272 
    
    adding product 0384691e7d566f12edd845ac862bd928
    success!
    remaining products:  271 
    
    adding product 4fd122953585783d1e67973850a79cde
    success!
    remaining products:  270 
    
    adding product 8875c52a7bcbd3e60c07ce65b152fefc
    success!
    remaining products:  269 
    
    adding product dd8a25785dcb6f7ceca55668d9ad9dc6
    success!
    remaining products:  268 
    
    adding product c4690d53032a8edfd2b88d6990956b1e
    success!
    remaining products:  267 
    
    adding product 4cc151838425461aaedbe5a3c3866160
    success!
    remaining products:  266 
    
    adding product 1b0e7baae0b3154624caf0cfb717eda3
    success!
    remaining products:  265 
    
    adding product 5ecc613150de01b7e6824594426f24f4
    success!
    remaining products:  264 
    
    adding product 451fbb024d0794ffcda2258170740a1e
    success!
    remaining products:  263 
    
    adding product 705e2dd2077bc06fbc5e2c754e75e500
    success!
    remaining products:  262 
    
    adding product 09510d526df60f47c2797dee42254939
    success!
    remaining products:  261 
    
    adding product cb61843a4d547a8d2a199fe80a7a1add
    success!
    remaining products:  260 
    
    adding product 08ff82ac429ab04b259284d197401656
    success!
    remaining products:  259 
    
    adding product a1ac769a261b4ac11e1fd4fd33b2ae8b
    success!
    remaining products:  258 
    
    adding product e47d77368f63af97c0b0358474b37a43
    success!
    remaining products:  257 
    
    adding product ad7030d0d43a7f5903f38cc76af762a5
    success!
    remaining products:  256 
    
    adding product 6026ab34372aefc36721984187f2afc6
    success!
    remaining products:  255 
    
    adding product 5e72cff204b3c07b4f71e7ae356dabda
    success!
    remaining products:  254 
    
    adding product 66b13b118270a7c760a840e70aa66af9
    success!
    remaining products:  253 
    
    adding product c121944d9d4e62e5cbb1a4a7f6a46b3a
    success!
    remaining products:  252 
    
    adding product 0d2bc6fd77be0fa8ef1216f473ef9437
    success!
    remaining products:  251 
    
    adding product 6d36227478a774fb5cf1b188eb483da1
    success!
    remaining products:  250 
    
    adding product a43cf1bfb47754486720d964df63425b
    success!
    remaining products:  249 
    
    adding product 51e706f8b33d4020dbe481ae37603842
    success!
    remaining products:  248 
    
    adding product e05baf3e0c3214593c5ee81af5917770
    success!
    remaining products:  247 
    
    adding product 2f96a08bf9fecb843023a3f94a8ddf9d
    unknown error, skipping product
    adding product 31163fedec9349e8f7d35a84c05cb94a
    success!
    remaining products:  245 
    
    adding product 75bd5e7f19a7584c82c3f77dc61bda5a
    success!
    remaining products:  244 
    
    adding product d4366c47a957a1d968dc4fde9667a5be
    unknown error, skipping product
    adding product 148d442971558088c915121f85c797b3
    success!
    remaining products:  242 
    
    adding product 17a5521f02c96ba003e028f278e3ab15
    success!
    remaining products:  241 
    
    adding product ca98acb44f4ced8db93b804eb3203a75
    success!
    remaining products:  240 
    
    adding product e797b9be4cf8c1f1de10c2fba822e99a
    success!
    remaining products:  239 
    
    adding product 83ece3622fecf463a8b520014ed96c26
    success!
    remaining products:  238 
    
    adding product 14f5c9b660813801b453b5903af18ca7
    success!
    remaining products:  237 
    
    adding product 56bbc93a8425b95e4ab7c789751475bf
    success!
    remaining products:  236 
    
    adding product 1134604ead16f77309235aa3d327bb59
    unknown error, skipping product
    adding product 34910069e394ae6693ff52fc4968be09
    unknown error, skipping product
    adding product a9614aeb0d542c581cbae2fe2832f236
    success!
    remaining products:  233 
    
    adding product 1b88523fe5ac3c4b7a149059ed1d7c77
    unknown error, skipping product
    adding product 48f36fcf310742f269ed41f985563900
    unknown error, skipping product
    adding product d26cf5a5aa1c2999c8339d77fc3eed44
    unknown error, skipping product
    adding product 2c9aabda8931c46b74753eb6dd9136dd
    success!
    remaining products:  229 
    
    adding product ab243ac315c8f9c55254cbafe079b3c5
    success!
    remaining products:  228 
    
    adding product dde4952e3874b24df8091b3e4b62e501
    unknown error, skipping product
    adding product b4785c555b94d84947f532c82c1eefa5
    unknown error, skipping product
    adding product 25f258164741bc25847ecef1067b24f0
    unknown error, skipping product
    adding product b844e481df2e5987e9f1a31c6642a2c4
    success!
    remaining products:  224 
    
    adding product 937f578a9e42b1c322cf1e27d4009180
    success!
    remaining products:  223 
    
    adding product 48b508b64892bdf1d3a44e6de12e146a
    success!
    remaining products:  222 
    
    adding product 5c72c99424191cfd8ec2227b923c03d9
    unknown error, skipping product
    adding product 319b2600b8defbf79afcb125d55ff9c7
    unknown error, skipping product
    adding product 6b18d371703cf5221bc39c1a03a3be64
    unknown error, skipping product
    adding product a1b2e04c65e8fa74ba1e50a429b828ca
    unknown error, skipping product
    adding product 68c20262d2657796f56d2101e46b3e73
    unknown error, skipping product
    adding product 596f13d0466c69130d1cf0951e189df3
    unknown error, skipping product
    adding product 28d89ee0086b8d5e875b59d3338f3d3a
    success!
    remaining products:  215 
    
    adding product 4bd106ffe5e8ef167227db73267a1011
    unknown error, skipping product
    adding product b4bee9fe0aef927e2a9e2b7072832d6e
    unknown error, skipping product
    adding product c054543302f7e03e186bb87adaecf20f
    success!
    remaining products:  212 
    
    adding product e31003304da364867f1dce3be564fb7a
    unknown error, skipping product
    adding product ddacbedc18fb4a0be1a48b7bdd424882
    unknown error, skipping product
    adding product 0fff885ae427e3adae25dbb31251470c
    unknown error, skipping product
    adding product 37654b793d96ed06d8c2bfa60658a502
    unknown error, skipping product
    adding product 4daf3131a3b73237edccfc5c6acbd7ad
    unknown error, skipping product
    adding product 9f5f53f37396a3c5c13d76d62b5edaab
    success!
    remaining products:  206 
    
    adding product 69908a3ae9144cf4918985a785114a46
    unknown error, skipping product
    adding product 541356751e5796df18e1e7bd77072993
    unknown error, skipping product
    adding product e988c6b43fabb5006ea95c18d5088b9a
    unknown error, skipping product
    adding product b42cd7198ff1d832fc7b8f6e3a187ad3
    unknown error, skipping product
    adding product d1753623bca69e9b6549954e526d6b64
    unknown error, skipping product
    adding product 32472eb6885a55919e517965fddd2d74
    unknown error, skipping product
    adding product 2ec937378ea08e49faa1a6608880d002
    unknown error, skipping product
    adding product 7ca595cd7955654b36629c1470763487
    success!
    remaining products:  198 
    
    adding product 737344cccb5f89b59a89a5f20cc3f303
    success!
    remaining products:  197 
    
    adding product cde2d9ccd0e94745859e8b340f4d06c2
    success!
    remaining products:  196 
    
    adding product 2cc0b255c4894762e17aad3059ce2dcb
    success!
    remaining products:  195 
    
    adding product 6be522523820e664e679c35e55de69c5
    success!
    remaining products:  194 
    
    adding product 082a1d41243d3f37ae65ba1c9d0e2b57
    success!
    remaining products:  193 
    
    adding product 4ad7fff9ccf799f755109fa80dcd8b10
    success!
    remaining products:  192 
    
    adding product b7c341015338340fc8cc5c21e0473579
    success!
    remaining products:  191 
    
    adding product 923afdc34efd0d626a7d62c49f3fb4e8
    success!
    remaining products:  190 
    
    adding product 436020d2ae84493b822204db77896285
    success!
    remaining products:  189 
    
    adding product 9a66e987aff9a6d0f673189ca1e72f78
    success!
    remaining products:  188 
    
    adding product 433db795566d9f51a72086333588e655
    success!
    remaining products:  187 
    
    adding product b713e1186b3985360c7c9c063de8bdf9
    success!
    remaining products:  186 
    
    adding product 56c25287be94f28752503a1cb341c896
    success!
    remaining products:  185 
    
    adding product c980fea9ff9c4bb4b175e2708d05e417
    success!
    remaining products:  184 
    
    adding product cb804af641d900ffe033193d2b7c4a84
    success!
    remaining products:  183 
    
    adding product 8557f7303f1355575b6e95d411c9cead
    success!
    remaining products:  182 
    
    adding product 8befb4efe8ce6cdf0e1a84974d452a9f
    success!
    remaining products:  181 
    
    adding product b056f5d60446a5d5ca7fba949cae3cfa
    success!
    remaining products:  180 
    
    adding product f9ae0edbbb69c65b4ba6a4df582eb206
    success!
    remaining products:  179 
    
    adding product 20350ae97d3380ae78f158aca7089b23
    unknown error, skipping product
    adding product 6f187627fbe3b7f3b9b5cffe3386d670
    unknown error, skipping product
    adding product 7d0c5dcce206d1a180df48ebcbd42e33
    unknown error, skipping product
    adding product f1b824c353b9b2bb1327e0e72a05b6b9
    success!
    remaining products:  175 
    
    adding product 42cae009aa36f970c223de2959268e5a
    success!
    remaining products:  174 
    
    adding product 1daf6cfb21ba43954310a6dd338d0416
    unknown error, skipping product
    adding product ae8970f7d83581427155bc725c003594
    unknown error, skipping product
    adding product 650cee762ff8291600de84b1ba99f5a4
    success!
    remaining products:  171 
    
    adding product 53d57871ab3fc4405f05229e639f166c
    unknown error, skipping product
    adding product 9bfbb9bee43bc3096b3f4bd26a79b40e
    unknown error, skipping product
    adding product 44917919527fa652749f1acb6dcf9617
    unknown error, skipping product
    adding product 91c9f9e26f5e7d629d676e53f90515d6
    unknown error, skipping product
    adding product e85ca00d008a532279b798033d59a4c7
    success!
    remaining products:  166 
    
    adding product 4b794d8229db8f33a386b3cbba9eeeee
    success!
    remaining products:  165 
    
    adding product fa7d9e789db0b0f053e0229ebc2aed0e
    success!
    remaining products:  164 
    
    adding product 865476c5e0cd0523e326757deceaae4a
    success!
    remaining products:  163 
    
    adding product 9f365ac1b27ce95a3f99bdcc68420756
    unknown error, skipping product
    adding product c625cd198573c817ebd635325e37cad0
    unknown error, skipping product
    adding product df624627466cdd1d24cd7604d38a73b7
    success!
    remaining products:  160 
    
    adding product 9181303866f74e72f4b4b39ffbd6a330
    unknown error, skipping product
    adding product 3384c1784cc82c0416e1d66d0f38baaf
    unknown error, skipping product
    adding product 4bbb3813dbe1e3a0e8466202046e5420
    unknown error, skipping product
    adding product fdacbbcc2ed7e3b738dd9b305a9f0515
    unknown error, skipping product
    adding product cfaaf68b01c527d626562c22c83010f0
    unknown error, skipping product
    adding product 50a0eaaf3cd93fa86551c7112c259a3a
    success!
    remaining products:  154 
    
    adding product 0623815a789c473725fe584c43c777e9
    unknown error, skipping product
    adding product fe4edcd654c99506f068af26a2c525c5
    success!
    remaining products:  152 
    
    adding product 453129ee88ee891d1b7581e77eafac40
    success!
    remaining products:  151 
    
    adding product a154ffbcec538a4161a406abf62f5b76
    unknown error, skipping product
    adding product 3a9aa4b8be741e21ba9f80b3c3684ce5
    success!
    remaining products:  149 
    
    adding product f14e3a6ee7be3b52ad152791d821faa5
    success!
    remaining products:  148 
    
    adding product 9f82b195b129c873424298c3285a2dc7
    unknown error, skipping product
    adding product f164ba76f5fba1522bfbb098c4597aa6
    unknown error, skipping product
    adding product b453b5a7a737a3fc489fa11aaac1618b
    unknown error, skipping product
    adding product 4fa0d97504185879b6e7524dda47d98b
    success!
    remaining products:  144 
    
    adding product 122fb7b4ed45d1a1489232864c606b4d
    success!
    remaining products:  143 
    
    adding product d6de3681d8f3827433a7f8c54bbd5a81
    success!
    remaining products:  142 
    
    adding product 465c005d04204f859d0080c915f744b3
    success!
    remaining products:  141 
    
    adding product 2947f6c206cc56866a88cc47130625d7
    success!
    remaining products:  140 
    
    adding product c01afd54a92fec65ed6d29f568cebd48
    success!
    remaining products:  139 
    
    adding product 0fb14adee231d12b0f1465448626e6f7
    success!
    remaining products:  138 
    
    adding product efd609fb87c34abf4e581a542aa70db5
    success!
    remaining products:  137 
    
    adding product 7ebc686287f7afcdb4a2af8644b852b2
    success!
    remaining products:  136 
    
    adding product 1f25f73c8d5b60bb64b60fe0457de1f3
    success!
    remaining products:  135 
    
    adding product a4dc1548af356049973fba2271ce29e4
    success!
    remaining products:  134 
    
    adding product d9f9c4243f3556c146d09809739084bf
    success!
    remaining products:  133 
    
    adding product 981d50d64a8d5a7d90aa7eb49927e1b9
    success!
    remaining products:  132 
    
    adding product 6d658d7273dabf3a616e9193db0446d8
    success!
    remaining products:  131 
    
    adding product 467fc60274ae2cefade0487f255c3f5d
    success!
    remaining products:  130 
    
    adding product dda33670117c841dd91bba9fb7165a03
    success!
    remaining products:  129 
    
    adding product a45045acdffeec7b92ea019675da48fc
    success!
    remaining products:  128 
    
    adding product eb7dc89b44cdaf01b7128e9c939ac234
    success!
    remaining products:  127 
    
    adding product cd0bf4755af4c76f48087ad34feb72a0
    success!
    remaining products:  126 
    
    adding product e2fd3d57e43d80a5df90a90a0da3bbf8
    success!
    remaining products:  125 
    
    adding product c105ca8b5f446bfd69de73e75defe1ac
    success!
    remaining products:  124 
    
    adding product 975643d32f495b3ef6409c11fa1dec18
    success!
    remaining products:  123 
    
    adding product 9b01ea73aa4de36440a527d52e63870a
    success!
    remaining products:  122 
    
    adding product 523a9ae9c20387fe0507793338aa7593
    success!
    remaining products:  121 
    
    adding product 01584ecac98696299889eaf9999e1b7b
    success!
    remaining products:  120 
    
    adding product 24d26100b9d2097f0418ed14dd528b38
    success!
    remaining products:  119 
    
    adding product 965052c6a03942b536a3ff2d134d61c1
    success!
    remaining products:  118 
    
    adding product b276f01d37dac0090b04042539a2aaab
    success!
    remaining products:  117 
    
    adding product 12f9ed7d561f5536e2316645dba28c66
    success!
    remaining products:  116 
    
    adding product 5ecc26b612316e2494011f9fcafa610f
    success!
    remaining products:  115 
    
    adding product a0ac90988690d6ab675e7758a92d5828
    success!
    remaining products:  114 
    
    adding product 630571ee8e61fb9efaa9786a9de27353
    success!
    remaining products:  113 
    
    adding product 3d96a03e368563d9a18aa0073f7f5c8b
    success!
    remaining products:  112 
    
    adding product 936a0bd1ac3c96d08aa66bfbc3ed9758
    success!
    remaining products:  111 
    
    adding product a3e2acf65a87adb6db5976ca4f149665
    success!
    remaining products:  110 
    
    adding product b8321cc2ef02a2823b92d06f2ea3e298
    success!
    remaining products:  109 
    
    adding product c86027cb65669a6bec290bd22c969f2d
    success!
    remaining products:  108 
    
    adding product 1951440c60517e1646b22e78584305b1
    success!
    remaining products:  107 
    
    adding product 6b1d7eadb42d159909af05a7a6d88989
    success!
    remaining products:  106 
    
    adding product 75181a27e029e8d9d2e677df25a90ae2
    success!
    remaining products:  105 
    
    adding product 58e36f59f7e532acacf9cb6ef1ab91b9
    success!
    remaining products:  104 
    
    adding product d3fb5d7eda419a28cb2719d965443ebd
    success!
    remaining products:  103 
    
    adding product 76a77077b53444d655fff2a6c350d5ca
    success!
    remaining products:  102 
    
    adding product 5ce7df80a9e32ee366f578e7ad3d290a
    success!
    remaining products:  101 
    
    adding product 5ceda82de78ea91d4c01e536a0673341
    success!
    remaining products:  100 
    
    adding product 4992f85f02e4d718826650c96219875d
    success!
    remaining products:  99 
    
    adding product 3f7c7beac887a8bf0ca061b7fe1bcae8
    success!
    remaining products:  98 
    
    adding product 3d888043a179cc3414d569db712d8846
    success!
    remaining products:  97 
    
    adding product 1d7c91037101cce725efa1fe469232a5
    success!
    remaining products:  96 
    
    adding product 230a6741f165e1875cdda3f2a19ba156
    success!
    remaining products:  95 
    
    adding product 18c41d036d6bc0a2715563dbbc56b94a
    success!
    remaining products:  94 
    
    adding product 81dd12121d041b1cff1d0792266b2ce6
    success!
    remaining products:  93 
    
    adding product cc10d94812d8d2c9c656c30fea0cf347
    success!
    remaining products:  92 
    
    adding product 773ac275840384ef9c3704eae98ff82a
    success!
    remaining products:  91 
    
    adding product 01d749bd803935f7edf4b14ce1176d78
    success!
    remaining products:  90 
    
    adding product bc5e6eabfbafdca716f42c5e69644480
    success!
    remaining products:  89 
    
    adding product 216cbd05fb1918ba700506718cd8d915
    success!
    remaining products:  88 
    
    adding product e0f4d7c0453ad5d27b69eba851357a31
    success!
    remaining products:  87 
    
    adding product c9bbba509b7304e3aec72ce594a0bec1
    success!
    remaining products:  86 
    
    adding product a30a278ef4861cddd5d253e9c63c1a27
    unknown error, skipping product
    adding product 0b9dcc39ddecd3359566b59d3c9a6b9c
    success!
    remaining products:  84 
    
    adding product ed4e17d67f76e380e297298c8629c38d
    success!
    remaining products:  83 
    
    adding product bdb7179e2db5dca88a7117c1d344a553
    success!
    remaining products:  82 
    
    adding product 1bbd8746b185f6d539461fc40141577a
    success!
    remaining products:  81 
    
    adding product 58836fbe6a74d8fbef6dbce688905c71
    success!
    remaining products:  80 
    
    adding product bc8147ca69573b8a699c18e7ddbcfb48
    success!
    remaining products:  79 
    
    adding product 6ef43ab936ea0c3ff156b0e099c783ba
    unknown error, skipping product
    adding product 7f88a31a9369d5c49af25c4e84ea29d8
    success!
    remaining products:  77 
    
    adding product b1655d34c2c61209de20e71d9a2dc66f
    success!
    remaining products:  76 
    
    adding product c94ac72ea12ea1943136f2bc64719600
    success!
    remaining products:  75 
    
    adding product 3ad933a1f8e366828cbfc88b038fa41e
    success!
    remaining products:  74 
    
    adding product 423aa9774a2b7131b0061979ecb645e8
    success!
    remaining products:  73 
    
    adding product 5646dfcf5046a6c1b4014ee53d9d614d
    success!
    remaining products:  72 
    
    adding product e35ca2aa785ca87355449938e1450f9b
    success!
    remaining products:  71 
    
    adding product a010998d9841ce5759a37db387d85268
    success!
    remaining products:  70 
    
    adding product f0e6d819eb9557e2fc97ac251d6bcd2a
    success!
    remaining products:  69 
    
    adding product 2418c4e3de622a573d9233ad9ab707a3
    success!
    remaining products:  68 
    
    adding product 047471c151b6f90ac3a4ffeaddadf03c
    success!
    remaining products:  67 
    
    adding product 055b16101a3381e4f963bdb30117593a
    success!
    remaining products:  66 
    
    adding product 2d818880a4d72c14f185cbef3fc6061a
    success!
    remaining products:  65 
    
    adding product 47b4388afbc65310d3ba617913e81d27
    success!
    remaining products:  64 
    
    adding product 251b44a9a8eb5bc3ce770290bc8609dd
    success!
    remaining products:  63 
    
    adding product c3be0a55f6361e9a215d06fe83166945
    success!
    remaining products:  62 
    
    adding product 6ead95f03fc10ada537a2c9a21098d16
    success!
    remaining products:  61 
    
    adding product 2b26cd1908760b63ea7268209d1750db
    success!
    remaining products:  60 
    
    adding product e3036fb3e0fd60ba20772f6325fa8f60
    success!
    remaining products:  59 
    
    adding product af4d3ce5d7a2e6955130f2b2720d0873
    success!
    remaining products:  58 
    
    adding product b6a243747ce4d20eaf2cf025e7176662
    success!
    remaining products:  57 
    
    adding product 667e73abb11a09bfac437b5479ce6552
    success!
    remaining products:  56 
    
    adding product 6d390dcb6eaca31079e9876887a36006
    success!
    remaining products:  55 
    
    adding product dfd5301dee688bb803b008e9db07b715
    success!
    remaining products:  54 
    
    adding product b0f00ab3d6cc17c5f71c749c7674042a
    success!
    remaining products:  53 
    
    adding product cc5fa5d3ae683cca46d75d9066d3d0d9
    success!
    remaining products:  52 
    
    adding product ab9b6a5f5330077cd65d92aa33697153
    success!
    remaining products:  51 
    
    adding product 2dbf95b5c5289b340cd53d7d7dd016ec
    success!
    remaining products:  50 
    
    adding product 2ae3d2aaceaf26246581744124859b07
    success!
    remaining products:  49 
    
    adding product e642e53491d96d64124a4d5800c43b5f
    success!
    remaining products:  48 
    
    adding product 72215f7896ee90dfda83e7803a3c08db
    success!
    remaining products:  47 
    
    adding product 3af7e700968700884b9da655b280a267
    success!
    remaining products:  46 
    
    adding product 2649b36f54ee6080dd7e2c057585bce6
    success!
    remaining products:  45 
    
    adding product 1895037c0fa55110b0e0e20d2e68a0d7
    unknown error, skipping product
    adding product dcb91ad1cd3630601020f83d7b6883e0
    unknown error, skipping product
    adding product 1499a983c3cb5d60d1e7d7d1419144b0
    unknown error, skipping product
    adding product 991eadc4a0783c27d2ef14ba5924c651
    unknown error, skipping product
    adding product 6812447e101094f86b8bbcf140a3d4bd
    unknown error, skipping product
    adding product 3688c640bfeed975b53e4bca4d1b72b0
    unknown error, skipping product
    adding product 7dd9884b559f0344c9254ce81e001ae4
    unknown error, skipping product
    adding product 471c96afb06d58297ee23ae23d6e18fd
    unknown error, skipping product
    adding product f18eb5201b7221148de220114667ba96
    success!
    remaining products:  36 
    
    adding product febb75eccd1101d84a2aa5eb87859ce0
    success!
    remaining products:  35 
    
    adding product 15a42c1646fee0042302653e2910092b
    success!
    remaining products:  34 
    
    adding product d613ca9ece1fec54ec026f15b9a000b2
    success!
    remaining products:  33 
    
    adding product 236a4be2466a2054e4d84a3126eccbda
    success!
    remaining products:  32 
    
    adding product a6dcd4cdbe70405ae64f746a1904b7d0
    success!
    remaining products:  31 
    
    adding product f09a9eae79258394989c32e851c9ec10
    unknown error, skipping product
    adding product b464d8ccff7bf47688f59c0fc4403b5b
    unknown error, skipping product
    adding product ebf3a58ca4075b3ceee4193c0dee1754
    success!
    remaining products:  28 
    
    adding product 7f29f58980570546b1ae814455bdcc31
    success!
    remaining products:  27 
    
    adding product 44646f4cd5c4c7d9c6c3f2f6be4dcb1f
    success!
    remaining products:  26 
    
    adding product 607f81b73375b618f549c6c8692c4e88
    success!
    remaining products:  25 
    
    adding product 9ad2f925a32643f541b183503f33a8c6
    success!
    remaining products:  24 
    
    adding product 5ca177e9af12f0d5d9c4c72668c81b63
    success!
    remaining products:  23 
    
    adding product 3ba0e6d211dbba5f6b63786af5313615
    success!
    remaining products:  22 
    
    adding product 9d53b7a44f7aea7ef05b4bc3c1e37d09
    success!
    remaining products:  21 
    
    adding product 7c8b2cb44792a9a6b30a02869a605fd8
    success!
    remaining products:  20 
    
    adding product 6fb41c898918ad5a0df0e50f3790f057
    success!
    remaining products:  19 
    
    adding product 755945a59ff256394631b079277ab8bc
    success!
    remaining products:  18 
    
    adding product 6525055e9dbde10e2238f8ebee694c96
    success!
    remaining products:  17 
    
    adding product b757aa8b4d5f403e9c6c0a50a8cef71f
    success!
    remaining products:  16 
    
    adding product 21c6d7a1fc87615fa44ae7657a773566
    success!
    remaining products:  15 
    
    adding product dd813fde7c3bf5f3b947d7d401d8fba4
    success!
    remaining products:  14 
    
    adding product f883e0d24a78b0f05da41c6dab3c54bb
    success!
    remaining products:  13 
    
    adding product 50f0a48e0c1f60f822f218c3e419d1a4
    success!
    remaining products:  12 
    
    adding product f9148ba9f7fe304fd171caff200636ab
    success!
    remaining products:  11 
    
    adding product 3a5a64a567a75090add692e2a4377e8d
    success!
    remaining products:  10 
    
    adding product 58589b2f5ef1c0bbfdcd09c6fb0b47b7
    success!
    remaining products:  9 
    
    adding product cf37368a5897a76478650a7eea56cfc1
    unknown error, skipping product
    adding product 9bf521d44fb17b1abc961ebc08c28de0
    unknown error, skipping product
    adding product cb01a90256508ed990fe50e3562d0983
    success!
    remaining products:  6 
    
    adding product c56aa2102f060ad7471fbefe5e296c92
    success!
    remaining products:  5 
    
    adding product b39ac02ff5021fed10cb9988a23d5d02
    unknown error, skipping product
    adding product 740c87068ac89f325b63a9dbeed2885b
    unknown error, skipping product
    adding product 9c8001602e98208ef4e8d1bbf79fee65
    success!
    remaining products:  2 
    
    adding product 874121fee1d3bebea38815d169d47848
    success!
    remaining products:  1 
    
    


```python
df.to_csv('/data/out/tables/output.csv', index=False)
```

Nakoniec pridávam aj odhady pre prípad, že uvažujeme len novšiu polovicu z historických dát o predajnosti.


```python
df_emphasize_recent = pd.DataFrame([], columns = ['product_id_hash', 'expected_stockout'])

for product_id_hash in productList:
    try:
        p = stockDaysData[stockDaysData['product_id_hash'] == product_id_hash]

        p = p.drop(columns='product_id_hash')
        p = p.drop_duplicates()
        p = p.sort_values(by=['period'])

        df_emphasize_recent = df_emphasize_recent.append({'product_id_hash': product_id_hash, 'expected_stockout': get_next_stockout(p, emphasize_recent=False)}, ignore_index=True)
    except:
        pass

df.to_csv('/data/out/tables/output_emphasize_recent.csv', index=False)
```

Do budúcna by som vyskúšal niektorý z pokročilejších modelov pre analýzu časových rád.

- https://towardsdatascience.com/time-series-modeling-using-scikit-pandas-and-numpy-682e3b8db8d1
- https://towardsdatascience.com/forecasting-of-periodic-events-with-ml-5081db493c46
- https://www.cienciadedatos.net/documentos/py27-time-series-forecasting-python-scikitlearn.html
